<?php namespace AlexDi\Ordersadd\Components;

use Cms\Classes\Page;
use Cms\Classes\ComponentBase;
use DB;
use input;
use Auth;
use Request;

use AlexDi\ordersadd\models\Order;

class OrdersAdd extends ComponentBase
{
    use \October\Rain\Database\Traits\Validation;

    public function componentDetails()
    {
        return [
            'name'        => 'Добавление в корзину',
            'description' => 'Добавление товаров в корзину'
        ];
    }

    public function defineProperties()
    {
        return [
            'slug' => [
                'title'       => 'Slug',
                'description' => 'URL slug parameter',
                'default'     => '{{ :slug }}',
                'type'        => 'string'
            ],
        ];
    }

    public $modal;

    public function onRun() {
        $func = 'AddToBasket';
            $this->$func(); 
    }

    protected function AddToBasket()
    {
        if($_SERVER['REQUEST_METHOD'] == "POST" and isset($_POST['btn1'])) {
        
        
            $slug = $this->property('slug');
            $priceproduct;
            $product_id;
            $select_size = Request::get('size');
            $quantity_product_id;
            $quantity_product = 0;

            $this->model = $this->page->album = Order::find($slug);
            //находит slug в таблице product и выводит его
            $id_product = DB::select("SELECT * FROM alexdi_alexdi_product WHERE (slug='$slug')");

            foreach ($id_product as $product) {
                if ($product->discount)
            {
                $priceproduct = ($product->price*(1-($product->discount)/100)) . PHP_EOL;
            }
            else{
                $priceproduct = $product->price . PHP_EOL;
            }
            $product_id = $product->id . PHP_EOL;
            }

            $exp = DB::select("SELECT * FROM alexdi_alexdi_availability WHERE (size='$select_size') AND (product_id='$product_id')");
            foreach ($exp as $ex) {
                $quantity_product_id = $ex->id . PHP_EOL;
                $quantity_product = $ex->quantity . PHP_EOL;
            }

            if ($quantity_product > 0)
            {
            echo(
                '
                <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Внимание</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        </div>
                        <div class="modal-body">
                        Товар добавлен в корзину
                        </div>
                        <div class="modal-footer">
                        <form class="m-0" action="/project/basket/1"> 
                            <button type="submit" class="btn btn-secondary">Корзина</button>
                        </form>
                        <p class="text-center text-md-left text-underline pl-1 m-0"> <a class="btn btn-dark" href="#" onclick="javascript:history.go(-2); return false;">Продолжить покупки</a> </p>

                        
                        </div>
                    </div>
                    </div>
                </div>
                '
            );
            }
            else
            {
                echo(
                '
                <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Внимание</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        </div>
                        <div class="modal-body">
                        Товара нет в наличии
                        </div>
                        <div class="modal-footer">
                        <p class="text-center text-md-left text-underline pl-1 m-0"> <a class="btn btn-dark" href="#" onclick="javascript:history.go(-2); return false;">Вернуться к каталогу</a> </p>
                        </div>
                    </div>
                    </div>
                </div>
                ');
            }

            $check= \alexdi\ordersadd\models\order::where('user_id', '=', Auth::getUser()->id)->get();

            if(sizeof($check)==0)
            {

                if ($quantity_product > 0)
                {
                    //находит ВСЕ ПОЛЯ в таблице product и дает возможность обратиться к ним
                    $id = Db::table('alexdi_ordersadd_orders')->insertGetId([
                    'user_id' => $user = Auth::getUser()->id,
                    ]);
    
                    Db::table('alexdi_ordersadd_orders_tovar')->insert([
                        [
                        'product_id' => $product_id,
                        'size' => $select_size,
                        'price' => $priceproduct,
                        'slug' => $slug,
                        'order_id' => $id,
                        'user_id' => $user = Auth::getUser()->id,]
                        ]);
                    
                    DB::update("UPDATE alexdi_alexdi_availability SET quantity = (quantity-1) WHERE id='$quantity_product_id'");
                    $quantity_product = 0;
                }
                else {
                    $quantity_product = 0;
                }        
            }
            else
            {

                $id_order= \alexdi\ordersadd\models\order::where('user_id', '=', Auth::getUser()->id)->get()[0];


                if ($quantity_product > 0)
                {
                Db::table('alexdi_ordersadd_orders_tovar')->insert([
                    [
                    'product_id' => $product_id,
                    'size' => $select_size,
                    'price' => $priceproduct,
                    'slug' => $slug,
                    'order_id' => $id_order->id,
                    'user_id' => $user = Auth::getUser()->id,]
                    ]);
                    DB::update("UPDATE alexdi_alexdi_availability SET quantity = (quantity-1) WHERE id='$quantity_product_id'");
                    $quantity_product = 0;
                }
                else {
                    $quantity_product = 0;
                }      
            }
        
        }
        clearstatcache();
    }

}
