<?php namespace AlexDi\OrdersAdd\Components;

use Cms\Classes\Page;
use Cms\Classes\ComponentBase;
use DB;
use Input;
use Auth;
use Request;

use AlexDi\ordersadd\models\Order;

class DeleteFromBasket extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Удаление из корзины',
            'description' => 'Удаление товара из корзины пользователя'
        ];
    }

    public function defineProperties()
    {
        return [];
    }


    public function onRun() {
        $func = 'DeleteFromBasketPr';
            $this->$func();     
    }

    protected function DeleteFromBasketPr()
    {
        $select_size = Request::get('size');
        $product_id = Request::get('product_id');
        $delete_product_id = Input::get('delete_product_id'); 
        $quantity_product_id;

        if($_SERVER['REQUEST_METHOD'] == "POST" and isset($_POST['btn_delete'])) {
            
            DB::delete("DELETE from alexdi_ordersadd_orders_tovar where (id = '$delete_product_id') "); 
            
            $exp = DB::select("SELECT * FROM alexdi_alexdi_availability WHERE (size='$select_size') AND (product_id='$product_id')");
            foreach ($exp as $ex) {
                $quantity_product_id = $ex->id . PHP_EOL;
            }
            DB::update("UPDATE alexdi_alexdi_availability SET quantity = (quantity+1) WHERE id='$quantity_product_id'");
            header("Refresh:0");
            
        }
        clearstatcache();
    }
}

