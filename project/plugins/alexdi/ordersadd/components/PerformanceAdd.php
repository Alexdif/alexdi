<?php namespace Alexdi\Ordersadd\Components;

use Cms\Classes\Page;
use Cms\Classes\ComponentBase;
use DateTime;
use DB;
use Input;
use Auth;
use Request;

use AlexDi\ordersadd\models\Performance;

class PerformanceAdd extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Оформление заказа',
            'description' => 'Компонент оформления заказа'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public $modal;

    public function onRun() {
        $func = 'AddPerformance';
            $this->$func();  
            $name = Request::get('name');
            echo($name);
    }

    protected function AddPerformance()
    {
        
        if($_SERVER['REQUEST_METHOD'] == "POST" and isset($_POST['send']) or isset($_POST['send1']) ) 
        {
            $user_id = Auth::getUser()->id;
            $name = Request::get('name');
            $surname = Request::get('surname');
            $email = Request::get('email');
            $telephone = Request::get('telephone');
            $order_id = "1";
            $index = "";
            $created_at = NOW();
            $start_date = new DateTime('2000-01-01 00:00:00');
            $time = $start_date->diff($created_at);
            $idid = $time->d . $time->h . $time->i . $time->y . $time->m . $time->s;
            $tovars = \alexdi\ordersadd\Models\Orders_tovar::where('user_id', '=', $user_id)->get(); 
            $stage = "Обрабатывается";
            if ($_POST['delivery'] == "Доставка курьером")
                {
                    $address = Request::get('address');
                    $index = Request::get('index');
                    $pickup = "Доставка курьером";
                }
                if ($_POST['delivery'] == "Самовывоз")
                {
                    $address = Request::get('point');
                    $pickup = "Самовывоз";
                }
            $id = Db::table('alexdi_ordersadd_performances')->insertGetId([ 
                'user_id' => $user_id, 
                'name' => $name,
                'surname' => $surname,
                'email' => $email,
                'phone' => $telephone,
                'index' => $index,
                'address' => $address,
                'pickup' => $pickup,
                'created_at' => $created_at,
                'order_id' => $idid,
                'stage' => $stage,
            
            ]); 
            foreach($tovars as $tov){
                $product_id = $tov->product_id;
                $price=$tov->price;
                $size=$tov->size;
                $slug=$tov->slug;

                Db::table('alexdi_ordersadd_performances_tovar')->insert([ 
                    ['product_id' => $product_id,
                    'price' => $price, 
                    'size' => $size,
                    'slug' => $slug, 
                    'performance_id' => $id,
                    ]
                    ]); 

            }
            
            DB::delete("DELETE FROM alexdi_ordersadd_orders_tovar WHERE user_id = '$user_id'"); 
            header("Refresh:0; url=ready");
            
            
        }
    }
}
