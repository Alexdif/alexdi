<?php namespace Alexdi\Ordersadd;

use Backend;
use System\Classes\PluginBase;

/**
 * ordersadd Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Корзина',
            'description' => 'Компоненты взаимодействия с корзиной',
            'author'      => 'alexdi',
            'icon'        => 'sign-in'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        //return []; // Remove this line to activate

        return [
            'AlexDi\ordersadd\Components\OrdersAdd' => 'OrdersAdd',
            'AlexDi\ordersadd\Components\DeleteFromBasket' => 'DeleteFromBasket',
            'AlexDi\ordersadd\Components\PerformanceAdd' => 'PerformanceAdd'
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'alexdi.ordersadd.some_permission' => [
                'tab' => 'ordersadd',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'ordersadd' => [
                'label'       => 'Корзина',
                'url'         => Backend::url('alexdi/ordersadd/Orders'),
                'icon'        => 'icon-leaf',
                'permissions' => ['alexdi.ordersadd.*'],
                'order'       => 500,
            ],
        ];
    }

}
