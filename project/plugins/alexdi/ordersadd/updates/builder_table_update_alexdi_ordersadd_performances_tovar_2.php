<?php namespace Alexdi\Ordersadd\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateAlexdiOrdersaddPerformancesTovar2 extends Migration
{
    public function up()
    {
        Schema::table('alexdi_ordersadd_performances_tovar', function($table)
        {
            $table->integer('performances_id');
        });
    }
    
    public function down()
    {
        Schema::table('alexdi_ordersadd_performances_tovar', function($table)
        {
            $table->dropColumn('performances_id');
        });
    }
}
