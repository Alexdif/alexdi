<?php namespace Alexdi\Ordersadd\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateAlexdiOrdersaddOrdersTovar5 extends Migration
{
    public function up()
    {
        Schema::table('alexdi_ordersadd_orders_tovar', function($table)
        {
            $table->string('slug')->change();
            $table->renameColumn('basket_id', 'order_id');
        });
    }
    
    public function down()
    {
        Schema::table('alexdi_ordersadd_orders_tovar', function($table)
        {
            $table->string('slug', 191)->change();
            $table->renameColumn('order_id', 'basket_id');
        });
    }
}
