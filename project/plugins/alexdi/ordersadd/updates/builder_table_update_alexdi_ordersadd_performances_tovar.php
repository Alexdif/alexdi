<?php namespace Alexdi\Ordersadd\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateAlexdiOrdersaddPerformancesTovar extends Migration
{
    public function up()
    {
        Schema::table('alexdi_ordersadd_performances_tovar', function($table)
        {
            $table->integer('product_id');
            $table->integer('price');
            $table->integer('size');
            $table->string('slug');
            $table->increments('id')->unsigned(false)->change();
        });
    }
    
    public function down()
    {
        Schema::table('alexdi_ordersadd_performances_tovar', function($table)
        {
            $table->dropColumn('product_id');
            $table->dropColumn('price');
            $table->dropColumn('size');
            $table->dropColumn('slug');
            $table->increments('id')->unsigned()->change();
        });
    }
}
