<?php namespace Alexdi\Ordersadd\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateAlexdiOrdersaddPerformancesTovar3 extends Migration
{
    public function up()
    {
        Schema::table('alexdi_ordersadd_performances_tovar', function($table)
        {
            $table->renameColumn('performances_id', 'performance_id');
        });
    }
    
    public function down()
    {
        Schema::table('alexdi_ordersadd_performances_tovar', function($table)
        {
            $table->renameColumn('performance_id', 'performances_id');
        });
    }
}
