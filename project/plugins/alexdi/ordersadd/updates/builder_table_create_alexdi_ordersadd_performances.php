<?php namespace Alexdi\Ordersadd\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateAlexdiOrdersaddPerformances extends Migration
{
    public function up()
    {
        Schema::create('alexdi_ordersadd_performances', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('product_id');
            $table->integer('price');
            $table->integer('size');
            $table->string('slug');
            $table->string('name');
            $table->string('surname');
            $table->string('email');
            $table->string('phone');
            $table->string('index')->nullable();
            $table->string('address');
            $table->boolean('pickup');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('order_id');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('alexdi_ordersadd_performances');
    }
}
