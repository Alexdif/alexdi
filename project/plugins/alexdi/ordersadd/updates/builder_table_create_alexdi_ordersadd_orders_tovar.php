<?php namespace Alexdi\Ordersadd\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateAlexdiOrdersaddOrdersTovar extends Migration
{
    public function up()
    {
        Schema::create('alexdi_ordersadd_orders_tovar', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('alexdi_ordersadd_orders_tovar');
    }
}
