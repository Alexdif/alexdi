<?php namespace Alexdi\Ordersadd\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateAlexdiOrdersaddOrders extends Migration
{
    public function up()
    {
        Schema::table('alexdi_ordersadd_orders', function($table)
        {
            $table->dropColumn('product_id');
            $table->dropColumn('price');
            $table->dropColumn('size');
            $table->dropColumn('slug');
        });
    }
    
    public function down()
    {
        Schema::table('alexdi_ordersadd_orders', function($table)
        {
            $table->integer('product_id');
            $table->integer('price');
            $table->integer('size');
            $table->string('slug', 50);
        });
    }
}
