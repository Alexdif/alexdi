<?php namespace Alexdi\Ordersadd\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateAlexdiOrdersaddOrdersTovar2 extends Migration
{
    public function up()
    {
        Schema::table('alexdi_ordersadd_orders_tovar', function($table)
        {
            $table->integer('id_basket_tovar');
            $table->string('slug')->change();
        });
    }
    
    public function down()
    {
        Schema::table('alexdi_ordersadd_orders_tovar', function($table)
        {
            $table->dropColumn('id_basket_tovar');
            $table->string('slug', 191)->change();
        });
    }
}
