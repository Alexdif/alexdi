<?php namespace Alexdi\Ordersadd\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateAlexdiOrdersaddPerformances5 extends Migration
{
    public function up()
    {
        Schema::table('alexdi_ordersadd_performances', function($table)
        {
            $table->integer('user_id')->unsigned()->change();
            $table->bigInteger('order_id')->unsigned()->change();
        });
    }
    
    public function down()
    {
        Schema::table('alexdi_ordersadd_performances', function($table)
        {
            $table->integer('user_id')->unsigned(false)->change();
            $table->bigInteger('order_id')->unsigned(false)->change();
        });
    }
}
