<?php namespace Alexdi\Ordersadd\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateAlexdiOrdersaddPerformances extends Migration
{
    public function up()
    {
        Schema::table('alexdi_ordersadd_performances', function($table)
        {
            $table->string(' stage');
            $table->string('pickup')->nullable(false)->unsigned(false)->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('alexdi_ordersadd_performances', function($table)
        {
            $table->dropColumn(' stage');
            $table->boolean('pickup')->nullable(false)->unsigned(false)->default(null)->change();
        });
    }
}
