<?php namespace Alexdi\Ordersadd\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreatePerformancesTable extends Migration
{
    public function up()
    {
        Schema::create('alexdi_ordersadd_performances', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('alexdi_ordersadd_performances');
    }
}
