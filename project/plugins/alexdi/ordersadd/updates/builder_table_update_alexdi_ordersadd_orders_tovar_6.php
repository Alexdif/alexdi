<?php namespace Alexdi\Ordersadd\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateAlexdiOrdersaddOrdersTovar6 extends Migration
{
    public function up()
    {
        Schema::table('alexdi_ordersadd_orders_tovar', function($table)
        {
            $table->integer('user_id');
        });
    }
    
    public function down()
    {
        Schema::table('alexdi_ordersadd_orders_tovar', function($table)
        {
            $table->dropColumn('user_id');
        });
    }
}
