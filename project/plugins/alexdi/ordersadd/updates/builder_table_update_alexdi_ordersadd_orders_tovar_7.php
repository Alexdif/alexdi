<?php namespace Alexdi\Ordersadd\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateAlexdiOrdersaddOrdersTovar7 extends Migration
{
    public function up()
    {
        Schema::table('alexdi_ordersadd_orders_tovar', function($table)
        {
            $table->increments('id')->unsigned()->change();
            $table->integer('product_id')->unsigned()->change();
            $table->integer('order_id')->unsigned()->change();
            $table->integer('user_id')->unsigned()->change();
        });
    }
    
    public function down()
    {
        Schema::table('alexdi_ordersadd_orders_tovar', function($table)
        {
            $table->increments('id')->unsigned(false)->change();
            $table->integer('product_id')->unsigned(false)->change();
            $table->integer('order_id')->unsigned(false)->change();
            $table->integer('user_id')->unsigned(false)->change();
        });
    }
}
