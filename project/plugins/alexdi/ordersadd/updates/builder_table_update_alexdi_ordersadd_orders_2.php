<?php namespace Alexdi\Ordersadd\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateAlexdiOrdersaddOrders2 extends Migration
{
    public function up()
    {
        Schema::table('alexdi_ordersadd_orders', function($table)
        {
            $table->integer('user_id')->unsigned()->change();
        });
    }
    
    public function down()
    {
        Schema::table('alexdi_ordersadd_orders', function($table)
        {
            $table->integer('user_id')->unsigned(false)->change();
        });
    }
}
