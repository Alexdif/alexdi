<?php namespace Alexdi\Ordersadd\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateOrdersTable extends Migration
{
    public function up()
    {
        Schema::create('alexdi_ordersadd_orders', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('product_id');
            $table->integer('size');
            $table->integer('price');
            $table->string('slug');
        });
    }

    public function down()
    {
        Schema::dropIfExists('alexdi_ordersadd_orders');
    }
}
