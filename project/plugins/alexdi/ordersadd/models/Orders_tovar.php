<?php namespace Alexdi\Ordersadd\Models;

use Model;
use DB;
use Auth;
/**
 * Model
 */
class Orders_tovar extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'alexdi_ordersadd_orders_tovar';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
    public $belongsTo = [ 
        'product' => ['alexdi\alexdi\Models\product', 'Key'=>'product_id'],
        'order' => ['alexdi\ordersadd\Models\order', 'otherKey'=>'order_id'], 
        ]; 
        

        public function scopeProduct2($filter)
        {
            $user_id = Auth::getUser()->id;
            $filter->Where('user_id', '=', $user_id);
            return $filter;
        }
}
