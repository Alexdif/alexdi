<?php namespace Alexdi\Ordersadd\Models;

use Model;
use Request;
use Auth;
/**
 * Model
 */
class Performance_tovar extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    public $belongsTo = [ 
        'product' => ['alexdi\alexdi\Models\product', 'Key'=>'product_id'],
        
        'performance' => ['alexdi\ordersadd\Models\Performance', 'otherKey'=>'performance_id'], 
    ]; 
    /**
     * @var string The database table used by the model.
     */
    public $table = 'alexdi_ordersadd_performances_tovar';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
    public function scopeProductDetails1($filter)
    {
        $select_id = Request::get('this_id');
        $user_id = Auth::getUser()->id;
        $filter->Where('alexdi_ordersadd_performances_tovar.performance_id','=',  $select_id);
    }

}
// 


// <?php namespace Alexdi\Ordersadd\Models;

// use Model;

// /**
//  * Model
//  */
// class Performance_tovar extends Model
// {
//     use \October\Rain\Database\Traits\Validation;
    
//     /*
//      * Disable timestamps by default.
//      * Remove this line if timestamps are defined in the database table.
//      */
//     public $timestamps = false;

//     public $belongsTo = [ 
//         'product' => ['alexdi\alexdi\Models\product', 'Key'=>'product_id'],
        
//         'performance' => ['alexdi\ordersadd\Models\Performance', 'otherKey'=>'performances_id'], 
//     ]; 
    
//     /**
//      * @var string The database table used by the model.
//      */
//     public $table = 'alexdi_ordersadd_performances_tovar';

//     /**
//      * @var array Validation rules
//      */
//     public $rules = [
//     ];

// }