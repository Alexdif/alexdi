<?php namespace Alexdi\Ordersadd\Models;

use Model;
use project\Rain\Database\Traits\Validation;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Auth;
use Recuest;
/**
 * orders Model
 */
class Order extends Model
{
    
    /**
     * @var string The database table used by the model. 
     */
    public $table = 'alexdi_ordersadd_orders';
    /**
     * @var array Guarded fields
     */
   // protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    // public $hasMany = [];
    public $belongsTo = [
        'products' => ['alexdi\alexdi\models\product', 'key' => 'product_id'],
        'user' => ['rainlab\user\models\user', 'key' => 'user_id']
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    
    public function scopeProduct($filter)
    {
        $user_id = Auth::getUser()->id;
        $filter->Where('alexdi_ordersadd_orders.user_id','=', $user_id);
    }
    public $hasMany = [ 
        'order_tovar' =>[ 'alexdi\ordersadd\Models\orders_tovar',
        'delete' => true],
        ]; 


        

        // public function onDeleteFromBasketPr()
        // {

        //     $select_size = Request::get('size');
        //     $product_id = Request::get('product_id');
        //     $delete_product_id = Input::get('delete_product_id'); 
        //     $quantity_product_id;
    
        //     if($_SERVER['REQUEST_METHOD'] == "POST" and isset($_POST['btn_delete'])) {
                
        //         DB::delete("DELETE from alexdi_ordersadd_orders_tovar where (id = '$delete_product_id') "); 
                
        //         $exp = DB::select("SELECT * FROM alexdi_alexdi_availability WHERE (size='$select_size') AND (product_id='$product_id')");
        //         foreach ($exp as $ex) {
        //             $quantity_product_id = $ex->id . PHP_EOL;
        //         }
        //         DB::update("UPDATE alexdi_alexdi_availability SET quantity = (quantity+1) WHERE id='$quantity_product_id'");
                
        //     }
        //     clearstatcache();
        //     return $this->listRefresh();
        // }
}
