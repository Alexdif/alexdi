<?php namespace Alexdi\Ordersadd\Models;

use Model;
use project\Rain\Database\Traits\Validation;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Auth;

use Cms\Classes\Page;
use Cms\Classes\ComponentBase;
use DateTime;
use Request;

/**
 * Performance Model
 */
class Performance extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'alexdi_ordersadd_performances';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [ 
        'performance_tovar' =>[ 'alexdi\ordersadd\Models\performance_tovar',
        'delete' => true],
        ]; 
    public $belongsTo = [
        'products' => ['alexdi\alexdi\models\product', 'key' => 'product_id'],
        'user' => ['rainlab\user\models\user', 'key' => 'user_id']
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public function scopeProduct3($filter)
    {
        $user_id = Auth::getUser()->id;
        $filter->Where('alexdi_ordersadd_performances.user_id','=', $user_id)->groupBy('order_id');
    }

    public function scopeProductDetails($filter)
    {
        $url =  (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $mass = parse_url($url, PHP_URL_PATH);
        $mass1 = explode("/",$mass);
        $slug = $mass1[3];
        $filter->Where('alexdi_ordersadd_performances.order_id','=', $slug );
    }


}

