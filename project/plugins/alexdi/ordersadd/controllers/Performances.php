<?php namespace Alexdi\Ordersadd\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Performances Back-end Controller
 */
class Performances extends Controller
{

        public $implement = [ 
            'Backend.Behaviors.FormController', 
            'Backend.Behaviors.ListController', 
            'Backend.Behaviors.RelationController',
            ]; 
    
        public $relationConfig = 'config_relation.yaml';    
        public $listConfig = 'config_list.yaml';
        public $formConfig = 'config_form.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Alexdi.Alexdi', 'main-menu-item', 'side-menu-item6');
    }
}
