<?php namespace Alexdi\Ordersadd\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Illuminate\Support\Facades\Input;
use Recuest;

/**
 * Orders Back-end Controller
 */
class Orders extends Controller
{
    public $implement = [
        'Backend\Behaviors\ListController',
        'Backend\Behaviors\FormController',
        'Backend.Behaviors.RelationController',
        'Backend\Behaviors\ReorderController'    
    ];

    public $relationConfig = 'config_relation.yaml';   
    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';
    public $reorderConfig = 'config_reorder.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Alexdi.Alexdi', 'main-menu-item', 'side-menu-item5');
    }

    public function onDeleteFromBasketPr2()
    {
        $del_cart = Input::get('checked'); 
        foreach($del_cart as $a) 
        { 
        $order = \alexdi\ordersadd\Models\order::where('id', '=', $a);
        
        $order_tovar= $order->get()[0]->order_tovar; 

        foreach($order_tovar as $b) 
        { 
            $t = \alexdi\alexdi\Models\availability::where('product_id', '=', $b->product->id) 
            ->where('size', '=', $b->size)->get()[0]; 
            $t->quantity += 1; 
            $t->save(); 

        $b->delete(); 
        } 

        $order->delete(); 
        } 
        return $this->listRefresh();
    }
}
