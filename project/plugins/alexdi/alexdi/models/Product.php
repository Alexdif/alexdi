<?php namespace Alexdi\Alexdi\Models;

use Model;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
/**
 * Model
 */
class Product extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'alexdi_alexdi_product';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $hasMany = [ 
        'availabilities' => 'alexdi\alexdi\Models\Availability'
        ]; 
        
    protected $fillable = ['Product','Availability'];

    public $belongsTo = [ 
        'Country' => ['alexdi\alexdi\models\Country', 'key' => 'id_country'], 
        'Type' => ['alexdi\alexdi\models\Type', 'key' => 'id_type'], 
        'Manufacturer' => ['alexdi\alexdi\models\Manufacturer', 'key' => 'id_manufacturer']
        ]; 
    
    public function getIdManufacturerOptions($keyValue = null) 
    { 
        return Manufacturer::lists('Manufacturer', 'id'); 
    }
    public function getIdCountryOptions($keyValue = null) 
    { 
        return Country::lists('country', 'id'); 
    }
    public function getIdTypeOptions($keyValue = null) 
    { 
        return Type::lists('type', 'id'); 
    }
    public function scopeIdd($query) 
    { 
        $countrys = Input::get('country');
        $sizes = Input::get('size');
        $colors = Input::get('color');
        $prices = Input::get('price');
        $discounts =Input::get('discount');
        $gender =Input::get('gender');
        $manufacturer =Input::get('manufacturer');
        $date = Input::get('sort');

        $types = Input::get('type');

        if ($countrys){ 
            $query->where(function ($query) { 
            foreach( $countrys = Input::get('country') as $country) { 
            $query->orWhere('alexdi_alexdi_Product.id_country', $country); 
            } 
            }); 
        }

        if ($manufacturer){ 
            $query->where(function ($query) { 
            foreach( $manufacturer = Input::get('manufacturer') as $manufacturers) { 
            $query->orWhere('alexdi_alexdi_Product.id_manufacturer', $manufacturers); 
            } 
            }); 
        }

        if ($sizes) { 
            $query->join('alexdi_alexdi_availability', 'alexdi_alexdi_product.id', '=', 'alexdi_alexdi_availability.product_id') 
            ->select('alexdi_alexdi_product.*', 'alexdi_alexdi_availability.size')->groupBy('alexdi_alexdi_product.id'); 
            $query->where(function ($query) { 
            foreach(Input::get('size') as $selectsize) { 
            $query->orWhere('alexdi_alexdi_availability.size', '=', $selectsize)
            ; 
            } 
            });
        }

        if ($types){ 
            $query->where(function ($query) { 
            foreach( $types = Input::get('type') as $type) { 
            $query->orWhere('alexdi_alexdi_Product.id_type', $type); 
            } 
            }); 
        }

        if ($colors){ 
            $query->where(function ($query) { 
            foreach( $colors = Input::get('color') as $color) { 
            $query->orWhere('alexdi_alexdi_Product.color', $color); 
            } 
            }); 
        }

        if ($gender){ 
            $query->where(function ($query) { 
            foreach( $gender = Input::get('gender') as $gen) { 
            $query->orWhere('alexdi_alexdi_Product.gender', $gen); 
            } 
            }); 
        }
        
        if ($prices == 'desc')
            $query->Select('alexdi_alexdi_product.*',DB::raw('CASE WHEN discount>0 THEN price*(1-(discount/100)) ELSE price end as realprice'))->orderBy('realprice','desc');
            
        if ($prices == 'asc')
            $query->Select('alexdi_alexdi_product.*',DB::raw('CASE WHEN discount>0 THEN price*(1-(discount/100)) ELSE price end as realprice'))->orderBy('realprice','asc');
        
        if ($date == 'old')
            $query->orderBy('alexdi_alexdi_product.id','asc');
            
        if ($date == 'new')
            $query->orderBy('alexdi_alexdi_product.id','desc');

        if ($discounts)
            $query->Where('alexdi_alexdi_Product.discount', '>', '0');

        return $query;
    }

}
