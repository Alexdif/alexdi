<?php namespace Alexdi\Alexdi\Models;

use Model;

/**
 * Model
 */
class Availability extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'alexdi_alexdi_availability';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
    
    public $belongsTo = [ 
        'Product' => 'alexdi\alexdi\Models\Product' 
        ];
}
