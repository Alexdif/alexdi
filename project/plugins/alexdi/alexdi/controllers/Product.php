<?php namespace Alexdi\Alexdi\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class Product extends Controller
{
    //public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController',        'Backend\Behaviors\ReorderController'    ];
    
    public $implement = [ 
        'Backend.Behaviors.FormController', 
        'Backend.Behaviors.ListController', 
        'Backend.Behaviors.RelationController',
        'Backend.Behaviors.ReorderController'
        ]; 

    public $relationConfig = 'config_relation.yaml';    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $reorderConfig = 'config_reorder.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Alexdi.Alexdi', 'main-menu-item');
    }
}
