<?php namespace Alexdi\Alexdi\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateAlexdiAlexdiType2 extends Migration
{
    public function up()
    {
        Schema::table('alexdi_alexdi_type', function($table)
        {
            $table->increments('id')->unsigned()->change();
        });
    }
    
    public function down()
    {
        Schema::table('alexdi_alexdi_type', function($table)
        {
            $table->increments('id')->unsigned(false)->change();
        });
    }
}
