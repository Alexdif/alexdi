<?php namespace Alexdi\Alexdi\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateAlexdiAlexdiCountry extends Migration
{
    public function up()
    {
        Schema::create('alexdi_alexdi_country', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('country');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('alexdi_alexdi_country');
    }
}
