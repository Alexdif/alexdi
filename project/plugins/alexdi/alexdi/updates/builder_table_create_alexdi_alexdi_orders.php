<?php namespace Alexdi\Alexdi\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateAlexdiAlexdiOrders extends Migration
{
    public function up()
    {
        Schema::create('alexdi_alexdi_orders', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('product_id');
            $table->integer('quantity');
            $table->integer('price');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('alexdi_alexdi_orders');
    }
}
