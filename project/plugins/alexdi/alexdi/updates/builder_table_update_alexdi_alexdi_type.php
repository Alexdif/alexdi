<?php namespace Alexdi\Alexdi\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateAlexdiAlexdiType extends Migration
{
    public function up()
    {
        Schema::rename('alexdi_alexdi_', 'alexdi_alexdi_type');
        Schema::table('alexdi_alexdi_type', function($table)
        {
            $table->increments('id')->unsigned(false)->change();
            $table->string('type')->change();
        });
    }
    
    public function down()
    {
        Schema::rename('alexdi_alexdi_type', 'alexdi_alexdi_');
        Schema::table('alexdi_alexdi_', function($table)
        {
            $table->increments('id')->unsigned()->change();
            $table->string('type', 191)->change();
        });
    }
}
