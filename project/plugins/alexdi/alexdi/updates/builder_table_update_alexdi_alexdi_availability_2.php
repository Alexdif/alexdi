<?php namespace Alexdi\Alexdi\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateAlexdiAlexdiAvailability2 extends Migration
{
    public function up()
    {
        Schema::table('alexdi_alexdi_availability', function($table)
        {
            $table->integer('product_id')->unsigned()->change();
        });
    }
    
    public function down()
    {
        Schema::table('alexdi_alexdi_availability', function($table)
        {
            $table->integer('product_id')->unsigned(false)->change();
        });
    }
}
