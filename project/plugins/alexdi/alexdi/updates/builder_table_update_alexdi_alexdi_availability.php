<?php namespace Alexdi\Alexdi\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateAlexdiAlexdiAvailability extends Migration
{
    public function up()
    {
        Schema::table('alexdi_alexdi_availability', function($table)
        {
            $table->integer('product_id');
        });
    }
    
    public function down()
    {
        Schema::table('alexdi_alexdi_availability', function($table)
        {
            $table->dropColumn('product_id');
        });
    }
}
