<?php namespace Alexdi\Alexdi\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateAlexdiAlexdiProduct extends Migration
{
    public function up()
    {
        Schema::create('alexdi_alexdi_product', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 100);
            $table->integer('price');
            $table->string('description', 1000);
            $table->string('color');
            $table->integer('size');
            $table->string('coutry', 100);
            $table->string('photo', 1000);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('alexdi_alexdi_product');
    }
}
