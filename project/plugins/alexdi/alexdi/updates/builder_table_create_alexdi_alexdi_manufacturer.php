<?php namespace Alexdi\Alexdi\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateAlexdiAlexdiManufacturer extends Migration
{
    public function up()
    {
        Schema::create('alexdi_alexdi_manufacturer', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('manufacturer');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('alexdi_alexdi_manufacturer');
    }
}
