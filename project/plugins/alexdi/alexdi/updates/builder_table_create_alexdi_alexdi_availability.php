<?php namespace Alexdi\Alexdi\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateAlexdiAlexdiAvailability extends Migration
{
    public function up()
    {
        Schema::create('alexdi_alexdi_availability', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('size');
            $table->integer('quantity');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('alexdi_alexdi_availability');
    }
}
