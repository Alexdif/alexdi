<?php namespace Alexdi\Alexdi\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateAlexdiAlexdiProduct14 extends Migration
{
    public function up()
    {
        Schema::table('alexdi_alexdi_product', function($table)
        {
            $table->integer('id_availability')->unsigned()->change();
            $table->integer('id_country')->unsigned()->change();
            $table->integer('id_type')->unsigned()->change();
            $table->integer('id_manufacturer')->unsigned()->change();
        });
    }
    
    public function down()
    {
        Schema::table('alexdi_alexdi_product', function($table)
        {
            $table->integer('id_availability')->unsigned(false)->change();
            $table->integer('id_country')->unsigned(false)->change();
            $table->integer('id_type')->unsigned(false)->change();
            $table->integer('id_manufacturer')->unsigned(false)->change();
        });
    }
}
