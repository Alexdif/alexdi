<?php namespace Alexdi\Alexdi\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateAlexdiAlexdiProduct12 extends Migration
{
    public function up()
    {
        Schema::table('alexdi_alexdi_product', function($table)
        {
            $table->string('discount')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('alexdi_alexdi_product', function($table)
        {
            $table->dropColumn('discount');
        });
    }
}
