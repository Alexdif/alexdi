<?php namespace Alexdi\Alexdi\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateAlexdiAlexdi extends Migration
{
    public function up()
    {
        Schema::create('alexdi_alexdi_', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('type');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('alexdi_alexdi_');
    }
}
