<?php namespace Alexdi\Alexdi\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateAlexdiAlexdiProduct7 extends Migration
{
    public function up()
    {
        Schema::table('alexdi_alexdi_product', function($table)
        {
            $table->renameColumn('availability', 'id_availability');
        });
    }
    
    public function down()
    {
        Schema::table('alexdi_alexdi_product', function($table)
        {
            $table->renameColumn('id_availability', 'availability');
        });
    }
}
