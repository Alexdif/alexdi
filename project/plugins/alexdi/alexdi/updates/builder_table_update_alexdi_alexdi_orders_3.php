<?php namespace Alexdi\Alexdi\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateAlexdiAlexdiOrders3 extends Migration
{
    public function up()
    {
        Schema::table('alexdi_alexdi_orders', function($table)
        {
            $table->integer('size');
        });
    }
    
    public function down()
    {
        Schema::table('alexdi_alexdi_orders', function($table)
        {
            $table->dropColumn('size');
        });
    }
}
