<?php namespace Alexdi\Alexdi\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateAlexdiAlexdiCountry extends Migration
{
    public function up()
    {
        Schema::table('alexdi_alexdi_country', function($table)
        {
            $table->increments('id')->unsigned(false)->change();
            $table->string('country')->nullable(false)->unsigned(false)->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('alexdi_alexdi_country', function($table)
        {
            $table->increments('id')->unsigned()->change();
            $table->integer('country')->nullable(false)->unsigned(false)->default(null)->change();
        });
    }
}
