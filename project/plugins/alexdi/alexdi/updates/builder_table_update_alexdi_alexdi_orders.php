<?php namespace Alexdi\Alexdi\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateAlexdiAlexdiOrders extends Migration
{
    public function up()
    {
        Schema::table('alexdi_alexdi_orders', function($table)
        {
            $table->date('date');
            $table->increments('id')->unsigned(false)->change();
        });
    }
    
    public function down()
    {
        Schema::table('alexdi_alexdi_orders', function($table)
        {
            $table->dropColumn('date');
            $table->increments('id')->unsigned()->change();
        });
    }
}
