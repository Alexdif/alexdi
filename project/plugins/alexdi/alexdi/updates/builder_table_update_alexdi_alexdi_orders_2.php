<?php namespace Alexdi\Alexdi\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateAlexdiAlexdiOrders2 extends Migration
{
    public function up()
    {
        Schema::table('alexdi_alexdi_orders', function($table)
        {
            $table->boolean('stage');
        });
    }
    
    public function down()
    {
        Schema::table('alexdi_alexdi_orders', function($table)
        {
            $table->dropColumn('stage');
        });
    }
}
