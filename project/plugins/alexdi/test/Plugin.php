<?php namespace Alexdi\Test;

use Backend;
use System\Classes\PluginBase;
use Rainlab\User\Controllers\Users as UsersController;
use Rainlab\User\Models\User as UserModel;
/**
 * test Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Профиль',
            'description' => 'Плагин деталей профиля',
            'author'      => 'alexdi',
            'icon'        => 'sign-in'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
       * @return array
     */
    public function boot(){

        UserModel::extend(function ($model){
            $model->addFillable([
                'address',
                'index',
                'telephone'
            ]);
        });

        UsersController::extendFormFields(function($form, $model, $context){

            $form->addTabFields([
                'address' => [
                    'label' => 'Адрес',
                    'type' => 'textarea',
                    'tab' => 'Профиль'
                ],
                'index' => [
                    'label' => 'Индекс',
                    'type' => 'text',
                    'tab' => 'Профиль'
                ],
                'telephone' => [
                    'label' => 'Телефон',
                    'type' => 'textarea',
                    'tab' => 'Профиль'
                ]
            ]);

        });

        UsersController::extendFormFields(function($form, $model, $context){

            $form->addTabFields([
                'address' => [
                    'label' => 'Адрес',
                    'type' => 'textarea',
                    'tab' => 'Профиль'
                ],
                'index' => [
                    'label' => 'Индекс',
                    'type' => 'text',
                    'tab' => 'Профиль'
                ],
                'telephone' => [
                    'label' => 'Телефон',
                    'type' => 'textarea',
                    'tab' => 'Профиль'
                ]
            ]);

        });
    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Alexdi\Test\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'alexdi.test.some_permission' => [
                'tab' => 'test',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'test' => [
                'label'       => 'test',
                'url'         => Backend::url('alexdi/test/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['alexdi.test.*'],
                'order'       => 500,
            ],
        ];
    }
}
