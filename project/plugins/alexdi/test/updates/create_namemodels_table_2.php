<?php namespace Alexdi\Test\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateNamemodelsTable3 extends Migration
{
    public function up() 
    { 
        Schema::table('users', function($table) 
        { 
            $table->renameColumn('email1', 'vk');
        }); 
    } 

    public function down() 
    { 
        Schema::table('users', function($table) 
        { 
        $table->renameColumn('vk', 'email1'); 
        }); 
    }
}
