<?php namespace Alexdi\Test\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateNamemodelsTable extends Migration
{
    public function up()
    {
        Schema::table('users', function($table) {
            $table->string('vk')->nullable();
        });
    }

    public function down()
    {
        Schema::table('users', function($table) {
            $table->dropColumn('vk');
        });
    }
}
