<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* C:\wamp64\www\project/themes/mine/partials/account/activation_check.htm */
class __TwigTemplate_99f20649263d0b63e8b6336bfeae6168f7c7065e44257e195d628346bf3a421a extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        if ( !twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "is_activated", [], "any", false, false, false, 1)) {
            // line 2
            echo "
    <h3>Ваш Email адрес не был подтвержден.</h3>
    <p>
        Ваш Email адрес не был подтвержден. Пожалуйста, проверьте вашу почту для верификации.
        <a href=\"javascript:;\" data-request=\"onSendActivationEmail\">Отправить сообщение с верефикацией снова.</a>.
    </p>

";
        }
    }

    public function getTemplateName()
    {
        return "C:\\wamp64\\www\\project/themes/mine/partials/account/activation_check.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  37 => 2,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% if not user.is_activated %}

    <h3>Ваш Email адрес не был подтвержден.</h3>
    <p>
        Ваш Email адрес не был подтвержден. Пожалуйста, проверьте вашу почту для верификации.
        <a href=\"javascript:;\" data-request=\"onSendActivationEmail\">Отправить сообщение с верефикацией снова.</a>.
    </p>

{% endif %}", "C:\\wamp64\\www\\project/themes/mine/partials/account/activation_check.htm", "");
    }
}
