<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* C:\wamp64\www\project/themes/mine/partials/ordersadd.htm */
class __TwigTemplate_bb3980fde2250451a5803cd621d240d9f7532456e090e054eaeaf73bff2e052f extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<div class=\"container p-3 pl-4\">
    <h1> Оформление заказа </h1>
</div>
<div class=\"bg_t\">
    <div class=\"container p-3\">
        <div class=\"row\">
            <div class=\"col-6 col-sm-8 col-md-9 col-lg-10\">
            <h4 class=\"p-3\"> Заказ оформлен </h4> 
            </div>
            <div class=\"col-6 col-sm-4 col-md-3 col-lg-2 my-auto p-0 text-center\">
                <form action=\"/project/account\"> 
                    <button class=\"btn btn-oytline-success my-2 my-sm-0 btn-dark\"> Личный кабинет </button>
                </form>
            </div> 
        </div>
        <div class=\"mx-auto container w-100 m-0\">
            <main class=\"col-12  p-0 pt-3\">
                
                <div class=\"row col-12 w-100 m-0 p-0\">
                    Информация о статусе Ваших заказов будет доступна на странице истории заказов в Личном кабинете.
                </div>
            </main>     
        </div>
</div>";
    }

    public function getTemplateName()
    {
        return "C:\\wamp64\\www\\project/themes/mine/partials/ordersadd.htm";
    }

    public function getDebugInfo()
    {
        return array (  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"container p-3 pl-4\">
    <h1> Оформление заказа </h1>
</div>
<div class=\"bg_t\">
    <div class=\"container p-3\">
        <div class=\"row\">
            <div class=\"col-6 col-sm-8 col-md-9 col-lg-10\">
            <h4 class=\"p-3\"> Заказ оформлен </h4> 
            </div>
            <div class=\"col-6 col-sm-4 col-md-3 col-lg-2 my-auto p-0 text-center\">
                <form action=\"/project/account\"> 
                    <button class=\"btn btn-oytline-success my-2 my-sm-0 btn-dark\"> Личный кабинет </button>
                </form>
            </div> 
        </div>
        <div class=\"mx-auto container w-100 m-0\">
            <main class=\"col-12  p-0 pt-3\">
                
                <div class=\"row col-12 w-100 m-0 p-0\">
                    Информация о статусе Ваших заказов будет доступна на странице истории заказов в Личном кабинете.
                </div>
            </main>     
        </div>
</div>", "C:\\wamp64\\www\\project/themes/mine/partials/ordersadd.htm", "");
    }
}
