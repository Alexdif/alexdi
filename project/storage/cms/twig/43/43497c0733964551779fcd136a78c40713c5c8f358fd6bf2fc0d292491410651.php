<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* C:\wamp64\www\project/themes/mine/partials/product.htm */
class __TwigTemplate_7d09208377572f853296e6754c66ae3a352f79322223a84d3b5ab4d5991c88d8 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        $context["record"] = twig_get_attribute($this->env, $this->source, ($context["builderDetails"] ?? null), "record", [], "any", false, false, false, 1);
        // line 2
        $context["displayColumn"] = twig_get_attribute($this->env, $this->source, ($context["builderDetails"] ?? null), "displayColumn", [], "any", false, false, false, 2);
        // line 3
        $context["notFoundMessage"] = twig_get_attribute($this->env, $this->source, ($context["builderDetails"] ?? null), "notFoundMessage", [], "any", false, false, false, 3);
        // line 4
        echo "
";
        // line 5
        if (($context["record"] ?? null)) {
            // line 6
            echo "<Div class=\"container h-100\">
    <div class=\"row h-100 my-auto\"> 
        
            <div class=\"col-md-8 \">
                <div class=\"row\">
                        <div class=\"col-md-12 pt-3 pb-md-3\"> <img src=\"";
            // line 11
            echo $this->extensions['System\Twig\Extension']->mediaFilter(twig_get_attribute($this->env, $this->source, ($context["record"] ?? null), "photo", [], "any", false, false, false, 11));
            echo "\"  alt=\"product\" class=\"d-block w-100 demo\"> </div> 
                </div>
                
            </div>
            <div class=\"col-md-4 pt-md-5 pt-3\">
                ";
            // line 16
            if ( !($context["user"] ?? null)) {
                // line 17
                echo "                    <form method=\"GET\" action=\"";
                echo url("account");
                echo "\" >
                ";
            } else {
                // line 19
                echo "                    <form method=\"post\" id=\"my_form\">
                ";
            }
            // line 21
            echo "                        ";
            $context["nal"] = 0;
            // line 22
            echo "                        ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["record"] ?? null), "availabilities", [], "any", false, false, false, 22));
            foreach ($context['_seq'] as $context["_key"] => $context["s"]) {
                echo " 
                            ";
                // line 23
                if ((twig_get_attribute($this->env, $this->source, $context["s"], "quantity", [], "any", false, false, false, 23) > 0)) {
                    // line 24
                    echo "                                ";
                    $context["nal"] = (($context["nal"] ?? null) + 1);
                    // line 25
                    echo "                            ";
                } else {
                    // line 26
                    echo "    
                            ";
                }
                // line 28
                echo "                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['s'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo " 
                    <div class=\"col-12\"> 
                        <h2 class=\"text-center\"> ";
            // line 30
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["record"] ?? null), "name", [], "any", false, false, false, 30), "html", null, true);
            echo "</h2>
                        ";
            // line 31
            if ((twig_get_attribute($this->env, $this->source, ($context["record"] ?? null), "discount", [], "any", false, false, false, 31) > 0)) {
                echo "<h3 class=\"text-left m-0\"> <s> ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["record"] ?? null), "price", [], "any", false, false, false, 31), "html", null, true);
                echo " ₽ </s> </h3>
                        <h3 class=\"text-left m-0\"> ";
                // line 32
                echo twig_escape_filter($this->env, (twig_get_attribute($this->env, $this->source, ($context["record"] ?? null), "price", [], "any", false, false, false, 32) * (1 - (twig_get_attribute($this->env, $this->source, ($context["record"] ?? null), "discount", [], "any", false, false, false, 32) / 100))), "html", null, true);
                echo " ₽ </h3> <p> Цена со скидкой ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["record"] ?? null), "discount", [], "any", false, false, false, 32), "html", null, true);
                echo " % </p>";
            } elseif ((twig_get_attribute($this->env, $this->source, ($context["record"] ?? null), "discount", [], "any", false, false, false, 32) == 0)) {
                echo "<h3> ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["record"] ?? null), "price", [], "any", false, false, false, 32), "html", null, true);
                echo "  ₽ </h3>";
            }
            // line 33
            echo "                        
                            <ul class=\" p-0 m-0\">
                                <li> Цвет: ";
            // line 35
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["record"] ?? null), "color", [], "any", false, false, false, 35), "html", null, true);
            echo " </li>
                                <li> Страна: ";
            // line 36
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["record"] ?? null), "Country", [], "any", false, false, false, 36), "country", [], "any", false, false, false, 36), "html", null, true);
            echo " </li>
                                ";
            // line 37
            if ((($context["nal"] ?? null) > 0)) {
                // line 38
                echo "                                <li> Размер:
                                <select class=\"btn-dark\" name=\"size\">    
                                ";
                // line 40
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["record"] ?? null), "availabilities", [], "any", false, false, false, 40));
                foreach ($context['_seq'] as $context["_key"] => $context["s"]) {
                    echo " 
                                ";
                    // line 41
                    if ((twig_get_attribute($this->env, $this->source, $context["s"], "quantity", [], "any", false, false, false, 41) > 0)) {
                        // line 42
                        echo "                                <option value=\"";
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["s"], "size", [], "any", false, false, false, 42), "html", null, true);
                        echo "\">";
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["s"], "size", [], "any", false, false, false, 42), "html", null, true);
                        echo " ";
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["s"], "quantity", [], "any", false, false, false, 42), "html", null, true);
                        echo " шт.</option> 
                                ";
                    }
                    // line 44
                    echo "                                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['s'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                echo " 
                                </select> 
                                </li>
                                ";
            } else {
                // line 48
                echo "                                <li class=\"danger-text\"> Товара нет в наличии </li>
                                ";
            }
            // line 50
            echo "                            </ul>
                </div>
                <div class=\"col-12  pt-3 text-center\">
                    ";
            // line 53
            if ((($context["nal"] ?? null) > 0)) {
                echo "             
                    <input type=\"submit\" 
                    class=\"btn btn-oytline-success my-2 my-sm-0 btn-dark\" name=\"btn1\" id=\"button\" value=\"Добавить в корзину\"/>
                    ";
            } else {
                // line 57
                echo "                    ";
            }
            echo "   
                </div>
            </form>
            
                <div class=\"col-12 pt-3\"> 
                    <h3> Описание товара </h3>
                    <p>";
            // line 63
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["record"] ?? null), "description", [], "any", false, false, false, 63), "html", null, true);
            echo "</p>
                </div>
            </div>
            
    </div>
    </div>
    ";
        } else {
            // line 70
            echo "    <h4 class=\"p-5\">";
            echo twig_escape_filter($this->env, ($context["notFoundMessage"] ?? null), "html", null, true);
            echo "</h4>
";
        }
        // line 72
        echo "
<script>
    \$(document).ready(function() {
    \$(\"#exampleModalCenter\").modal(\"show\");
    \$('img.demo').loupe();
    });

    var \$myForm = \$(\"#my_form\");
    \$myForm.submit(function(){
    \$myForm.submit(function(){
        return false;
    });
});
</script>
<script type=\"text/javascript\" src=\"";
        // line 86
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/javascript/alexdi/product.js");
        echo "\" ></script>  
<script type=\"text/javascript\" src=\"";
        // line 87
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/javascript/alexdi/loupe.min.js");
        echo "\" ></script>";
    }

    public function getTemplateName()
    {
        return "C:\\wamp64\\www\\project/themes/mine/partials/product.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  233 => 87,  229 => 86,  213 => 72,  207 => 70,  197 => 63,  187 => 57,  180 => 53,  175 => 50,  171 => 48,  160 => 44,  150 => 42,  148 => 41,  142 => 40,  138 => 38,  136 => 37,  132 => 36,  128 => 35,  124 => 33,  114 => 32,  108 => 31,  104 => 30,  95 => 28,  91 => 26,  88 => 25,  85 => 24,  83 => 23,  76 => 22,  73 => 21,  69 => 19,  63 => 17,  61 => 16,  53 => 11,  46 => 6,  44 => 5,  41 => 4,  39 => 3,  37 => 2,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% set record = builderDetails.record %}
{% set displayColumn = builderDetails.displayColumn %}
{% set notFoundMessage = builderDetails.notFoundMessage %}

{% if record %}
<Div class=\"container h-100\">
    <div class=\"row h-100 my-auto\"> 
        
            <div class=\"col-md-8 \">
                <div class=\"row\">
                        <div class=\"col-md-12 pt-3 pb-md-3\"> <img src=\"{{ record.photo | media }}\"  alt=\"product\" class=\"d-block w-100 demo\"> </div> 
                </div>
                
            </div>
            <div class=\"col-md-4 pt-md-5 pt-3\">
                {% if not user %}
                    <form method=\"GET\" action=\"{{ url('account')}}\" >
                {% else %}
                    <form method=\"post\" id=\"my_form\">
                {% endif %}
                        {% set nal=0 %}
                        {%for s in record.availabilities %} 
                            {% if s.quantity>0 %}
                                {% set nal=nal+1 %}
                            {% else %}
    
                            {% endif %}
                        {% endfor %} 
                    <div class=\"col-12\"> 
                        <h2 class=\"text-center\"> {{ record.name }}</h2>
                        {% if record.discount > 0 %}<h3 class=\"text-left m-0\"> <s> {{ record.price }} ₽ </s> </h3>
                        <h3 class=\"text-left m-0\"> {{ record.price*(1-(record.discount/100)) }} ₽ </h3> <p> Цена со скидкой {{ record.discount}} % </p>{% elseif record.discount == 0 %}<h3> {{ record.price }}  ₽ </h3>{% endif %}
                        
                            <ul class=\" p-0 m-0\">
                                <li> Цвет: {{ record.color }} </li>
                                <li> Страна: {{ record.Country.country }} </li>
                                {% if nal>0%}
                                <li> Размер:
                                <select class=\"btn-dark\" name=\"size\">    
                                {%for s in record.availabilities %} 
                                {% if s.quantity>0 %}
                                <option value=\"{{s.size}}\">{{ s.size }} {{s.quantity}} шт.</option> 
                                {% endif %}
                                {% endfor %} 
                                </select> 
                                </li>
                                {% else %}
                                <li class=\"danger-text\"> Товара нет в наличии </li>
                                {% endif %}
                            </ul>
                </div>
                <div class=\"col-12  pt-3 text-center\">
                    {% if nal>0%}             
                    <input type=\"submit\" 
                    class=\"btn btn-oytline-success my-2 my-sm-0 btn-dark\" name=\"btn1\" id=\"button\" value=\"Добавить в корзину\"/>
                    {% else %}
                    {% endif %}   
                </div>
            </form>
            
                <div class=\"col-12 pt-3\"> 
                    <h3> Описание товара </h3>
                    <p>{{ record.description }}</p>
                </div>
            </div>
            
    </div>
    </div>
    {% else %}
    <h4 class=\"p-5\">{{ notFoundMessage }}</h4>
{% endif %}

<script>
    \$(document).ready(function() {
    \$(\"#exampleModalCenter\").modal(\"show\");
    \$('img.demo').loupe();
    });

    var \$myForm = \$(\"#my_form\");
    \$myForm.submit(function(){
    \$myForm.submit(function(){
        return false;
    });
});
</script>
<script type=\"text/javascript\" src=\"{{ 'assets/javascript/alexdi/product.js'|theme }}\" ></script>  
<script type=\"text/javascript\" src=\"{{ 'assets/javascript/alexdi/loupe.min.js'|theme }}\" ></script>", "C:\\wamp64\\www\\project/themes/mine/partials/product.htm", "");
    }
}
