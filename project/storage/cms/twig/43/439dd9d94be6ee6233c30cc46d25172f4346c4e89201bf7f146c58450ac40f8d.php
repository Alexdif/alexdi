<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* C:\wamp64\www\project/themes/mine/partials/footer.htm */
class __TwigTemplate_7168af7c4994e6e13e1ed571185759d32f8f75c1fc752d1bd7d3802594e18c5f extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<div class=\"main-footer bg_color_firm light-color\" >
    <div class=\"container\">
        <div class=\"row\">
            <div class =\"col-12 col-md-6 p-0 text-left text-md-center order-1 order-md-2\">
                <ul>
                    <li class=\"pt-3 light-link\"><h3 class=\"p_1\"> <a href=\"/project/about_us\">  О нас </a> </h3></li>
                    <li class=\"light-link\"> <a href=\"/project/blog\"> Новости </a>  </li> 
                    <li class=\"light-link\"> <a href=\"/project/feedback\"> Обратная связь </a></li> 
                </ul>
            </div>
            <div class =\"col-12 col-md-6 p-0 text-left order-2 order-md-1\">
                <ul>
                    <li class=\"pt-3\"><h3 class=\"p_1\"> Контакты </h3></li>
                    <li>Наш адрес: г. Москва, ул. Ленина, д. 2А.  </li>
                    <li>Наш телефон: 8 (916) 78-29-738 </li> 
                    <li>Все права защищены © </li> 
                </ul>
            </div>
            <div class =\"col-12 p-0 text-center order-3\">
                <ul class=\"list-inline\">
                    <li class=\"pt-3 list-inline-item\">
                        <a href=\"http://vk.com\"><img src=\"";
        // line 22
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/vk.png");
        echo "\"  alt=\"ВКонтакте\" class=\"d-block img-social\"></a>   
                    </li>  
                    <li class=\"pt-3 list-inline-item\">
                        <a href=\"https://www.instagram.com/zhestko/\"><img src=\"";
        // line 25
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/instagram.png");
        echo "\"  alt=\"ВКонтакте\" class=\"d-block img-social\"></a>   
                    </li>  
                    <li class=\"pt-3 list-inline-item\">
                        <a href=\"tel:89167829738\"><img src=\"";
        // line 28
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/whatsapp.png");
        echo "\" alt=\"ВКонтакте\" class=\"d-block img-social\"></a>   
                    </li>  
                </ul>
            </div>
        </div>  
    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "C:\\wamp64\\www\\project/themes/mine/partials/footer.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  70 => 28,  64 => 25,  58 => 22,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"main-footer bg_color_firm light-color\" >
    <div class=\"container\">
        <div class=\"row\">
            <div class =\"col-12 col-md-6 p-0 text-left text-md-center order-1 order-md-2\">
                <ul>
                    <li class=\"pt-3 light-link\"><h3 class=\"p_1\"> <a href=\"/project/about_us\">  О нас </a> </h3></li>
                    <li class=\"light-link\"> <a href=\"/project/blog\"> Новости </a>  </li> 
                    <li class=\"light-link\"> <a href=\"/project/feedback\"> Обратная связь </a></li> 
                </ul>
            </div>
            <div class =\"col-12 col-md-6 p-0 text-left order-2 order-md-1\">
                <ul>
                    <li class=\"pt-3\"><h3 class=\"p_1\"> Контакты </h3></li>
                    <li>Наш адрес: г. Москва, ул. Ленина, д. 2А.  </li>
                    <li>Наш телефон: 8 (916) 78-29-738 </li> 
                    <li>Все права защищены © </li> 
                </ul>
            </div>
            <div class =\"col-12 p-0 text-center order-3\">
                <ul class=\"list-inline\">
                    <li class=\"pt-3 list-inline-item\">
                        <a href=\"http://vk.com\"><img src=\"{{ 'assets/images/vk.png'| theme }}\"  alt=\"ВКонтакте\" class=\"d-block img-social\"></a>   
                    </li>  
                    <li class=\"pt-3 list-inline-item\">
                        <a href=\"https://www.instagram.com/zhestko/\"><img src=\"{{ 'assets/images/instagram.png'| theme }}\"  alt=\"ВКонтакте\" class=\"d-block img-social\"></a>   
                    </li>  
                    <li class=\"pt-3 list-inline-item\">
                        <a href=\"tel:89167829738\"><img src=\"{{ 'assets/images/whatsapp.png'| theme }}\" alt=\"ВКонтакте\" class=\"d-block img-social\"></a>   
                    </li>  
                </ul>
            </div>
        </div>  
    </div>
</div>", "C:\\wamp64\\www\\project/themes/mine/partials/footer.htm", "");
    }
}
