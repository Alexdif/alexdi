<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* C:\wamp64\www\project/themes/mine/pages/goods.htm */
class __TwigTemplate_12b608584c9257791c2acfc028db9eb5841f0ce30b740ed496b50c5dcac9119a extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        $context['__cms_partial_params'] = [];
        $context['__cms_partial_params']['way1'] = "1-f.png"        ;
        $context['__cms_partial_params']['way2'] = "2-f.png"        ;
        $context['__cms_partial_params']['way3'] = "3-f.png"        ;
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("carousel_all_width"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 2
        echo "
<div class=\"row w-100 m-0\">
  <div class=\"col-md-3 col-lg-2 bg_t\" >
    ";
        // line 5
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("left_menu"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        echo " 
  </div>
  <main class=\"col-md-9 col-lg-10 p-0 pt-3\">
    <div class=\"row text-center col-lg-10 w-100 m-0 p-0\">
      ";
        // line 9
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("products_menu"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 10
        echo "    </div>
  </main>
</div>
";
        // line 13
        $_minify = System\Classes\CombineAssets::instance()->useMinify;
        if ($_minify) {
            echo '<script src="'. Request::getBasePath()
                    .'/modules/system/assets/js/framework.combined-min.js"></script>'.PHP_EOL;
        }
        else {
            echo '<script src="'. Request::getBasePath()
                    .'/modules/system/assets/js/framework.js"></script>'.PHP_EOL;
            echo '<script src="'. Request::getBasePath()
                    .'/modules/system/assets/js/framework.extras.js"></script>'.PHP_EOL;
        }
        echo '<link rel="stylesheet" property="stylesheet" href="'. Request::getBasePath()
                    .'/modules/system/assets/css/framework.extras'.($_minify ? '-min' : '').'.css">'.PHP_EOL;
        unset($_minify);
        // line 14
        echo $this->env->getExtension('Cms\Twig\Extension')->assetsFunction('js');
        echo $this->env->getExtension('Cms\Twig\Extension')->displayBlock('scripts');
    }

    public function getTemplateName()
    {
        return "C:\\wamp64\\www\\project/themes/mine/pages/goods.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  80 => 14,  65 => 13,  60 => 10,  56 => 9,  47 => 5,  42 => 2,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% partial \"carousel_all_width\" way1=\"1-f.png\" way2=\"2-f.png\" way3=\"3-f.png\" %}

<div class=\"row w-100 m-0\">
  <div class=\"col-md-3 col-lg-2 bg_t\" >
    {% partial \"left_menu\" %} 
  </div>
  <main class=\"col-md-9 col-lg-10 p-0 pt-3\">
    <div class=\"row text-center col-lg-10 w-100 m-0 p-0\">
      {% partial \"products_menu\" %}
    </div>
  </main>
</div>
{% framework extras %}
{% scripts %}", "C:\\wamp64\\www\\project/themes/mine/pages/goods.htm", "");
    }
}
