<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* C:\wamp64\www\project/themes/mine/partials/account/deactivate_link.htm */
class __TwigTemplate_9fedc92df3b14bc627f1bf4ca36cca3c49d3caf859726502863e89809ea126a2 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<a class=\"dark-link\"
    href=\"javascript:;\"
    onclick=\"\$('#accountDeactivateForm').toggle()\">
    Удалить аккаунт
</a>

<div id=\"accountDeactivateForm\" style=\"display: none\">
    ";
        // line 8
        echo call_user_func_array($this->env->getFunction('form_ajax')->getCallable(), ["ajax", "onDeactivate"]);
        echo "
        <hr />
        <h3>Вы действительно хотите удалить акаунт?</h3>
        <p>
            Ваша учетная запись будет отключена, а ваши данные удалены с сайта. Вы можете активировать свою учетную запись в любое время, выполнив вход.
        </p>
        <div class=\"form-group\">
            <label for=\"accountDeletePassword\">Чтобы продолжить, введите ваш пароль:</label>
            <input name=\"password\" type=\"password\" class=\"form-control\" id=\"accountDeletePassword\" />
        </div>
        <button type=\"submit\" class=\"btn btn-secondary\">
            Удалить аккаунт
        </button>
        <a class=\"pl-3 dark-link\"
            href=\"javascript:;\"
            onclick=\"\$('#accountDeactivateForm').toggle()\">
            Я передумал
        </a>
    ";
        // line 26
        echo call_user_func_array($this->env->getFunction('form_close')->getCallable(), ["close"]);
        echo "
</div>";
    }

    public function getTemplateName()
    {
        return "C:\\wamp64\\www\\project/themes/mine/partials/account/deactivate_link.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  65 => 26,  44 => 8,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<a class=\"dark-link\"
    href=\"javascript:;\"
    onclick=\"\$('#accountDeactivateForm').toggle()\">
    Удалить аккаунт
</a>

<div id=\"accountDeactivateForm\" style=\"display: none\">
    {{ form_ajax('onDeactivate') }}
        <hr />
        <h3>Вы действительно хотите удалить акаунт?</h3>
        <p>
            Ваша учетная запись будет отключена, а ваши данные удалены с сайта. Вы можете активировать свою учетную запись в любое время, выполнив вход.
        </p>
        <div class=\"form-group\">
            <label for=\"accountDeletePassword\">Чтобы продолжить, введите ваш пароль:</label>
            <input name=\"password\" type=\"password\" class=\"form-control\" id=\"accountDeletePassword\" />
        </div>
        <button type=\"submit\" class=\"btn btn-secondary\">
            Удалить аккаунт
        </button>
        <a class=\"pl-3 dark-link\"
            href=\"javascript:;\"
            onclick=\"\$('#accountDeactivateForm').toggle()\">
            Я передумал
        </a>
    {{ form_close() }}
</div>", "C:\\wamp64\\www\\project/themes/mine/partials/account/deactivate_link.htm", "");
    }
}
