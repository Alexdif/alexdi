<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* C:\wamp64\www\project/themes/mine/pages/registration.htm */
class __TwigTemplate_e245a9b5561d4b49e9fb870de9b0b7f4f86213b7f21b139e9f5eb32fe8c7f3b3 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<div class=\"container\">
    <div class=\"row\">
            ";
        // line 3
        if ( !($context["user"] ?? null)) {
            // line 4
            echo "<div class=\"col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 pt-md-5 p-3\">
    ";
            // line 5
            $context['__cms_partial_params'] = [];
            echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction((($context["account"] ?? null) . "::register")            , $context['__cms_partial_params']            , true            );
            unset($context['__cms_partial_params']);
            // line 6
            echo "</div>
";
        } else {
            // line 8
            echo "
        ";
        }
        // line 9
        echo " 
</div>
</div>
<script type=\"text/javascript\" src=\"";
        // line 12
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/javascript/jquery.maskedinput-master/src/jquery.maskedinput.js");
        echo "\" ></script>  
<script type=\"text/javascript\" src=\"";
        // line 13
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/javascript/alexdi/account.js");
        echo "\" ></script>";
    }

    public function getTemplateName()
    {
        return "C:\\wamp64\\www\\project/themes/mine/pages/registration.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  65 => 13,  61 => 12,  56 => 9,  52 => 8,  48 => 6,  44 => 5,  41 => 4,  39 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"container\">
    <div class=\"row\">
            {% if not user %}
<div class=\"col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 pt-md-5 p-3\">
    {% partial account ~ '::register' %}
</div>
{% else %}

        {% endif %} 
</div>
</div>
<script type=\"text/javascript\" src=\"{{ 'assets/javascript/jquery.maskedinput-master/src/jquery.maskedinput.js'|theme }}\" ></script>  
<script type=\"text/javascript\" src=\"{{ 'assets/javascript/alexdi/account.js'|theme }}\" ></script>", "C:\\wamp64\\www\\project/themes/mine/pages/registration.htm", "");
    }
}
