<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* C:\wamp64\www\project/themes/mine/partials/account/register.htm */
class __TwigTemplate_91b530b7d7c234489a8776b93c9dcebe2485559562020f342a357f19640c9168 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        if (($context["canRegister"] ?? null)) {
            // line 2
            echo "    <h3>Регистрация</h3>
    <form
        data-request=\"onRegister\">
        <div class=\"lch-container row\">
                <div class=\"form-group  col-12 col-xl-6\">
                        <label for=\"registerName\">Имя</label>
                        <input
                            name=\"name\"
                            type=\"text\"
                            class=\"form-control\"
                            id=\"registerName\" required 
                            placeholder=\"Введите ваше имя\" />
                    </div>
            
                    <div class=\"form-group  col-12 col-xl-6\">
                        <label for=\"registerSurname\">Фамилия</label>
                        <input
                            name=\"surname\"
                            type=\"text\"
                            class=\"form-control\"
                            id=\"registerSurname\"
                            placeholder=\"Введите вашу фамилию\" required  />
                    </div>
            
                    <div class=\"form-group col-12\">
                        <label for=\"registerEmail\">Email</label>
                        <input
                            name=\"email\"
                            type=\"email\"
                            class=\"form-control\"
                            id=\"registerEmail\" required 
                            placeholder=\"Введите вашу почту\" />
                    </div>
                    ";
            // line 35
            if ((($context["loginAttribute"] ?? null) == "username")) {
                // line 36
                echo "                    <div class=\"form-group\">
                        <label for=\"registerUsername\">Логин</label>
                        <input
                            name=\"username\"
                            type=\"text\"
                            class=\"form-control\"
                            id=\"registerUsername\"
                            placeholder=\"Введите логин\" />
                    </div>
                ";
            }
            // line 46
            echo "                
        <div class=\"form-group col-12 \">
                <label for=\"registerPassword\">Пароль</label>
                <input
                    name=\"password\"
                    type=\"password\"
                    class=\"form-control\"
                    id=\"registerPassword\"
                    placeholder=\"Введите пароль\" />
            </div>

        </div>

        <div class=\"lch-container row\">
                <div class=\"form-group col-12\">
                        <label>Aдресс</label>
                        <textarea name=\"address\" type=\"text\" class=\"form-control\" id=\"regAddress\" required></textarea>
                    </div>
            
                    <div class=\"form-group col-12 col-xl-3\">
                            <label>Индекс</label>
                            <input name=\"index\" type=\"text\" class=\"form-control\" id=\"regIndex\" required>
                        </div>
            
                    <div class=\"form-group col-12 col-xl-9\">
                        <label>Телефон</label>
                        <input name=\"telephone\" type=\"text\" class=\"form-control\" id=\"regTelephone\" required placeholder=\"Введите ваш телефон\">
                    </div>
        </div>  

        <button type=\"submit\" class=\"btn btn-dark\">Регистрация</button>
        <a class=\"dark-link pl-3\" href=\"/project/account\"> Назад </a>

    </form>
";
        } else {
            // line 81
            echo "    <!-- Registration is disabled. -->
";
        }
    }

    public function getTemplateName()
    {
        return "C:\\wamp64\\www\\project/themes/mine/partials/account/register.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  123 => 81,  86 => 46,  74 => 36,  72 => 35,  37 => 2,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% if canRegister %}
    <h3>Регистрация</h3>
    <form
        data-request=\"onRegister\">
        <div class=\"lch-container row\">
                <div class=\"form-group  col-12 col-xl-6\">
                        <label for=\"registerName\">Имя</label>
                        <input
                            name=\"name\"
                            type=\"text\"
                            class=\"form-control\"
                            id=\"registerName\" required 
                            placeholder=\"Введите ваше имя\" />
                    </div>
            
                    <div class=\"form-group  col-12 col-xl-6\">
                        <label for=\"registerSurname\">Фамилия</label>
                        <input
                            name=\"surname\"
                            type=\"text\"
                            class=\"form-control\"
                            id=\"registerSurname\"
                            placeholder=\"Введите вашу фамилию\" required  />
                    </div>
            
                    <div class=\"form-group col-12\">
                        <label for=\"registerEmail\">Email</label>
                        <input
                            name=\"email\"
                            type=\"email\"
                            class=\"form-control\"
                            id=\"registerEmail\" required 
                            placeholder=\"Введите вашу почту\" />
                    </div>
                    {% if loginAttribute == \"username\" %}
                    <div class=\"form-group\">
                        <label for=\"registerUsername\">Логин</label>
                        <input
                            name=\"username\"
                            type=\"text\"
                            class=\"form-control\"
                            id=\"registerUsername\"
                            placeholder=\"Введите логин\" />
                    </div>
                {% endif %}
                
        <div class=\"form-group col-12 \">
                <label for=\"registerPassword\">Пароль</label>
                <input
                    name=\"password\"
                    type=\"password\"
                    class=\"form-control\"
                    id=\"registerPassword\"
                    placeholder=\"Введите пароль\" />
            </div>

        </div>

        <div class=\"lch-container row\">
                <div class=\"form-group col-12\">
                        <label>Aдресс</label>
                        <textarea name=\"address\" type=\"text\" class=\"form-control\" id=\"regAddress\" required></textarea>
                    </div>
            
                    <div class=\"form-group col-12 col-xl-3\">
                            <label>Индекс</label>
                            <input name=\"index\" type=\"text\" class=\"form-control\" id=\"regIndex\" required>
                        </div>
            
                    <div class=\"form-group col-12 col-xl-9\">
                        <label>Телефон</label>
                        <input name=\"telephone\" type=\"text\" class=\"form-control\" id=\"regTelephone\" required placeholder=\"Введите ваш телефон\">
                    </div>
        </div>  

        <button type=\"submit\" class=\"btn btn-dark\">Регистрация</button>
        <a class=\"dark-link pl-3\" href=\"/project/account\"> Назад </a>

    </form>
{% else %}
    <!-- Registration is disabled. -->
{% endif %}", "C:\\wamp64\\www\\project/themes/mine/partials/account/register.htm", "");
    }
}
