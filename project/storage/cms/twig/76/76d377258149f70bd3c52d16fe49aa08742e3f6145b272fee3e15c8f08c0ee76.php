<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* C:\wamp64\www\project/themes/mine/partials/products_menu.htm */
class __TwigTemplate_abb3d292260c1fbd7f0ab282228bcb89fbd0f7b67b4e521ff1f66b1d5f406d09 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        $context["records"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "records", [], "any", false, false, false, 1);
        // line 2
        $context["displayColumn"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "displayColumn", [], "any", false, false, false, 2);
        // line 3
        $context["noRecordsMessage"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "noRecordsMessage", [], "any", false, false, false, 3);
        // line 4
        $context["detailsPage"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "detailsPage", [], "any", false, false, false, 4);
        // line 5
        $context["detailsKeyColumn"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "detailsKeyColumn", [], "any", false, false, false, 5);
        // line 6
        $context["detailsUrlParameter"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "detailsUrlParameter", [], "any", false, false, false, 6);
        // line 7
        echo "

";
        // line 9
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["records"] ?? null));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["record"]) {
            // line 10
            echo "
";
            // line 11
            ob_start(function () { return ''; });
            // line 12
            echo "
    <div class=\"col-6 col-md-4 col-lg-4 col-xl-3 mb-3\" >
        <div class=\"col-12 pb-3 bg_t dark-link-1 item h-100\" > 
                
            ";
            // line 16
            if (($context["detailsPage"] ?? null)) {
                // line 17
                echo "                <a href=\"";
                echo $this->extensions['Cms\Twig\Extension']->pageFilter(($context["detailsPage"] ?? null), [($context["detailsUrlParameter"] ?? null) => twig_get_attribute($this->env, $this->source, $context["record"], ($context["detailsKeyColumn"] ?? null), [], "any", false, false, false, 17)]);
                echo "\">
            ";
            }
            // line 19
            echo "            <div>
                <img src=\"";
            // line 20
            echo $this->extensions['System\Twig\Extension']->mediaFilter(twig_get_attribute($this->env, $this->source, $context["record"], "photo", [], "any", false, false, false, 20));
            echo "\" alt=\"Изображение товара\" class=\"w-100\">  
            </div>
            <div class=\"pt-3 dname\">
                <h3> ";
            // line 23
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["record"], "name", [], "any", false, false, false, 23), "html", null, true);
            echo " </h3> 
            </div>       
            <hr>
            <div>
                <p class=\"text-left\"> ";
            // line 27
            echo call_user_func_array($this->env->getFunction('html_limit')->getCallable(), ["limit", twig_get_attribute($this->env, $this->source, $context["record"], "description", [], "any", false, false, false, 27), 40]);
            echo "..<br></p> 
            </div>
            <div class=\"p-0 m-0 text-left\">
                ";
            // line 30
            $context["nal"] = 0;
            // line 31
            echo "                ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["record"], "availabilities", [], "any", false, false, false, 31));
            foreach ($context['_seq'] as $context["_key"] => $context["s"]) {
                echo " 
                    ";
                // line 32
                if ((twig_get_attribute($this->env, $this->source, $context["s"], "quantity", [], "any", false, false, false, 32) > 0)) {
                    // line 33
                    echo "                        ";
                    $context["nal"] = (($context["nal"] ?? null) + 1);
                    // line 34
                    echo "                    ";
                } else {
                    // line 35
                    echo "                
                    ";
                }
                // line 37
                echo "                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['s'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 38
            echo "                    ";
            if ((($context["nal"] ?? null) > 0)) {
                echo " 
                    <p>  ";
                // line 39
                if ((twig_get_attribute($this->env, $this->source, $context["record"], "discount", [], "any", false, false, false, 39) > 0)) {
                    echo "<s> ";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["record"], "price", [], "any", false, false, false, 39), "html", null, true);
                    echo " ₽ </s> <br>
                    ";
                    // line 40
                    echo twig_escape_filter($this->env, (twig_get_attribute($this->env, $this->source, $context["record"], "price", [], "any", false, false, false, 40) * (1 - (twig_get_attribute($this->env, $this->source, $context["record"], "discount", [], "any", false, false, false, 40) / 100))), "html", null, true);
                    echo " ₽ ";
                } elseif ((twig_get_attribute($this->env, $this->source, $context["record"], "discount", [], "any", false, false, false, 40) == 0)) {
                    echo " ";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["record"], "price", [], "any", false, false, false, 40), "html", null, true);
                    echo " ₽";
                }
                echo " 
                    </p> 
                    ";
            } else {
                // line 43
                echo "                    <p class=\"danger-text\">
                        Нет в наличии
                    </p>
                    ";
            }
            // line 47
            echo "
            </div>
            ";
            // line 49
            if (($context["detailsPage"] ?? null)) {
                // line 50
                echo "        </a>
            ";
            }
            // line 52
            echo "        </div>
    </div> 


";
            echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 58
            echo "<h4 class=\"p-3\"> ";
            echo twig_escape_filter($this->env, ($context["noRecordsMessage"] ?? null), "html", null, true);
            echo " </h4>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['record'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 60
        echo "<div class=\"col-12 col-sm-12 col-md-12 col-lg-12 py-3\"> 
    ";
        // line 61
        if ((twig_get_attribute($this->env, $this->source, ($context["records"] ?? null), "lastPage", [], "any", false, false, false, 61) > 1)) {
            // line 62
            echo "        <ul class=\"pagination dark-link justify-content-center\">
            ";
            // line 63
            if ((twig_get_attribute($this->env, $this->source, ($context["records"] ?? null), "currentPage", [], "any", false, false, false, 63) > 1)) {
                // line 64
                echo "                <li>
                    <a class=\"page-link\" href=\"";
                // line 65
                echo $this->extensions['Cms\Twig\Extension']->pageFilter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "page", [], "any", false, false, false, 65), "baseFileName", [], "any", false, false, false, 65), [($context["pageParam"] ?? null) => (twig_get_attribute($this->env, $this->source, ($context["records"] ?? null), "currentPage", [], "any", false, false, false, 65) - 1)]);
                echo twig_escape_filter($this->env, ($context["paginats"] ?? null), "html", null, true);
                echo "\">
                            &#60
                    </a>
                </li>
            ";
            }
            // line 70
            echo "            ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(range(1, twig_get_attribute($this->env, $this->source, ($context["records"] ?? null), "lastPage", [], "any", false, false, false, 70)));
            foreach ($context['_seq'] as $context["_key"] => $context["page"]) {
                // line 71
                echo "                <li class=\" ";
                echo (((twig_get_attribute($this->env, $this->source, ($context["records"] ?? null), "currentPage", [], "any", false, false, false, 71) == $context["page"])) ? ("active") : (null));
                echo "\">
                    <a class=\"page-link ";
                // line 72
                echo (((twig_get_attribute($this->env, $this->source, ($context["records"] ?? null), "currentPage", [], "any", false, false, false, 72) == $context["page"])) ? ("active bg_d") : (null));
                echo "\" href=\"";
                echo $this->extensions['Cms\Twig\Extension']->pageFilter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "page", [], "any", false, false, false, 72), "baseFileName", [], "any", false, false, false, 72), [($context["pageParam"] ?? null) => $context["page"]]);
                echo twig_escape_filter($this->env, ($context["paginats"] ?? null), "html", null, true);
                echo "\"><lable class=\"";
                echo (((twig_get_attribute($this->env, $this->source, ($context["records"] ?? null), "currentPage", [], "any", false, false, false, 72) == $context["page"])) ? ("light-color") : (null));
                echo "\">";
                echo twig_escape_filter($this->env, $context["page"], "html", null, true);
                echo "</lable></a>
                </li>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['page'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 75
            echo "            ";
            if ((twig_get_attribute($this->env, $this->source, ($context["records"] ?? null), "lastPage", [], "any", false, false, false, 75) > twig_get_attribute($this->env, $this->source, ($context["records"] ?? null), "currentPage", [], "any", false, false, false, 75))) {
                // line 76
                echo "                <li><a class=\"page-link\" href=\"";
                echo $this->extensions['Cms\Twig\Extension']->pageFilter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "page", [], "any", false, false, false, 76), "baseFileName", [], "any", false, false, false, 76), [($context["pageParam"] ?? null) => (twig_get_attribute($this->env, $this->source, ($context["records"] ?? null), "currentPage", [], "any", false, false, false, 76) + 1)]);
                echo twig_escape_filter($this->env, ($context["paginats"] ?? null), "html", null, true);
                echo "\">&#62</a></li>
            ";
            }
            // line 78
            echo "        </ul>
    ";
        }
        // line 80
        echo "</div>";
    }

    public function getTemplateName()
    {
        return "C:\\wamp64\\www\\project/themes/mine/partials/products_menu.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  244 => 80,  240 => 78,  233 => 76,  230 => 75,  214 => 72,  209 => 71,  204 => 70,  195 => 65,  192 => 64,  190 => 63,  187 => 62,  185 => 61,  182 => 60,  173 => 58,  163 => 52,  159 => 50,  157 => 49,  153 => 47,  147 => 43,  135 => 40,  129 => 39,  124 => 38,  118 => 37,  114 => 35,  111 => 34,  108 => 33,  106 => 32,  99 => 31,  97 => 30,  91 => 27,  84 => 23,  78 => 20,  75 => 19,  69 => 17,  67 => 16,  61 => 12,  59 => 11,  56 => 10,  51 => 9,  47 => 7,  45 => 6,  43 => 5,  41 => 4,  39 => 3,  37 => 2,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% set records = builderList.records %}
{% set displayColumn = builderList.displayColumn %}
{% set noRecordsMessage = builderList.noRecordsMessage %}
{% set detailsPage = builderList.detailsPage %}
{% set detailsKeyColumn = builderList.detailsKeyColumn %}
{% set detailsUrlParameter = builderList.detailsUrlParameter %}


{% for record in records %}

{% spaceless %}

    <div class=\"col-6 col-md-4 col-lg-4 col-xl-3 mb-3\" >
        <div class=\"col-12 pb-3 bg_t dark-link-1 item h-100\" > 
                
            {% if detailsPage %}
                <a href=\"{{ detailsPage|page({ (detailsUrlParameter): attribute(record, detailsKeyColumn) }) }}\">
            {% endif %}
            <div>
                <img src=\"{{record.photo|media}}\" alt=\"Изображение товара\" class=\"w-100\">  
            </div>
            <div class=\"pt-3 dname\">
                <h3> {{record.name}} </h3> 
            </div>       
            <hr>
            <div>
                <p class=\"text-left\"> {{html_limit(record.description, 40)|raw}}..<br></p> 
            </div>
            <div class=\"p-0 m-0 text-left\">
                {% set nal=0 %}
                {%for s in record.availabilities %} 
                    {% if s.quantity>0 %}
                        {% set nal=nal+1 %}
                    {% else %}
                
                    {% endif %}
                {% endfor %}
                    {% if nal>0 %} 
                    <p>  {% if record.discount > 0 %}<s> {{ record.price }} ₽ </s> <br>
                    {{  record.price*(1-(record.discount/100)) }} ₽ {% elseif record.discount == 0 %} {{ record.price }} ₽{% endif %} 
                    </p> 
                    {% else %}
                    <p class=\"danger-text\">
                        Нет в наличии
                    </p>
                    {% endif %}

            </div>
            {% if detailsPage %}
        </a>
            {% endif %}
        </div>
    </div> 


{% endspaceless %}
{% else %}
<h4 class=\"p-3\"> {{ noRecordsMessage }} </h4>
{% endfor %}
<div class=\"col-12 col-sm-12 col-md-12 col-lg-12 py-3\"> 
    {% if records.lastPage > 1 %}
        <ul class=\"pagination dark-link justify-content-center\">
            {% if records.currentPage > 1 %}
                <li>
                    <a class=\"page-link\" href=\"{{ this.page.baseFileName|page({ (pageParam): (records.currentPage-1) }) }}{{paginats}}\">
                            &#60
                    </a>
                </li>
            {% endif %}
            {% for page in 1..records.lastPage %}
                <li class=\" {{ records.currentPage == page ? 'active' : null }}\">
                    <a class=\"page-link {{ records.currentPage == page ? 'active bg_d' : null }}\" href=\"{{ this.page.baseFileName|page({ (pageParam): page }) }}{{paginats}}\"><lable class=\"{{ records.currentPage == page ? 'light-color' : null }}\">{{ page }}</lable></a>
                </li>
            {% endfor %}
            {% if records.lastPage > records.currentPage %}
                <li><a class=\"page-link\" href=\"{{ this.page.baseFileName|page({ (pageParam): (records.currentPage+1) }) }}{{paginats}}\">&#62</a></li>
            {% endif %}
        </ul>
    {% endif %}
</div>", "C:\\wamp64\\www\\project/themes/mine/partials/products_menu.htm", "");
    }
}
