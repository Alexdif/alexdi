<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* C:\wamp64\www\project/themes/mine/partials/order_form.htm */
class __TwigTemplate_e753a11921d010f61ea3f773ff833d10de61cd5de71908c01c142e865fdf8d74 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<div class=\"col-12 col-md-6 col-lg-7\">
    <h4 class=\"text-center pb-3\">  Личные данные  </h4>
    <form class=\"pb-3\" method=\"POST\" id=\"my_form1\">
        <div class=\"lch-container row\">
            <div class=\"form-group col-12 col-md-6\">
                <label>Имя</label>
                <input name=\"name\" type=\"text\" class=\"form-control\" id=\"accountName\" value=\"";
        // line 7
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "name", [], "any", false, false, false, 7), "html", null, true);
        echo "\" required>
            </div>
        
            <div class=\"form-group col-12 col-md-6\">
                <label>Фамилия</label>
                <input name=\"surname\" type=\"text\" class=\"form-control\" id=\"accountSurname\" value=\"";
        // line 12
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "surname", [], "any", false, false, false, 12), "html", null, true);
        echo "\" required>
            </div>
        
            <div class=\"form-group col-12\">
                <label\">Email</label>
                <input name=\"email\" type=\"text\" class=\"form-control\" id=\"accountEmail\" value=\"";
        // line 17
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "email", [], "any", false, false, false, 17), "html", null, true);
        echo "\" required>
            </div>

            <div class=\"form-group col-12 col-md-6\">
                    <label\">Телефон</label>
                    <input name=\"telephone\" type=\"text\" class=\"form-control\" id=\"accountTelephone\" value=\"";
        // line 22
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "telephone", [], "any", false, false, false, 22), "html", null, true);
        echo "\" required>
            </div>

        </div>
        

        <div id=\"accordion\" >
            <div class=\"lch-container row\">
                <div class=\"pb-3 col-12\" id=\"headingOne\">
                    <h5 class=\"mb-0\">
                        <input type=\"radio\" checked=\"checked\" name=\"delivery\" id=\"delivery1\" class=\"btn btn-dark\" data-toggle=\"collapse\"
                        data-target=\"#collapseOne\" aria-expanded=\"true\" aria-controls=\"collapseOne\" value=\"Доставка курьером\">
                        Доставка курьером
                        </input>
                    </h5>
                </div>

                <div id=\"collapseOne\" class=\"collapse show col-12\" aria-labelledby=\"headingOne\" data-parent=\"#accordion\">
                        <div class=\"row\">
                                <div class=\"form-group col-12\">
                                    <label>Aдресс: </label>
                                    <textarea name=\"address\" type=\"text\" class=\"form-control\" id=\"accountAddress\"> ";
        // line 43
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "address", [], "any", false, false, false, 43), "html", null, true);
        echo " </textarea>
                                </div>
                            
                                <div class=\"form-group col-12 col-md-6\">
                                    <label>Индекс: </label>
                                    <input name=\"index\" type=\"text\" class=\"form-control\" id=\"accountIndex\" value=\"";
        // line 48
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "index", [], "any", false, false, false, 48), "html", null, true);
        echo "\">
                                </div>
                                <div class=\"text-center text-md-right col-12 col-md-6\">
                                        <br>
                                        <button class=\"btn btn-dark center mt-md-2 mb-3 mb-md-0\" type=\"submit\" name=\"send\"> Оформить заказ </button>
                                </div>
                        </div>
                </div>
                
                
                <div class=\"text-left pb-3 col-12\" id=\"headingTwo\">
                    <h5 class=\"mb-0\">
                    <input type=\"radio\" name=\"delivery\" id=\"delivery2\" class=\"btn btn-link collapsed\" data-toggle=\"collapse\"
                    data-target=\"#collapseTwo\" aria-expanded=\"false\" aria-controls=\"collapseTwo\" value=\"Самовывоз\">
                            Самовывоз
                        </input>
                    </h5>
                </div>
                <div id=\"collapseTwo\" class=\"collapse text-center text-md-left col-12\" aria-labelledby=\"headingTwo\" data-parent=\"#accordion\">
                        <div class=\"row\">
                                <div class=\"form-group col-12 col-lg-8\">
                                    <label> Пункт выдачи: </label> <br>
                                    <select class=\"btn btn-light\" name=\"point\" style=\"width: 300px;\">
                                        <option disabled class=\"btn-dark\"> Пункты выдачи г.Москва: </option>
                                        <option value=\"Москва, Чонгарский бульвар, 4 корп. 2, 6 \"> Чонгарский бульвар, 4 корп. 2, 6 </option>
                                        <option selected value=\"Москва, ул. Шухова, 17 корп. 2\"> ул. Шухова, 17 корп. 2 </option>
                                        <option value=\"Москва, Озерная, 44 стр.2 \"> Озерная, 44 стр. 2 </option>
                                        <option value=\"Москва, ул. Вешняковская, 12Г\"> ул. Вешняковская, 12Г  </option>
                                        <option disabled class=\"btn-dark\"> Пункты выдачи г.Коломна: </option>
                                        <option value=\"Коломна, ул..Бульвар 800-летия Коломны, 14, 8\"> ул. Бульвар 800-летия Коломны, 14, 8  </option>
                                        <option value=\"Коломна, ул. Октябрьской Революции, 330\"> ул. Октябрьской Революции, 330  </option>
                                        <option value=\"Коломна, ул. Зайцева, 38, 19\"> ул. Зайцева, 38, 19 </option>
                                    </select>
                                </div>
                                <div class=\"text-center text-lg-right col-12 col-lg-4\">
                                        <br>
                                        <button class=\"btn btn-dark center mt-lg-2 mb-3 mb-lg-0\" type=\"submit\" name=\"send1\"> Оформить заказ </button>
                                </div>
                        </div>
                </div>
                </div>
            </div>
    </form>
</div>

<script>
var \$myForm = \$(\"#my_form1\");
    \$myForm.submit(function(){
    \$myForm.submit(function(){
        return false;
    });
});
</script>";
    }

    public function getTemplateName()
    {
        return "C:\\wamp64\\www\\project/themes/mine/partials/order_form.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  99 => 48,  91 => 43,  67 => 22,  59 => 17,  51 => 12,  43 => 7,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"col-12 col-md-6 col-lg-7\">
    <h4 class=\"text-center pb-3\">  Личные данные  </h4>
    <form class=\"pb-3\" method=\"POST\" id=\"my_form1\">
        <div class=\"lch-container row\">
            <div class=\"form-group col-12 col-md-6\">
                <label>Имя</label>
                <input name=\"name\" type=\"text\" class=\"form-control\" id=\"accountName\" value=\"{{ user.name }}\" required>
            </div>
        
            <div class=\"form-group col-12 col-md-6\">
                <label>Фамилия</label>
                <input name=\"surname\" type=\"text\" class=\"form-control\" id=\"accountSurname\" value=\"{{ user.surname }}\" required>
            </div>
        
            <div class=\"form-group col-12\">
                <label\">Email</label>
                <input name=\"email\" type=\"text\" class=\"form-control\" id=\"accountEmail\" value=\"{{ user.email }}\" required>
            </div>

            <div class=\"form-group col-12 col-md-6\">
                    <label\">Телефон</label>
                    <input name=\"telephone\" type=\"text\" class=\"form-control\" id=\"accountTelephone\" value=\"{{ user.telephone }}\" required>
            </div>

        </div>
        

        <div id=\"accordion\" >
            <div class=\"lch-container row\">
                <div class=\"pb-3 col-12\" id=\"headingOne\">
                    <h5 class=\"mb-0\">
                        <input type=\"radio\" checked=\"checked\" name=\"delivery\" id=\"delivery1\" class=\"btn btn-dark\" data-toggle=\"collapse\"
                        data-target=\"#collapseOne\" aria-expanded=\"true\" aria-controls=\"collapseOne\" value=\"Доставка курьером\">
                        Доставка курьером
                        </input>
                    </h5>
                </div>

                <div id=\"collapseOne\" class=\"collapse show col-12\" aria-labelledby=\"headingOne\" data-parent=\"#accordion\">
                        <div class=\"row\">
                                <div class=\"form-group col-12\">
                                    <label>Aдресс: </label>
                                    <textarea name=\"address\" type=\"text\" class=\"form-control\" id=\"accountAddress\"> {{ user.address }} </textarea>
                                </div>
                            
                                <div class=\"form-group col-12 col-md-6\">
                                    <label>Индекс: </label>
                                    <input name=\"index\" type=\"text\" class=\"form-control\" id=\"accountIndex\" value=\"{{ user.index }}\">
                                </div>
                                <div class=\"text-center text-md-right col-12 col-md-6\">
                                        <br>
                                        <button class=\"btn btn-dark center mt-md-2 mb-3 mb-md-0\" type=\"submit\" name=\"send\"> Оформить заказ </button>
                                </div>
                        </div>
                </div>
                
                
                <div class=\"text-left pb-3 col-12\" id=\"headingTwo\">
                    <h5 class=\"mb-0\">
                    <input type=\"radio\" name=\"delivery\" id=\"delivery2\" class=\"btn btn-link collapsed\" data-toggle=\"collapse\"
                    data-target=\"#collapseTwo\" aria-expanded=\"false\" aria-controls=\"collapseTwo\" value=\"Самовывоз\">
                            Самовывоз
                        </input>
                    </h5>
                </div>
                <div id=\"collapseTwo\" class=\"collapse text-center text-md-left col-12\" aria-labelledby=\"headingTwo\" data-parent=\"#accordion\">
                        <div class=\"row\">
                                <div class=\"form-group col-12 col-lg-8\">
                                    <label> Пункт выдачи: </label> <br>
                                    <select class=\"btn btn-light\" name=\"point\" style=\"width: 300px;\">
                                        <option disabled class=\"btn-dark\"> Пункты выдачи г.Москва: </option>
                                        <option value=\"Москва, Чонгарский бульвар, 4 корп. 2, 6 \"> Чонгарский бульвар, 4 корп. 2, 6 </option>
                                        <option selected value=\"Москва, ул. Шухова, 17 корп. 2\"> ул. Шухова, 17 корп. 2 </option>
                                        <option value=\"Москва, Озерная, 44 стр.2 \"> Озерная, 44 стр. 2 </option>
                                        <option value=\"Москва, ул. Вешняковская, 12Г\"> ул. Вешняковская, 12Г  </option>
                                        <option disabled class=\"btn-dark\"> Пункты выдачи г.Коломна: </option>
                                        <option value=\"Коломна, ул..Бульвар 800-летия Коломны, 14, 8\"> ул. Бульвар 800-летия Коломны, 14, 8  </option>
                                        <option value=\"Коломна, ул. Октябрьской Революции, 330\"> ул. Октябрьской Революции, 330  </option>
                                        <option value=\"Коломна, ул. Зайцева, 38, 19\"> ул. Зайцева, 38, 19 </option>
                                    </select>
                                </div>
                                <div class=\"text-center text-lg-right col-12 col-lg-4\">
                                        <br>
                                        <button class=\"btn btn-dark center mt-lg-2 mb-3 mb-lg-0\" type=\"submit\" name=\"send1\"> Оформить заказ </button>
                                </div>
                        </div>
                </div>
                </div>
            </div>
    </form>
</div>

<script>
var \$myForm = \$(\"#my_form1\");
    \$myForm.submit(function(){
    \$myForm.submit(function(){
        return false;
    });
});
</script>", "C:\\wamp64\\www\\project/themes/mine/partials/order_form.htm", "");
    }
}
