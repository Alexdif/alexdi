<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* C:\wamp64\www\project/themes/mine/partials/header_bottom.htm */
class __TwigTemplate_98bee806664f10eb59be05623d3f169dddab60c5c4cb57674a9687a6667a12bc extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<nav class=\"navbar navbar-expand-md navbar-default bg-dark navbar-dark sticky-top\">
        <a class=\"navbar-brand\" href=\"/project/\"><img src=\"";
        // line 2
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/zhestko_logo.png");
        echo "\" alt=\"logo\"></a>
        <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\"
        data-target=\"#navbarSupportedContent\" aria-controls=
        \"navbarSupportedContent\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
          <span class=\"navbar-toggler-icon\"></span>
        </button>

          <div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">
            <ul class=\"navbar-nav mr-auto text-center\">
            
              <li class=\"nav-item dropdown\">
              <!-- <div class=\"btn-group\">  --> 
                <form method=\"GET\" action=\"/project/goods/man/?\">
                  <input type=\"checkbox\" name=\"gender[]\" value=\"мужской\" class=\"unvisible\" checked=\"checked\"
                  ";
        // line 16
        if (twig_in_filter("мужской", (($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 = ($context["input"] ?? null)) && is_array($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4) || $__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 instanceof ArrayAccess ? ($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4["gender"] ?? null) : null))) {
            echo "checked=\"on\"";
        }
        echo ">
                  <input type=\"checkbox\" name=\"gender[]\" value=\"унисекс\" class=\"unvisible\" checked=\"checked\"
                  ";
        // line 18
        if (twig_in_filter("унисекс", (($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 = ($context["input"] ?? null)) && is_array($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144) || $__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 instanceof ArrayAccess ? ($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144["gender"] ?? null) : null))) {
            echo "checked=\"on\"";
        }
        echo "> 
                  <button type=\"submit\" class=\"btn text-light\">Мужчины</button>
                  </form>
              </li>
              <li class=\"nav-item dropdown\">
              <!-- <div class=\"btn-group\"> -->   
                  <form action=\"/project/goods/woman/?gender=женский \">
                    <input type=\"checkbox\" name=\"gender[]\" value=\"женский\" class=\"unvisible\" checked=\"checked\"
                  ";
        // line 26
        if (twig_in_filter("женский", (($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b = ($context["input"] ?? null)) && is_array($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b) || $__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b instanceof ArrayAccess ? ($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b["gender"] ?? null) : null))) {
            echo "checked=\"on\"";
        }
        echo ">
                  <input type=\"checkbox\" name=\"gender[]\" value=\"унисекс\" class=\"unvisible\" checked=\"checked\"
                  ";
        // line 28
        if (twig_in_filter("унисекс", (($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 = ($context["input"] ?? null)) && is_array($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002) || $__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 instanceof ArrayAccess ? ($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002["gender"] ?? null) : null))) {
            echo "checked=\"on\"";
        }
        echo ">
                      <button type=\"submit\" class=\"btn text-light\">Женщины</button>
                      </form>
            </ul>
            
          <!--  <form class=\"form-inline my-2 my-md-0\">
              <input type=\"text\" class=\"form-control mr-sm-2\" placeholder=\"Поиск\" aria-label=\"Search\">
              <button class=\"btn btn-oytline-success my-2 my-sm-0 btn-light\"> Поиск </button>
            </form> -->
          </div>
    </nav>";
    }

    public function getTemplateName()
    {
        return "C:\\wamp64\\www\\project/themes/mine/partials/header_bottom.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  82 => 28,  75 => 26,  62 => 18,  55 => 16,  38 => 2,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<nav class=\"navbar navbar-expand-md navbar-default bg-dark navbar-dark sticky-top\">
        <a class=\"navbar-brand\" href=\"/project/\"><img src=\"{{'assets/images/zhestko_logo.png'|theme}}\" alt=\"logo\"></a>
        <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\"
        data-target=\"#navbarSupportedContent\" aria-controls=
        \"navbarSupportedContent\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
          <span class=\"navbar-toggler-icon\"></span>
        </button>

          <div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">
            <ul class=\"navbar-nav mr-auto text-center\">
            
              <li class=\"nav-item dropdown\">
              <!-- <div class=\"btn-group\">  --> 
                <form method=\"GET\" action=\"/project/goods/man/?\">
                  <input type=\"checkbox\" name=\"gender[]\" value=\"мужской\" class=\"unvisible\" checked=\"checked\"
                  {%if 'мужской' in input['gender'] %}checked=\"on\"{%endif%}>
                  <input type=\"checkbox\" name=\"gender[]\" value=\"унисекс\" class=\"unvisible\" checked=\"checked\"
                  {%if 'унисекс' in input['gender'] %}checked=\"on\"{%endif%}> 
                  <button type=\"submit\" class=\"btn text-light\">Мужчины</button>
                  </form>
              </li>
              <li class=\"nav-item dropdown\">
              <!-- <div class=\"btn-group\"> -->   
                  <form action=\"/project/goods/woman/?gender=женский \">
                    <input type=\"checkbox\" name=\"gender[]\" value=\"женский\" class=\"unvisible\" checked=\"checked\"
                  {%if 'женский' in input['gender'] %}checked=\"on\"{%endif%}>
                  <input type=\"checkbox\" name=\"gender[]\" value=\"унисекс\" class=\"unvisible\" checked=\"checked\"
                  {%if 'унисекс' in input['gender'] %}checked=\"on\"{%endif%}>
                      <button type=\"submit\" class=\"btn text-light\">Женщины</button>
                      </form>
            </ul>
            
          <!--  <form class=\"form-inline my-2 my-md-0\">
              <input type=\"text\" class=\"form-control mr-sm-2\" placeholder=\"Поиск\" aria-label=\"Search\">
              <button class=\"btn btn-oytline-success my-2 my-sm-0 btn-light\"> Поиск </button>
            </form> -->
          </div>
    </nav>", "C:\\wamp64\\www\\project/themes/mine/partials/header_bottom.htm", "");
    }
}
