<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* C:\wamp64\www\project/themes/mine/partials/header_top.htm */
class __TwigTemplate_47ad8422f8206e339a56623a3005cfb861dab571dabe91cae9f8428294c1ad8a extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<div  class=\"d-inline-flex w-100 string-top bg_d\">
  <ul class=\" list-inline light-link ml-auto m-0 pr-3\">
    <li class=\"list-inline-item light-link\"> 
      <a class=\"light-link\" href=\"/project/about_us\">О нас </a> 
    </li>
    <li class=\"list-inline-item\"> 
      <a class=\"light-link\" href=\"/project/feedback\">Обратная связь </a>
    </li>
    <li class=\"list-inline-item align-middle\"> 
      <a href=\"/project/account\"><img src=\"";
        // line 10
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/u11.png");
        echo "\"  alt=\"Профиль\" class=\"d-block img-fluid \" style=\"height: 25px; width: 25px;\"></a>
    </li>
    <li class=\"list-inline-item align-middle\"> 
      <a href=\"/project/basket/1\"><img src=\"";
        // line 13
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/u12.png");
        echo "\"  alt=\"Корзина\" class=\"d-block img-fluid \" style=\"height: 35px; width: 35px;\"></a>
    </li>
  </ul>
</div>";
    }

    public function getTemplateName()
    {
        return "C:\\wamp64\\www\\project/themes/mine/partials/header_top.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  52 => 13,  46 => 10,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div  class=\"d-inline-flex w-100 string-top bg_d\">
  <ul class=\" list-inline light-link ml-auto m-0 pr-3\">
    <li class=\"list-inline-item light-link\"> 
      <a class=\"light-link\" href=\"/project/about_us\">О нас </a> 
    </li>
    <li class=\"list-inline-item\"> 
      <a class=\"light-link\" href=\"/project/feedback\">Обратная связь </a>
    </li>
    <li class=\"list-inline-item align-middle\"> 
      <a href=\"/project/account\"><img src=\"{{ 'assets/images/u11.png'| theme }}\"  alt=\"Профиль\" class=\"d-block img-fluid \" style=\"height: 25px; width: 25px;\"></a>
    </li>
    <li class=\"list-inline-item align-middle\"> 
      <a href=\"/project/basket/1\"><img src=\"{{ 'assets/images/u12.png'| theme }}\"  alt=\"Корзина\" class=\"d-block img-fluid \" style=\"height: 35px; width: 35px;\"></a>
    </li>
  </ul>
</div>", "C:\\wamp64\\www\\project/themes/mine/partials/header_top.htm", "");
    }
}
