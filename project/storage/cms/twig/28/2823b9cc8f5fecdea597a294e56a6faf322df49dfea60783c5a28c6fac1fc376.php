<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* C:\wamp64\www\project/themes/mine/partials/checkout.htm */
class __TwigTemplate_7795dbfa0c448cc036166bc40ce919dee0d76f5071e30e3f71d6f15ae16a1051 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        $context["records"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "records", [], "any", false, false, false, 1);
        // line 2
        $context["displayColumn"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "displayColumn", [], "any", false, false, false, 2);
        // line 3
        $context["noRecordsMessage"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "noRecordsMessage", [], "any", false, false, false, 3);
        // line 4
        $context["detailsPage"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "detailsPage", [], "any", false, false, false, 4);
        // line 5
        $context["detailsKeyColumn"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "detailsKeyColumn", [], "any", false, false, false, 5);
        // line 6
        $context["detailsUrlParameter"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "detailsUrlParameter", [], "any", false, false, false, 6);
        // line 7
        echo "<div class=\"container p-3 pl-4\">
    <h1> Оформление заказа </h1>
</div>
<div class=\"bg_t\">
    <div class=\"container p-3\">
        <div class=\"row\">
                ";
        // line 13
        $context["full_price"] = 0;
        // line 14
        echo "                ";
        $context["full_quantity"] = 0;
        // line 15
        echo "                ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["records"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["record"]) {
            // line 16
            echo "                    ";
            $context["full_price"] = (($context["full_price"] ?? null) + twig_get_attribute($this->env, $this->source, $context["record"], "price", [], "any", false, false, false, 16));
            // line 17
            echo "                    ";
            $context["full_quantity"] = (($context["full_quantity"] ?? null) + 1);
            // line 18
            echo "                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['record'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 19
        echo "                ";
        if ((($context["full_price"] ?? null) > 0)) {
            // line 20
            echo "            <div class=\"col-6 col-sm-8 col-md-9 col-lg-10\">
                    <label>  Стоимость:<b> ";
            // line 21
            echo twig_escape_filter($this->env, ($context["full_price"] ?? null), "html", null, true);
            echo "₽</b></label> <br>  
                    <label>  Количество: <b> ";
            // line 22
            echo twig_escape_filter($this->env, ($context["full_quantity"] ?? null), "html", null, true);
            echo " шт.</b></label>
            </div>
            ";
        } else {
            // line 25
            echo "            <h4 class=\"p-3\"> Как вы здесь оказались? o:</h4> 
            ";
        }
        // line 27
        echo "            <div class=\"col-6 col-sm-4 col-md-3 col-lg-2 my-auto p-0 text-center\">
                ";
        // line 28
        if ((($context["full_price"] ?? null) > 0)) {
            echo " 
                <form action=\"/project/basket/1\"> 
                    <button class=\"btn btn-oytline-success my-2 my-sm-0 btn-dark\"> Корзина </button>
                </form>
                ";
        } else {
            // line 32
            echo " 
                ";
        }
        // line 33
        echo "                                           
            </div> 
        </div>
    </div>
</div>
";
        // line 38
        if ((($context["full_price"] ?? null) > 0)) {
            echo " 
<div class=\"mx-auto container w-100 m-0\">
    <main class=\"col-12  p-0 pt-3\">
        
        <div class=\"row col-12 w-100 m-0 p-0\">
            


            ";
            // line 46
            $context['__cms_partial_params'] = [];
            echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("order_form"            , $context['__cms_partial_params']            , true            );
            unset($context['__cms_partial_params']);
            // line 47
            echo "
            <div class=\"col-12 col-md-6 col-lg-5\">
                <h4 class=\"text-center pb-3\"> Список продуктов </h4>
                    ";
            // line 50
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["records"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["record"]) {
                // line 51
                echo "                    ";
                ob_start(function () { return ''; });
                echo "          
                        <div class=\"col-12 mb-3 \">
                            <div class=\"col-12 pb-3 bg_t dark-link-1 item h-100\" style=\"border-radius: 15px 15px 15px 15px !important;\"> 
                                <div class=\"row \">
                                    <div class=\"col-6 col-sm-4 pt-3\">
                                        ";
                // line 56
                if (($context["detailsPage"] ?? null)) {
                    // line 57
                    echo "                                            <a href=\"";
                    echo $this->extensions['Cms\Twig\Extension']->pageFilter(($context["detailsPage"] ?? null), [($context["detailsUrlParameter"] ?? null) => twig_get_attribute($this->env, $this->source, $context["record"], ($context["detailsKeyColumn"] ?? null), [], "any", false, false, false, 57)]);
                    echo "\">
                                        ";
                }
                // line 59
                echo "                                        <img src=\"";
                echo $this->extensions['System\Twig\Extension']->mediaFilter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["record"], "product", [], "any", false, false, false, 59), "photo", [], "any", false, false, false, 59));
                echo "\" alt=\"Изображение товара\" class=\"w-100\"> 
                                        ";
                // line 60
                if (($context["detailsPage"] ?? null)) {
                    // line 61
                    echo "                                            </a>
                                        ";
                }
                // line 63
                echo "                                    </div>
                                    
                                    <div class=\"col-6 col-sm-8 my-auto\">
                                            
                                        <div class=\"text-left text-md-center row \">
                                                
                                            <div class=\"col-12 my-auto p-0 pt-3 pt-md-0\">
                                                ";
                // line 70
                if (($context["detailsPage"] ?? null)) {
                    // line 71
                    echo "                                                    <a href=\"";
                    echo $this->extensions['Cms\Twig\Extension']->pageFilter(($context["detailsPage"] ?? null), [($context["detailsUrlParameter"] ?? null) => twig_get_attribute($this->env, $this->source, $context["record"], ($context["detailsKeyColumn"] ?? null), [], "any", false, false, false, 71)]);
                    echo "\">
                                                ";
                }
                // line 73
                echo "                                                <h3> ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["record"], "product", [], "any", false, false, false, 73), "name", [], "any", false, false, false, 73), "html", null, true);
                echo " </h3> 
                                                ";
                // line 74
                if (($context["detailsPage"] ?? null)) {
                    // line 75
                    echo "                                                    </a>
                                                ";
                }
                // line 77
                echo "                                            </div>   
                                            ";
                // line 78
                if (($context["detailsPage"] ?? null)) {
                    // line 79
                    echo "                                                    <a href=\"";
                    echo $this->extensions['Cms\Twig\Extension']->pageFilter(($context["detailsPage"] ?? null), [($context["detailsUrlParameter"] ?? null) => twig_get_attribute($this->env, $this->source, $context["record"], ($context["detailsKeyColumn"] ?? null), [], "any", false, false, false, 79)]);
                    echo "\">
                                                ";
                }
                // line 80
                echo "   
                                            <div class=\"col-12 p-0 border-right\">
                                                <div class=\"text-left row p-0 \">
                                                    <div class=\"col-12 my-auto\">
                                                        <label class=\"m-0\"> Цвет: ";
                // line 84
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["record"], "product", [], "any", false, false, false, 84), "color", [], "any", false, false, false, 84), "html", null, true);
                echo " </label> 
                                                    </div>
                                                    <div class=\"col-12 my-auto\">
                                                        <label class=\"m-0\">Размер: ";
                // line 87
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["record"], "size", [], "any", false, false, false, 87), "html", null, true);
                echo " </label> 
                                                    </div>
                                                    <div class=\"col-12 my-auto=\">
                                                        <label class=\"m-0\"> <b>Цена: ";
                // line 90
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["record"], "price", [], "any", false, false, false, 90), "html", null, true);
                echo "</b> </label> 
                                                    </div>
                                                </div>
                                                ";
                // line 93
                if (($context["detailsPage"] ?? null)) {
                    // line 94
                    echo "                                                    </a>
                                                ";
                }
                // line 96
                echo "                                            </div>
    
                                            
                                                <!-- <form method=\"POST\">
                                                <input type=\"hidden\" name=\"size\" value=\"";
                // line 100
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["record"], "size", [], "any", false, false, false, 100), "html", null, true);
                echo "\" /> 
                                                <input type=\"hidden\" name=\"delete_product_id\" value=\"";
                // line 101
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["record"], "id", [], "any", false, false, false, 101), "html", null, true);
                echo "\" /> 
                                                <input type=\"hidden\" name=\"product_id\" value=\"";
                // line 102
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["record"], "products", [], "any", false, false, false, 102), "id", [], "any", false, false, false, 102), "html", null, true);
                echo "\" /> 
                                            </form>                                         -->

                                        </div>
                                    
                                    </div> 
                                </div>
                                
                                
                            </div>
                        </div> 
                    ";
                echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
                // line 114
                echo "    
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['record'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 116
            echo "            </div>
        </div>
    </main>
";
        }
    }

    public function getTemplateName()
    {
        return "C:\\wamp64\\www\\project/themes/mine/partials/checkout.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  276 => 116,  269 => 114,  254 => 102,  250 => 101,  246 => 100,  240 => 96,  236 => 94,  234 => 93,  228 => 90,  222 => 87,  216 => 84,  210 => 80,  204 => 79,  202 => 78,  199 => 77,  195 => 75,  193 => 74,  188 => 73,  182 => 71,  180 => 70,  171 => 63,  167 => 61,  165 => 60,  160 => 59,  154 => 57,  152 => 56,  143 => 51,  139 => 50,  134 => 47,  130 => 46,  119 => 38,  112 => 33,  108 => 32,  100 => 28,  97 => 27,  93 => 25,  87 => 22,  83 => 21,  80 => 20,  77 => 19,  71 => 18,  68 => 17,  65 => 16,  60 => 15,  57 => 14,  55 => 13,  47 => 7,  45 => 6,  43 => 5,  41 => 4,  39 => 3,  37 => 2,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% set records = builderList.records %}
{% set displayColumn = builderList.displayColumn %}
{% set noRecordsMessage = builderList.noRecordsMessage %}
{% set detailsPage = builderList.detailsPage %}
{% set detailsKeyColumn = builderList.detailsKeyColumn %}
{% set detailsUrlParameter = builderList.detailsUrlParameter %}
<div class=\"container p-3 pl-4\">
    <h1> Оформление заказа </h1>
</div>
<div class=\"bg_t\">
    <div class=\"container p-3\">
        <div class=\"row\">
                {% set full_price = 0 %}
                {% set full_quantity = 0 %}
                {% for record in records %}
                    {%set full_price=full_price+record.price %}
                    {%set full_quantity=full_quantity+1 %}
                {%endfor%}
                {% if full_price > 0 %}
            <div class=\"col-6 col-sm-8 col-md-9 col-lg-10\">
                    <label>  Стоимость:<b> {{full_price}}₽</b></label> <br>  
                    <label>  Количество: <b> {{full_quantity}} шт.</b></label>
            </div>
            {% else %}
            <h4 class=\"p-3\"> Как вы здесь оказались? o:</h4> 
            {% endif %}
            <div class=\"col-6 col-sm-4 col-md-3 col-lg-2 my-auto p-0 text-center\">
                {% if full_price > 0 %} 
                <form action=\"/project/basket/1\"> 
                    <button class=\"btn btn-oytline-success my-2 my-sm-0 btn-dark\"> Корзина </button>
                </form>
                {% else %} 
                {% endif %}                                           
            </div> 
        </div>
    </div>
</div>
{% if full_price > 0 %} 
<div class=\"mx-auto container w-100 m-0\">
    <main class=\"col-12  p-0 pt-3\">
        
        <div class=\"row col-12 w-100 m-0 p-0\">
            


            {% partial \"order_form\" %}

            <div class=\"col-12 col-md-6 col-lg-5\">
                <h4 class=\"text-center pb-3\"> Список продуктов </h4>
                    {% for record in records %}
                    {% spaceless %}          
                        <div class=\"col-12 mb-3 \">
                            <div class=\"col-12 pb-3 bg_t dark-link-1 item h-100\" style=\"border-radius: 15px 15px 15px 15px !important;\"> 
                                <div class=\"row \">
                                    <div class=\"col-6 col-sm-4 pt-3\">
                                        {% if detailsPage %}
                                            <a href=\"{{ detailsPage|page({ (detailsUrlParameter): attribute(record, detailsKeyColumn) }) }}\">
                                        {% endif %}
                                        <img src=\"{{record.product.photo|media}}\" alt=\"Изображение товара\" class=\"w-100\"> 
                                        {% if detailsPage %}
                                            </a>
                                        {% endif %}
                                    </div>
                                    
                                    <div class=\"col-6 col-sm-8 my-auto\">
                                            
                                        <div class=\"text-left text-md-center row \">
                                                
                                            <div class=\"col-12 my-auto p-0 pt-3 pt-md-0\">
                                                {% if detailsPage %}
                                                    <a href=\"{{ detailsPage|page({ (detailsUrlParameter): attribute(record, detailsKeyColumn) }) }}\">
                                                {% endif %}
                                                <h3> {{record.product.name}} </h3> 
                                                {% if detailsPage %}
                                                    </a>
                                                {% endif %}
                                            </div>   
                                            {% if detailsPage %}
                                                    <a href=\"{{ detailsPage|page({ (detailsUrlParameter): attribute(record, detailsKeyColumn) }) }}\">
                                                {% endif %}   
                                            <div class=\"col-12 p-0 border-right\">
                                                <div class=\"text-left row p-0 \">
                                                    <div class=\"col-12 my-auto\">
                                                        <label class=\"m-0\"> Цвет: {{record.product.color}} </label> 
                                                    </div>
                                                    <div class=\"col-12 my-auto\">
                                                        <label class=\"m-0\">Размер: {{  record.size }} </label> 
                                                    </div>
                                                    <div class=\"col-12 my-auto=\">
                                                        <label class=\"m-0\"> <b>Цена: {{ record.price }}</b> </label> 
                                                    </div>
                                                </div>
                                                {% if detailsPage %}
                                                    </a>
                                                {% endif %}
                                            </div>
    
                                            
                                                <!-- <form method=\"POST\">
                                                <input type=\"hidden\" name=\"size\" value=\"{{ record.size  }}\" /> 
                                                <input type=\"hidden\" name=\"delete_product_id\" value=\"{{ record.id }}\" /> 
                                                <input type=\"hidden\" name=\"product_id\" value=\"{{ record.products.id }}\" /> 
                                            </form>                                         -->

                                        </div>
                                    
                                    </div> 
                                </div>
                                
                                
                            </div>
                        </div> 
                    {% endspaceless %}
    
                {% endfor %}
            </div>
        </div>
    </main>
{% endif %}", "C:\\wamp64\\www\\project/themes/mine/partials/checkout.htm", "");
    }
}
