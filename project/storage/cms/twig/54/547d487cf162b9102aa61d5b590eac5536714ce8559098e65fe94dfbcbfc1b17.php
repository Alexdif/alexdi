<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* C:\wamp64\www\project/themes/mine/partials/left_menu.htm */
class __TwigTemplate_93278f87be392b4201d2039716a76e67df57d1b0a2a8cea85b5d09905ff91052 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        $context["records"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "records", [], "any", false, false, false, 1);
        // line 2
        $context["displayColumn"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "displayColumn", [], "any", false, false, false, 2);
        // line 3
        $context["noRecordsMessage"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "noRecordsMessage", [], "any", false, false, false, 3);
        // line 4
        $context["detailsPage"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "detailsPage", [], "any", false, false, false, 4);
        // line 5
        $context["detailsKeyColumn"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "detailsKeyColumn", [], "any", false, false, false, 5);
        // line 6
        $context["detailsUrlParameter"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "detailsUrlParameter", [], "any", false, false, false, 6);
        // line 7
        echo "
<nav class=\"navbar-light navbar-expand-md pl-3 \">
  <div class=\"row w-100 m-0\">
    <div class=\"col-9 p-0 \" >
      <a href=\"#\" class=\"logo navbar-brand\" style=\"padding-top:12px;\">
        Ассортимент
      </a>
    </div>
    <div class=\"col-3 \">
      <button class=\"navbar-toggler my-2\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarSupportedContent1\"
        aria-controls=\"navbarSupportedContent1\" id=\"navtitle\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
        <span class=\"navbar-toggler-icon\"> </span>
      </button>
    </div>  
  </div>

<div class=\"left-menu collapse show\" id=\"navbarSupportedContent1\">
<!-- ";
        // line 24
        if ((((twig_in_filter("size", twig_get_array_keys_filter(($context["input"] ?? null))) || twig_in_filter("type", twig_get_array_keys_filter(($context["input"] ?? null)))) || twig_in_filter("country", twig_get_array_keys_filter(($context["input"] ?? null)))) || twig_in_filter("color", twig_get_array_keys_filter(($context["input"] ?? null))))) {
            echo "class=\"left-menu collapse in show\" ";
        } else {
            echo " class=\"left-menu collapse\"";
        }
        echo ">  -->

";
        // line 26
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "param", [], "any", false, false, false, 26), "slug", [], "any", false, false, false, 26) == "man")) {
            // line 27
            echo "
    <form method=\"GET\" action=\"";
            // line 28
            echo url("goods/man/1?");
            echo "\">

                  <input type=\"checkbox\" name=\"gender[]\" value=\"мужской\" class=\"unvisible\" checked=\"checked\"
                  ";
            // line 31
            if (twig_in_filter("мужской", (($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 = ($context["input"] ?? null)) && is_array($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4) || $__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 instanceof ArrayAccess ? ($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4["gender"] ?? null) : null))) {
                echo "checked=\"on\"";
            }
            echo ">
                  <input type=\"checkbox\" name=\"gender[]\" value=\"унисекс\" class=\"unvisible\" checked=\"checked\"
                  ";
            // line 33
            if (twig_in_filter("унисекс", (($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 = ($context["input"] ?? null)) && is_array($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144) || $__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 instanceof ArrayAccess ? ($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144["gender"] ?? null) : null))) {
                echo "checked=\"on\"";
            }
            echo ">

    ";
        } elseif ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,         // line 35
($context["this"] ?? null), "param", [], "any", false, false, false, 35), "slug", [], "any", false, false, false, 35) == "woman")) {
            // line 36
            echo "
    <form method=\"GET\" action=\"";
            // line 37
            echo url("goods/woman/1?");
            echo "\">
                <input type=\"checkbox\" name=\"gender[]\" value=\"женский\" class=\"unvisible\" checked=\"checked\"
                ";
            // line 39
            if (twig_in_filter("женский", (($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b = ($context["input"] ?? null)) && is_array($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b) || $__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b instanceof ArrayAccess ? ($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b["gender"] ?? null) : null))) {
                echo "checked=\"on\"";
            }
            echo ">
                <input type=\"checkbox\" name=\"gender[]\" value=\"унисекс\" class=\"unvisible\" checked=\"checked\"
                ";
            // line 41
            if (twig_in_filter("унисекс", (($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 = ($context["input"] ?? null)) && is_array($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002) || $__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 instanceof ArrayAccess ? ($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002["gender"] ?? null) : null))) {
                echo "checked=\"on\"";
            }
            echo ">

    ";
        }
        // line 44
        echo "
      <ul class=\"p-0 dark-link\"> 
        <li> 
          <a href=\"#\" data-toggle=\"collapse\" data-target=\"#left-menu1\"><h6> Тип одежды </h6></a> 
          <div  ";
        // line 48
        if (twig_in_filter("type", twig_get_array_keys_filter(($context["input"] ?? null)))) {
            echo "class=\"collapse in show\" ";
        } else {
            echo " class=\"collapse\"";
        }
        echo "   id=\"left-menu1\"> 
            <ul class=\"ul-li p-0\"> 
              ";
        // line 50
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["t_type"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["t"]) {
            // line 51
            echo "              <li>
                <label>
                  <input type=\"checkbox\" name=\"type[]\" value=\"";
            // line 53
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["t"], "id", [], "any", false, false, false, 53), "html", null, true);
            echo "\"
                  ";
            // line 54
            if (twig_in_filter(twig_get_attribute($this->env, $this->source, $context["t"], "id", [], "any", false, false, false, 54), (($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4 = ($context["input"] ?? null)) && is_array($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4) || $__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4 instanceof ArrayAccess ? ($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4["type"] ?? null) : null))) {
                echo "checked=\"on\"";
            }
            echo ">
                  <span>";
            // line 55
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["t"], "type", [], "any", false, false, false, 55), "html", null, true);
            echo "</span>
                </label>
              </li>
              ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['t'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 59
        echo "            </ul>   
          </div>
        </li> 
        <li> 
          <a href=\"#\" data-toggle=\"collapse\" data-target=\"#left-menu7\"> <h6> Производитель </h6></a> 
          <div  ";
        // line 64
        if (twig_in_filter("manufacturer", twig_get_array_keys_filter(($context["input"] ?? null)))) {
            echo "class=\"collapse in show\"";
        } else {
            echo " class=\"collapse\" ";
        }
        echo "   id=\"left-menu7\"> 
            <ul class=\"ul-li p-0\"> 
              ";
        // line 66
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["m_manufacturer"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["m"]) {
            // line 67
            echo "              <li>
                <label>
                  <input type=\"checkbox\" name=\"manufacturer[]\" value=\"";
            // line 69
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["m"], "id", [], "any", false, false, false, 69), "html", null, true);
            echo "\"
                  ";
            // line 70
            if (twig_in_filter(twig_get_attribute($this->env, $this->source, $context["m"], "id", [], "any", false, false, false, 70), (($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666 = ($context["input"] ?? null)) && is_array($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666) || $__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666 instanceof ArrayAccess ? ($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666["manufacturer"] ?? null) : null))) {
                echo "checked=\"on\"";
            }
            echo ">
                  <span>";
            // line 71
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["m"], "manufacturer", [], "any", false, false, false, 71), "html", null, true);
            echo "</span>
                </label>
              </li>
              ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['m'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 75
        echo "            </ul>   
          </div>
        </li> 
        <li> 
          <a href=\"#\" data-toggle=\"collapse\" data-target=\"#left-menu2\"> <h6> Страна </h6></a> 
          <div  ";
        // line 80
        if (twig_in_filter("country", twig_get_array_keys_filter(($context["input"] ?? null)))) {
            echo "class=\"collapse in show\"";
        } else {
            echo " class=\"collapse\" ";
        }
        echo "   id=\"left-menu2\"> 
            <ul class=\"ul-li p-0\"> 
              ";
        // line 82
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["s_country"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["c"]) {
            // line 83
            echo "              <li>
                <label>
                  <input type=\"checkbox\" name=\"country[]\" value=\"";
            // line 85
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["c"], "id", [], "any", false, false, false, 85), "html", null, true);
            echo "\"
                  ";
            // line 86
            if (twig_in_filter(twig_get_attribute($this->env, $this->source, $context["c"], "id", [], "any", false, false, false, 86), (($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e = ($context["input"] ?? null)) && is_array($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e) || $__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e instanceof ArrayAccess ? ($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e["country"] ?? null) : null))) {
                echo "checked=\"on\"";
            }
            echo ">
                  <span>";
            // line 87
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["c"], "country", [], "any", false, false, false, 87), "html", null, true);
            echo "</span>
                </label>
              </li>
              ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['c'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 91
        echo "            </ul>   
          </div>
        </li> 
        <li> 
          <a href=\"#\" data-toggle=\"collapse\" data-target=\"#left-menu3\"> <h6> Цвет </h6></a> 
          <div  ";
        // line 96
        if (twig_in_filter("color", twig_get_array_keys_filter(($context["input"] ?? null)))) {
            echo "class=\"collapse in show\"";
        } else {
            echo " class=\"collapse\" ";
        }
        echo "   id=\"left-menu3\"> 
            <ul class=\"ul-li p-0\"> 
              ";
        // line 98
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["p_product"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["p"]) {
            // line 99
            echo "              <li>
                <label>
                  <input type=\"checkbox\" name=\"color[]\" value=\"";
            // line 101
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["p"], "color", [], "any", false, false, false, 101), "html", null, true);
            echo "\"
                  ";
            // line 102
            if (twig_in_filter(twig_get_attribute($this->env, $this->source, $context["p"], "color", [], "any", false, false, false, 102), (($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52 = ($context["input"] ?? null)) && is_array($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52) || $__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52 instanceof ArrayAccess ? ($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52["color"] ?? null) : null))) {
                echo "checked=\"on\"";
            }
            echo ">
                  <span>";
            // line 103
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["p"], "color", [], "any", false, false, false, 103), "html", null, true);
            echo "</span>
                </label>
              </li>
              ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['p'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 107
        echo "            </ul>   
          </div>                
        </li> 
        <li> 
          <a href=\"#\" data-toggle=\"collapse\" data-target=\"#left-menu4\"> <h6> Размер </h6></a> 
          <div  ";
        // line 112
        if (twig_in_filter("size", twig_get_array_keys_filter(($context["input"] ?? null)))) {
            echo "class=\"collapse in show\"";
        } else {
            echo " class=\"collapse\" ";
        }
        echo " id=\"left-menu4\"> 
            <ul class=\"ul-li p-0\">
              ";
        // line 114
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["a_availability"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["a"]) {
            // line 115
            echo "              <li>
                <label>
                  <input type=\"checkbox\" name=\"size[]\" value=\"";
            // line 117
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["a"], "size", [], "any", false, false, false, 117), "html", null, true);
            echo "\"
                  ";
            // line 118
            if (twig_in_filter(twig_get_attribute($this->env, $this->source, $context["a"], "size", [], "any", false, false, false, 118), (($__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136 = ($context["input"] ?? null)) && is_array($__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136) || $__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136 instanceof ArrayAccess ? ($__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136["size"] ?? null) : null))) {
                echo "checked=\"on\"";
            }
            echo ">
                  <span>";
            // line 119
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["a"], "size", [], "any", false, false, false, 119), "html", null, true);
            echo "</span>
                </label>
              </li>
              ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['a'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 123
        echo "            </ul>   
          </div>
        </li>
        <li> 
          <a href=\"#\" data-toggle=\"collapse\" data-target=\"#left-menu5\"> <h6> Скидки </h6></a> 
          <div  ";
        // line 128
        if (twig_in_filter("discount", twig_get_array_keys_filter(($context["input"] ?? null)))) {
            echo "class=\"collapse in show\"";
        } else {
            echo " class=\"collapse\" ";
        }
        echo " id=\"left-menu5\"> 
            <ul class=\"ul-li p-0\">
              <li>
                <label>
                  <input type=\"checkbox\" name=\"discount\" 
                  ";
        // line 133
        if (((($__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386 = ($context["input"] ?? null)) && is_array($__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386) || $__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386 instanceof ArrayAccess ? ($__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386["discount"] ?? null) : null) == "on")) {
            echo "checked";
        }
        echo ">
                  <span>Показать товары со скидкой</span>
                </label>
              </li>  
            </ul>   
          </div>
        </li>
        <li> 
            <a href=\"#\" data-toggle=\"collapse\" data-target=\"#left-menu6\"> <h6> Сортировка </h6></a> 
            <div class=\"collapse\" id=\"left-menu6\"> 
              <ul class=\"ul-li p-0\">
                <li>
                  <label class=\"type_checkbox\">
                    <input  type=\"radio\" name=\"price\" value= \"asc\" onMouseDown=\"this.isChecked=this.checked;\"
                    onClick=\"this.checked=!this.isChecked;\"
                    ";
        // line 148
        if (((($__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9 = ($context["input"] ?? null)) && is_array($__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9) || $__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9 instanceof ArrayAccess ? ($__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9["price"] ?? null) : null) == "asc")) {
            echo "checked=\"on\"";
        }
        echo ">
                    <span>Цена по возрастанию</span>
                  </label>
                </li>  
                <li>
                    <label>
                        <input  type=\"radio\" name=\"price\" value=\"desc\" onMouseDown=\"this.isChecked=this.checked;\"
                        onClick=\"this.checked=!this.isChecked;\"
                        ";
        // line 156
        if (((($__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae = ($context["input"] ?? null)) && is_array($__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae) || $__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae instanceof ArrayAccess ? ($__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae["price"] ?? null) : null) == "desc")) {
            echo "checked=\"on\"";
        }
        echo ">
                        <span>Цена по убыванию</span>
                    </label>
                </li>
                <li>
                  <label class=\"type_checkbox\">
                    <input  type=\"radio\" name=\"sort\" value= \"new\" onMouseDown=\"this.isChecked=this.checked;\"
                    onClick=\"this.checked=!this.isChecked;\"
                    ";
        // line 164
        if (((($__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f = ($context["input"] ?? null)) && is_array($__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f) || $__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f instanceof ArrayAccess ? ($__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f["sort"] ?? null) : null) == "new")) {
            echo "checked=\"on\"";
        }
        echo ">
                    <span>Сначала новые</span>
                  </label>
                </li>  
                <li>
                    <label>
                      <input  type=\"radio\" name=\"sort\" value=\"old\"  onMouseDown=\"this.isChecked=this.checked;\"
                      onClick=\"this.checked=!this.isChecked;\"
                      ";
        // line 172
        if (((($__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40 = ($context["input"] ?? null)) && is_array($__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40) || $__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40 instanceof ArrayAccess ? ($__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40["sort"] ?? null) : null) == "old")) {
            echo "checked=\"on\"";
        }
        echo ">
                      <span>Сначала старые</span>
                    </label>
                </li>
              </ul>   
            </div>
          </li>
        <button class=\"btn btn-dark mt-3\" type=\"submit\"> 
          Фильтровать
        </button> 
      </ul> 
    </form>
  </div>
</nav>

<script>
  function click()
  {
    this.checked = false;
  }
</script>";
    }

    public function getTemplateName()
    {
        return "C:\\wamp64\\www\\project/themes/mine/partials/left_menu.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  422 => 172,  409 => 164,  396 => 156,  383 => 148,  363 => 133,  351 => 128,  344 => 123,  334 => 119,  328 => 118,  324 => 117,  320 => 115,  316 => 114,  307 => 112,  300 => 107,  290 => 103,  284 => 102,  280 => 101,  276 => 99,  272 => 98,  263 => 96,  256 => 91,  246 => 87,  240 => 86,  236 => 85,  232 => 83,  228 => 82,  219 => 80,  212 => 75,  202 => 71,  196 => 70,  192 => 69,  188 => 67,  184 => 66,  175 => 64,  168 => 59,  158 => 55,  152 => 54,  148 => 53,  144 => 51,  140 => 50,  131 => 48,  125 => 44,  117 => 41,  110 => 39,  105 => 37,  102 => 36,  100 => 35,  93 => 33,  86 => 31,  80 => 28,  77 => 27,  75 => 26,  66 => 24,  47 => 7,  45 => 6,  43 => 5,  41 => 4,  39 => 3,  37 => 2,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% set records = builderList.records %}
{% set displayColumn = builderList.displayColumn %}
{% set noRecordsMessage = builderList.noRecordsMessage %}
{% set detailsPage = builderList.detailsPage %}
{% set detailsKeyColumn = builderList.detailsKeyColumn %}
{% set detailsUrlParameter = builderList.detailsUrlParameter %}

<nav class=\"navbar-light navbar-expand-md pl-3 \">
  <div class=\"row w-100 m-0\">
    <div class=\"col-9 p-0 \" >
      <a href=\"#\" class=\"logo navbar-brand\" style=\"padding-top:12px;\">
        Ассортимент
      </a>
    </div>
    <div class=\"col-3 \">
      <button class=\"navbar-toggler my-2\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarSupportedContent1\"
        aria-controls=\"navbarSupportedContent1\" id=\"navtitle\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
        <span class=\"navbar-toggler-icon\"> </span>
      </button>
    </div>  
  </div>

<div class=\"left-menu collapse show\" id=\"navbarSupportedContent1\">
<!-- {%if 'size' in input|keys or 'type' in input|keys or 'country' in input|keys or 'color' in input|keys %}class=\"left-menu collapse in show\" {% else %} class=\"left-menu collapse\"{% endif %}>  -->

{% if this.param.slug == 'man' %}

    <form method=\"GET\" action=\"{{ url('goods/man/1?')}}\">

                  <input type=\"checkbox\" name=\"gender[]\" value=\"мужской\" class=\"unvisible\" checked=\"checked\"
                  {%if 'мужской' in input['gender'] %}checked=\"on\"{%endif%}>
                  <input type=\"checkbox\" name=\"gender[]\" value=\"унисекс\" class=\"unvisible\" checked=\"checked\"
                  {%if 'унисекс' in input['gender'] %}checked=\"on\"{%endif%}>

    {% elseif this.param.slug == 'woman' %}

    <form method=\"GET\" action=\"{{ url('goods/woman/1?')}}\">
                <input type=\"checkbox\" name=\"gender[]\" value=\"женский\" class=\"unvisible\" checked=\"checked\"
                {%if 'женский' in input['gender'] %}checked=\"on\"{%endif%}>
                <input type=\"checkbox\" name=\"gender[]\" value=\"унисекс\" class=\"unvisible\" checked=\"checked\"
                {%if 'унисекс' in input['gender'] %}checked=\"on\"{%endif%}>

    {% endif %}

      <ul class=\"p-0 dark-link\"> 
        <li> 
          <a href=\"#\" data-toggle=\"collapse\" data-target=\"#left-menu1\"><h6> Тип одежды </h6></a> 
          <div  {%if 'type' in input|keys %}class=\"collapse in show\" {% else %} class=\"collapse\"{% endif %}   id=\"left-menu1\"> 
            <ul class=\"ul-li p-0\"> 
              {%for t in t_type%}
              <li>
                <label>
                  <input type=\"checkbox\" name=\"type[]\" value=\"{{t.id}}\"
                  {%if t.id in input['type'] %}checked=\"on\"{%endif%}>
                  <span>{{t.type}}</span>
                </label>
              </li>
              {%endfor%}
            </ul>   
          </div>
        </li> 
        <li> 
          <a href=\"#\" data-toggle=\"collapse\" data-target=\"#left-menu7\"> <h6> Производитель </h6></a> 
          <div  {%if 'manufacturer' in input|keys %}class=\"collapse in show\"{% else %} class=\"collapse\" {% endif %}   id=\"left-menu7\"> 
            <ul class=\"ul-li p-0\"> 
              {%for m in m_manufacturer%}
              <li>
                <label>
                  <input type=\"checkbox\" name=\"manufacturer[]\" value=\"{{m.id}}\"
                  {%if m.id in input['manufacturer'] %}checked=\"on\"{%endif%}>
                  <span>{{m.manufacturer}}</span>
                </label>
              </li>
              {%endfor%}
            </ul>   
          </div>
        </li> 
        <li> 
          <a href=\"#\" data-toggle=\"collapse\" data-target=\"#left-menu2\"> <h6> Страна </h6></a> 
          <div  {%if 'country' in input|keys %}class=\"collapse in show\"{% else %} class=\"collapse\" {% endif %}   id=\"left-menu2\"> 
            <ul class=\"ul-li p-0\"> 
              {%for c in s_country%}
              <li>
                <label>
                  <input type=\"checkbox\" name=\"country[]\" value=\"{{c.id}}\"
                  {%if c.id in input['country'] %}checked=\"on\"{%endif%}>
                  <span>{{c.country}}</span>
                </label>
              </li>
              {%endfor%}
            </ul>   
          </div>
        </li> 
        <li> 
          <a href=\"#\" data-toggle=\"collapse\" data-target=\"#left-menu3\"> <h6> Цвет </h6></a> 
          <div  {%if 'color' in input|keys %}class=\"collapse in show\"{% else %} class=\"collapse\" {% endif %}   id=\"left-menu3\"> 
            <ul class=\"ul-li p-0\"> 
              {%for p in p_product%}
              <li>
                <label>
                  <input type=\"checkbox\" name=\"color[]\" value=\"{{p.color}}\"
                  {%if p.color in input['color'] %}checked=\"on\"{%endif%}>
                  <span>{{p.color}}</span>
                </label>
              </li>
              {%endfor%}
            </ul>   
          </div>                
        </li> 
        <li> 
          <a href=\"#\" data-toggle=\"collapse\" data-target=\"#left-menu4\"> <h6> Размер </h6></a> 
          <div  {%if 'size' in input|keys %}class=\"collapse in show\"{% else %} class=\"collapse\" {% endif %} id=\"left-menu4\"> 
            <ul class=\"ul-li p-0\">
              {%for a in a_availability%}
              <li>
                <label>
                  <input type=\"checkbox\" name=\"size[]\" value=\"{{a.size}}\"
                  {%if a.size in input['size'] %}checked=\"on\"{%endif%}>
                  <span>{{a.size}}</span>
                </label>
              </li>
              {%endfor%}
            </ul>   
          </div>
        </li>
        <li> 
          <a href=\"#\" data-toggle=\"collapse\" data-target=\"#left-menu5\"> <h6> Скидки </h6></a> 
          <div  {%if 'discount' in input|keys %}class=\"collapse in show\"{% else %} class=\"collapse\" {% endif %} id=\"left-menu5\"> 
            <ul class=\"ul-li p-0\">
              <li>
                <label>
                  <input type=\"checkbox\" name=\"discount\" 
                  {%if input['discount']=='on' %}checked{%endif%}>
                  <span>Показать товары со скидкой</span>
                </label>
              </li>  
            </ul>   
          </div>
        </li>
        <li> 
            <a href=\"#\" data-toggle=\"collapse\" data-target=\"#left-menu6\"> <h6> Сортировка </h6></a> 
            <div class=\"collapse\" id=\"left-menu6\"> 
              <ul class=\"ul-li p-0\">
                <li>
                  <label class=\"type_checkbox\">
                    <input  type=\"radio\" name=\"price\" value= \"asc\" onMouseDown=\"this.isChecked=this.checked;\"
                    onClick=\"this.checked=!this.isChecked;\"
                    {%if input['price']=='asc' %}checked=\"on\"{%endif%}>
                    <span>Цена по возрастанию</span>
                  </label>
                </li>  
                <li>
                    <label>
                        <input  type=\"radio\" name=\"price\" value=\"desc\" onMouseDown=\"this.isChecked=this.checked;\"
                        onClick=\"this.checked=!this.isChecked;\"
                        {%if input['price']=='desc' %}checked=\"on\"{%endif%}>
                        <span>Цена по убыванию</span>
                    </label>
                </li>
                <li>
                  <label class=\"type_checkbox\">
                    <input  type=\"radio\" name=\"sort\" value= \"new\" onMouseDown=\"this.isChecked=this.checked;\"
                    onClick=\"this.checked=!this.isChecked;\"
                    {%if input['sort']=='new' %}checked=\"on\"{%endif%}>
                    <span>Сначала новые</span>
                  </label>
                </li>  
                <li>
                    <label>
                      <input  type=\"radio\" name=\"sort\" value=\"old\"  onMouseDown=\"this.isChecked=this.checked;\"
                      onClick=\"this.checked=!this.isChecked;\"
                      {%if input['sort']=='old' %}checked=\"on\"{%endif%}>
                      <span>Сначала старые</span>
                    </label>
                </li>
              </ul>   
            </div>
          </li>
        <button class=\"btn btn-dark mt-3\" type=\"submit\"> 
          Фильтровать
        </button> 
      </ul> 
    </form>
  </div>
</nav>

<script>
  function click()
  {
    this.checked = false;
  }
</script>", "C:\\wamp64\\www\\project/themes/mine/partials/left_menu.htm", "");
    }
}
