<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* C:\wamp64\www\project/themes/mine/pages/checkout.htm */
class __TwigTemplate_70425d96bd982f8cb8352ec5f605e1eef8d83e04b5a0f43506470c21dde82591 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        if ( !($context["user"] ?? null)) {
            // line 2
            echo "        <div class=\"col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 pt-md-5 p-3\">
            ";
            // line 3
            $context['__cms_partial_params'] = [];
            echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction((($context["account"] ?? null) . "::signin")            , $context['__cms_partial_params']            , true            );
            unset($context['__cms_partial_params']);
            // line 4
            echo "        </div>
";
        } else {
            // line 6
            $context['__cms_partial_params'] = [];
            echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("checkout"            , $context['__cms_partial_params']            , true            );
            unset($context['__cms_partial_params']);
        }
        // line 8
        echo "
<script type=\"text/javascript\" src=\"";
        // line 9
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/javascript/jquery.maskedinput-master/src/jquery.maskedinput.js");
        echo "\" ></script>  
<script type=\"text/javascript\" src=\"";
        // line 10
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/javascript/alexdi/account.js");
        echo "\" ></script>";
    }

    public function getTemplateName()
    {
        return "C:\\wamp64\\www\\project/themes/mine/pages/checkout.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  60 => 10,  56 => 9,  53 => 8,  48 => 6,  44 => 4,  40 => 3,  37 => 2,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% if not user %}
        <div class=\"col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 pt-md-5 p-3\">
            {% partial account ~ '::signin' %}
        </div>
{% else %}
{% partial \"checkout\" %}
{% endif %}

<script type=\"text/javascript\" src=\"{{ 'assets/javascript/jquery.maskedinput-master/src/jquery.maskedinput.js'|theme }}\" ></script>  
<script type=\"text/javascript\" src=\"{{ 'assets/javascript/alexdi/account.js'|theme }}\" ></script>", "C:\\wamp64\\www\\project/themes/mine/pages/checkout.htm", "");
    }
}
