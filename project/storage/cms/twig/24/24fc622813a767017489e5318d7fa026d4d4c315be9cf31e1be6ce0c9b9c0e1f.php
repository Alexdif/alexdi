<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* C:\wamp64\www\project/themes/mine/layouts/default.htm */
class __TwigTemplate_710ab8e13ba9a22ac81d4afb149071e0d7decf028d52b57b85b8020a3c0219f1 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<!DOCTYPE html>
<html>
        <head>
                <meta charset=\"utf-8\">
                <title> ZHESTKO ";
        // line 5
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "page", [], "any", false, false, false, 5), "title", [], "any", false, false, false, 5), "html", null, true);
        echo "</title>
                <meta name=\"author\" content=\"AlexDi\">
                <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, shirink-to-fit=no\">
                <link rel=\"icon\" type=\"image/png\" href=\"";
        // line 8
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/main.png");
        echo "\">

                <script type=\"text/javascript\" src=\"";
        // line 10
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/javascript/jquery/dist/jquery.js");
        echo "\"></script>
                <script type=\"text/javascript\" src=\"";
        // line 11
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/javascript/popper.js/dist/umd/popper.js");
        echo "\"></script>
                <script type=\"text/javascript\" src=\"";
        // line 12
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/javascript/tooltip.js/dist/umd/tooltip.js");
        echo "\"></script>
        
                <!-- LESS стили CSS-->
                <link  rel=\"stylesheet/less\" type=\"text/css\" href=\"";
        // line 15
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/css/styles.less");
        echo "\">  
                <!-- LESS препроцессор-->
                <script type=\"text/javascript\" src=\"";
        // line 17
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/javascript/less.min.js");
        echo "\" ></script>  
                <!-- Bootstrap -->
                <link rel=\"stylesheet\" href=\"";
        // line 19
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/css/bootstrap/bootstrap.min.css");
        echo "\">

        ";
        // line 21
        echo $this->env->getExtension('Cms\Twig\Extension')->assetsFunction('css');
        echo $this->env->getExtension('Cms\Twig\Extension')->displayBlock('styles');
        // line 22
        echo "        </head>
        <body>

        <div class=\"wrapper\">
                ";
        // line 26
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("header_top"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 27
        echo "                ";
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("header_bottom"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 28
        echo "                <div class=\"content\">
                        ";
        // line 29
        echo $this->env->getExtension('Cms\Twig\Extension')->pageFunction();
        // line 30
        echo "                </div>
                <div class=\"footer\">
                        ";
        // line 32
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("footer"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 33
        echo "                </div>
        </div>

        <!-- Scripts -->
        
        <script type=\"text/javascript\" src=\"";
        // line 38
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/javascript/bootstrap/bootstrap.min.js");
        echo "\"></script>
        <script type=\"text/javascript\" src=\"";
        // line 39
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/javascript/app.js");
        echo "\"></script>
        
        
        ";
        // line 42
        echo $this->env->getExtension('Cms\Twig\Extension')->assetsFunction('js');
        echo $this->env->getExtension('Cms\Twig\Extension')->displayBlock('scripts');
        // line 43
        echo "        ";
        $_minify = System\Classes\CombineAssets::instance()->useMinify;
        echo '<script src="'. Request::getBasePath()
                .'/modules/system/assets/js/framework'.($_minify ? '-min' : '').'.js"></script>'.PHP_EOL;
        unset($_minify);
        // line 44
        echo "        ";
        $_minify = System\Classes\CombineAssets::instance()->useMinify;
        if ($_minify) {
            echo '<script src="'. Request::getBasePath()
                    .'/modules/system/assets/js/framework.combined-min.js"></script>'.PHP_EOL;
        }
        else {
            echo '<script src="'. Request::getBasePath()
                    .'/modules/system/assets/js/framework.js"></script>'.PHP_EOL;
            echo '<script src="'. Request::getBasePath()
                    .'/modules/system/assets/js/framework.extras.js"></script>'.PHP_EOL;
        }
        echo '<link rel="stylesheet" property="stylesheet" href="'. Request::getBasePath()
                    .'/modules/system/assets/css/framework.extras'.($_minify ? '-min' : '').'.css">'.PHP_EOL;
        unset($_minify);
        // line 45
        echo "        </body>
</html>";
    }

    public function getTemplateName()
    {
        return "C:\\wamp64\\www\\project/themes/mine/layouts/default.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  154 => 45,  138 => 44,  132 => 43,  129 => 42,  123 => 39,  119 => 38,  112 => 33,  108 => 32,  104 => 30,  102 => 29,  99 => 28,  94 => 27,  90 => 26,  84 => 22,  81 => 21,  76 => 19,  71 => 17,  66 => 15,  60 => 12,  56 => 11,  52 => 10,  47 => 8,  41 => 5,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>
<html>
        <head>
                <meta charset=\"utf-8\">
                <title> ZHESTKO {{ this.page.title }}</title>
                <meta name=\"author\" content=\"AlexDi\">
                <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, shirink-to-fit=no\">
                <link rel=\"icon\" type=\"image/png\" href=\"{{ 'assets/images/main.png'|theme }}\">

                <script type=\"text/javascript\" src=\"{{ 'assets/javascript/jquery/dist/jquery.js'|theme }}\"></script>
                <script type=\"text/javascript\" src=\"{{ 'assets/javascript/popper.js/dist/umd/popper.js'|theme }}\"></script>
                <script type=\"text/javascript\" src=\"{{ 'assets/javascript/tooltip.js/dist/umd/tooltip.js'|theme }}\"></script>
        
                <!-- LESS стили CSS-->
                <link  rel=\"stylesheet/less\" type=\"text/css\" href=\"{{ 'assets/css/styles.less'|theme }}\">  
                <!-- LESS препроцессор-->
                <script type=\"text/javascript\" src=\"{{ 'assets/javascript/less.min.js'|theme }}\" ></script>  
                <!-- Bootstrap -->
                <link rel=\"stylesheet\" href=\"{{ 'assets/css/bootstrap/bootstrap.min.css'|theme }}\">

        {% styles %}
        </head>
        <body>

        <div class=\"wrapper\">
                {% partial \"header_top\" %}
                {% partial \"header_bottom\" %}
                <div class=\"content\">
                        {% page %}
                </div>
                <div class=\"footer\">
                        {% partial \"footer\" %}
                </div>
        </div>

        <!-- Scripts -->
        
        <script type=\"text/javascript\" src=\"{{ 'assets/javascript/bootstrap/bootstrap.min.js'|theme }}\"></script>
        <script type=\"text/javascript\" src=\"{{ 'assets/javascript/app.js'|theme }}\"></script>
        
        
        {% scripts %}
        {% framework %}
        {% framework extras %}
        </body>
</html>", "C:\\wamp64\\www\\project/themes/mine/layouts/default.htm", "");
    }
}
