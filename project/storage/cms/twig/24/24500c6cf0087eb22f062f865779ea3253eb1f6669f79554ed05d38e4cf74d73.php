<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* C:\wamp64\www\project/themes/mine/pages/account.htm */
class __TwigTemplate_86e8fda2c699c605ca3f7abbe084ebad704030a1e499ec45d6bde82bd57d4a57 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<div class=\"container\">
    <div class=\"row\">
        ";
        // line 3
        if ( !($context["user"] ?? null)) {
            // line 4
            echo "        <div class=\"col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 pt-md-5 p-3\">
            ";
            // line 5
            $context['__cms_partial_params'] = [];
            echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction((($context["account"] ?? null) . "::signin")            , $context['__cms_partial_params']            , true            );
            unset($context['__cms_partial_params']);
            // line 6
            echo "        </div>
        

        ";
        } else {
            // line 10
            echo "        <div class=\"col-12 p-3\">
            ";
            // line 11
            $context['__cms_partial_params'] = [];
            echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction((($context["account"] ?? null) . "::activation_check")            , $context['__cms_partial_params']            , true            );
            unset($context['__cms_partial_params']);
            // line 12
            echo "        </div>
        <div class=\"col-12 p-3\">
                    ";
            // line 14
            $context['__cms_partial_params'] = [];
            echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction((($context["account"] ?? null) . "::update")            , $context['__cms_partial_params']            , true            );
            unset($context['__cms_partial_params']);
            // line 15
            echo "        </div>
        <div class=\"col-12 p-3\">
            ";
            // line 17
            $context['__cms_partial_params'] = [];
            echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction((($context["account"] ?? null) . "::deactivate_link")            , $context['__cms_partial_params']            , true            );
            unset($context['__cms_partial_params']);
            // line 18
            echo "        </div>

        ";
        }
        // line 20
        echo " 
    </div>
</div>
<script type=\"text/javascript\" src=\"";
        // line 23
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/javascript/jquery.maskedinput-master/src/jquery.maskedinput.js");
        echo "\" ></script>  
<script type=\"text/javascript\" src=\"";
        // line 24
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/javascript/alexdi/account.js");
        echo "\" ></script>";
    }

    public function getTemplateName()
    {
        return "C:\\wamp64\\www\\project/themes/mine/pages/account.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  91 => 24,  87 => 23,  82 => 20,  77 => 18,  73 => 17,  69 => 15,  65 => 14,  61 => 12,  57 => 11,  54 => 10,  48 => 6,  44 => 5,  41 => 4,  39 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"container\">
    <div class=\"row\">
        {% if not user %}
        <div class=\"col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 pt-md-5 p-3\">
            {% partial account ~ '::signin' %}
        </div>
        

        {% else %}
        <div class=\"col-12 p-3\">
            {% partial account  ~ '::activation_check' %}
        </div>
        <div class=\"col-12 p-3\">
                    {% partial account ~ '::update' %}
        </div>
        <div class=\"col-12 p-3\">
            {% partial account ~ '::deactivate_link' %}
        </div>

        {% endif %} 
    </div>
</div>
<script type=\"text/javascript\" src=\"{{ 'assets/javascript/jquery.maskedinput-master/src/jquery.maskedinput.js'|theme }}\" ></script>  
<script type=\"text/javascript\" src=\"{{ 'assets/javascript/alexdi/account.js'|theme }}\" ></script>", "C:\\wamp64\\www\\project/themes/mine/pages/account.htm", "");
    }
}
