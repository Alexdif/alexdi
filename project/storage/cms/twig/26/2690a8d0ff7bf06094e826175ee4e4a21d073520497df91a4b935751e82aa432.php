<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* C:\wamp64\www\project/themes/mine/partials/carousel_all_width.htm */
class __TwigTemplate_c4feaa3539c8e25ba6d45718df6807168d22558d6b641f91a194fb0e32f9c4d2 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<div class=\"container-fluid p-0 \">
  <div id=\"carouselExampleIndicators\" class=\"carousel slide\"
  data-ride=\"carousel\">
    <ol class=\"carousel-indicators\">
      <li class=\"active d-none d-md-block\" data-target=\"#carouselExampleIndicators\" data-slide-to=\"0\">
      </li>
      <li class=\"d-none d-md-block\" data-target=\"#carouselExampleIndicators\" data-slide-to=\"1\">
      </li>
      <li class=\"d-none d-md-block\" data-target=\"#carouselExampleIndicators\" data-slide-to=\"2\">
      </li>
    </ol>
    
      <div class=\"carousel-inner\">
        <div class=\"carousel-item active\">
          <a href=\"http://zhestko.hopto.org:8080/project/product/ADIDAS_EQUIPMENT_SUPPORT_A_green\">
          <img src=\"";
        // line 16
        echo $this->extensions['Cms\Twig\Extension']->themeFilter(("assets/images/" . ($context["way1"] ?? null)));
        echo "\" alt=\"one\" class=\"d-block w-100\">
          </a>
        </div>
        <div class=\"carousel-item\">
            <a href=\"http://zhestko.hopto.org:8080/project/goods/man/1?gender%5B%5D=%D0%BC%D1%83%D0%B6%D1%81%D0%BA%D0%BE%D0%B9&gender%5B%5D=%D1%83%D0%BD%D0%B8%D1%81%D0%B5%D0%BA%D1%81&type%5B%5D=1&manufacturer%5B%5D=2\">
          <img src=\"";
        // line 21
        echo $this->extensions['Cms\Twig\Extension']->themeFilter(("assets/images/" . ($context["way2"] ?? null)));
        echo "\" alt=\"two\" class=\"d-block w-100\">
          </a>
        </div>
        <div class=\"carousel-item\">
            <a href=\"http://zhestko.hopto.org:8080/project/goods/woman/?gender%5B%5D=%D0%B6%D0%B5%D0%BD%D1%81%D0%BA%D0%B8%D0%B9&gender%5B%5D=%D1%83%D0%BD%D0%B8%D1%81%D0%B5%D0%BA%D1%81\">
          <img src=\"";
        // line 26
        echo $this->extensions['Cms\Twig\Extension']->themeFilter(("assets/images/" . ($context["way3"] ?? null)));
        echo "\"  alt=\"three\" class=\"d-block w-100\">
          </a>
        </div>
      </div>

      <a href=\"#carouselExampleIndicators\" class=\"carousel-control-prev\"
      role=\"button\" data-slide=\"prev\">
      <span class=\"carousel-control-prev-icon\" aria-hidden=\"true\"></span>
      <span class=\"sr-only\">Предыдущая</span>
      <a href=\"#carouselExampleIndicators\" class=\"carousel-control-next\"
      role=\"button\" data-slide=\"next\">
      <span class=\"carousel-control-next-icon\" aria-hidden=\"true\"></span>
      <span class=\"sr-only\">Следущая</span>
      </a>
    
  </div>
</div>";
    }

    public function getTemplateName()
    {
        return "C:\\wamp64\\www\\project/themes/mine/partials/carousel_all_width.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  68 => 26,  60 => 21,  52 => 16,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"container-fluid p-0 \">
  <div id=\"carouselExampleIndicators\" class=\"carousel slide\"
  data-ride=\"carousel\">
    <ol class=\"carousel-indicators\">
      <li class=\"active d-none d-md-block\" data-target=\"#carouselExampleIndicators\" data-slide-to=\"0\">
      </li>
      <li class=\"d-none d-md-block\" data-target=\"#carouselExampleIndicators\" data-slide-to=\"1\">
      </li>
      <li class=\"d-none d-md-block\" data-target=\"#carouselExampleIndicators\" data-slide-to=\"2\">
      </li>
    </ol>
    
      <div class=\"carousel-inner\">
        <div class=\"carousel-item active\">
          <a href=\"http://zhestko.hopto.org:8080/project/product/ADIDAS_EQUIPMENT_SUPPORT_A_green\">
          <img src=\"{{ ('assets/images/'~way1)|theme }}\" alt=\"one\" class=\"d-block w-100\">
          </a>
        </div>
        <div class=\"carousel-item\">
            <a href=\"http://zhestko.hopto.org:8080/project/goods/man/1?gender%5B%5D=%D0%BC%D1%83%D0%B6%D1%81%D0%BA%D0%BE%D0%B9&gender%5B%5D=%D1%83%D0%BD%D0%B8%D1%81%D0%B5%D0%BA%D1%81&type%5B%5D=1&manufacturer%5B%5D=2\">
          <img src=\"{{ ('assets/images/'~way2)|theme }}\" alt=\"two\" class=\"d-block w-100\">
          </a>
        </div>
        <div class=\"carousel-item\">
            <a href=\"http://zhestko.hopto.org:8080/project/goods/woman/?gender%5B%5D=%D0%B6%D0%B5%D0%BD%D1%81%D0%BA%D0%B8%D0%B9&gender%5B%5D=%D1%83%D0%BD%D0%B8%D1%81%D0%B5%D0%BA%D1%81\">
          <img src=\"{{ ('assets/images/'~way3)|theme }}\"  alt=\"three\" class=\"d-block w-100\">
          </a>
        </div>
      </div>

      <a href=\"#carouselExampleIndicators\" class=\"carousel-control-prev\"
      role=\"button\" data-slide=\"prev\">
      <span class=\"carousel-control-prev-icon\" aria-hidden=\"true\"></span>
      <span class=\"sr-only\">Предыдущая</span>
      <a href=\"#carouselExampleIndicators\" class=\"carousel-control-next\"
      role=\"button\" data-slide=\"next\">
      <span class=\"carousel-control-next-icon\" aria-hidden=\"true\"></span>
      <span class=\"sr-only\">Следущая</span>
      </a>
    
  </div>
</div>", "C:\\wamp64\\www\\project/themes/mine/partials/carousel_all_width.htm", "");
    }
}
