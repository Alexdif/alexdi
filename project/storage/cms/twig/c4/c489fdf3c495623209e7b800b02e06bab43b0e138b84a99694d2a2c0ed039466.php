<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* C:\wamp64\www\project/themes/mine/partials/account/update.htm */
class __TwigTemplate_96f1fd85d1f023085fa75d1ce2e88020e68d34a2f68f4c62213fa92c6537ae61 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo call_user_func_array($this->env->getFunction('form_ajax')->getCallable(), ["ajax", "onUpdate"]);
        echo "
<h4>  Личные данные  </h4>
<div class=\"lch-container row mt-3\">
    <div class=\"form-group col-12 col-md-6\">
        <label for=\"accountName\">Имя</label>
        <input name=\"name\" type=\"text\" class=\"form-control\" id=\"accountName\" value=\"";
        // line 6
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "name", [], "any", false, false, false, 6), "html", null, true);
        echo "\">
    </div>

    <div class=\"form-group col-12 col-md-6\">
        <label for=\"accountSurname\">Фамилия</label>
        <input name=\"surname\" type=\"text\" class=\"form-control\" id=\"accountSurname\" value=\"";
        // line 11
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "surname", [], "any", false, false, false, 11), "html", null, true);
        echo "\">
    </div>

    <div class=\"form-group col-12\">
        <label for=\"accountEmail\">Email</label>
        <input name=\"email\" type=\"text\" class=\"form-control\" id=\"accountEmail\" value=\"";
        // line 16
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "email", [], "any", false, false, false, 16), "html", null, true);
        echo "\">
    </div>
</div>
    <div class=\"lch-container row\">
    <div class=\"form-group col-12\">
        <label>Aдресс</label>
        <textarea name=\"address\" type=\"text\" class=\"form-control\" id=\"accountAddress\"> ";
        // line 22
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "address", [], "any", false, false, false, 22), "html", null, true);
        echo " </textarea>
    </div>

    <div class=\"form-group col-12 col-md-6\">
        <label>Индекс</label>
        <input name=\"index\" type=\"text\" class=\"form-control\" id=\"accountIndex\" value=\"";
        // line 27
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "index", [], "any", false, false, false, 27), "html", null, true);
        echo "\">
    </div>

    <div class=\"form-group col-12 col-md-6\">
        <label>Телефон</label>
        <input name=\"telephone\" type=\"text\" class=\"form-control\" id=\"accountTelephone\" value=\"";
        // line 32
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "telephone", [], "any", false, false, false, 32), "html", null, true);
        echo "\">
    </div>
    </div>

    <div class=\"lch-container row\">
        <div class=\"form-group col-12\">
            <label for=\"accountPassword\">Пароль</label>
            <input name=\"password\" type=\"password\" class=\"form-control\" id=\"accountPassword\">
        </div>

        <div class=\"form-group col-12\">
            <label for=\"accountPasswordConfirm\">Новый пароль</label>
            <input name=\"password_confirmation\" type=\"password\" class=\"form-control\" id=\"accountPasswordConfirm\">
        </div>
    </div>
    
    <div class=\"row\">
        <div class=\"text-left col-6\"> 
                <button type=\"submit\" class=\"btn btn-dark mt-1\">Сохранить</button>
                <button class=\"btn btn-secondary mt-1\" type=\"submit\" data-request=\"onLogout\" data-request-data=\"redirect: '/account'\"> Выйти </button>
        </div>
        <div class=\"text-right col-6\"> 
                
                    <a href=\"/project/history\" class=\"btn btn-dark mt-1\" type=\"button\"> История заказов </a>
                
        </div>
    </div>
";
        // line 59
        echo call_user_func_array($this->env->getFunction('form_close')->getCallable(), ["close"]);
    }

    public function getTemplateName()
    {
        return "C:\\wamp64\\www\\project/themes/mine/partials/account/update.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  114 => 59,  84 => 32,  76 => 27,  68 => 22,  59 => 16,  51 => 11,  43 => 6,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{{ form_ajax('onUpdate') }}
<h4>  Личные данные  </h4>
<div class=\"lch-container row mt-3\">
    <div class=\"form-group col-12 col-md-6\">
        <label for=\"accountName\">Имя</label>
        <input name=\"name\" type=\"text\" class=\"form-control\" id=\"accountName\" value=\"{{ user.name }}\">
    </div>

    <div class=\"form-group col-12 col-md-6\">
        <label for=\"accountSurname\">Фамилия</label>
        <input name=\"surname\" type=\"text\" class=\"form-control\" id=\"accountSurname\" value=\"{{ user.surname }}\">
    </div>

    <div class=\"form-group col-12\">
        <label for=\"accountEmail\">Email</label>
        <input name=\"email\" type=\"text\" class=\"form-control\" id=\"accountEmail\" value=\"{{ user.email }}\">
    </div>
</div>
    <div class=\"lch-container row\">
    <div class=\"form-group col-12\">
        <label>Aдресс</label>
        <textarea name=\"address\" type=\"text\" class=\"form-control\" id=\"accountAddress\"> {{ user.address }} </textarea>
    </div>

    <div class=\"form-group col-12 col-md-6\">
        <label>Индекс</label>
        <input name=\"index\" type=\"text\" class=\"form-control\" id=\"accountIndex\" value=\"{{ user.index }}\">
    </div>

    <div class=\"form-group col-12 col-md-6\">
        <label>Телефон</label>
        <input name=\"telephone\" type=\"text\" class=\"form-control\" id=\"accountTelephone\" value=\"{{ user.telephone }}\">
    </div>
    </div>

    <div class=\"lch-container row\">
        <div class=\"form-group col-12\">
            <label for=\"accountPassword\">Пароль</label>
            <input name=\"password\" type=\"password\" class=\"form-control\" id=\"accountPassword\">
        </div>

        <div class=\"form-group col-12\">
            <label for=\"accountPasswordConfirm\">Новый пароль</label>
            <input name=\"password_confirmation\" type=\"password\" class=\"form-control\" id=\"accountPasswordConfirm\">
        </div>
    </div>
    
    <div class=\"row\">
        <div class=\"text-left col-6\"> 
                <button type=\"submit\" class=\"btn btn-dark mt-1\">Сохранить</button>
                <button class=\"btn btn-secondary mt-1\" type=\"submit\" data-request=\"onLogout\" data-request-data=\"redirect: '/account'\"> Выйти </button>
        </div>
        <div class=\"text-right col-6\"> 
                
                    <a href=\"/project/history\" class=\"btn btn-dark mt-1\" type=\"button\"> История заказов </a>
                
        </div>
    </div>
{{ form_close() }}", "C:\\wamp64\\www\\project/themes/mine/partials/account/update.htm", "");
    }
}
