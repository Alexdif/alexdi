<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* C:\wamp64\www\project/themes/mine/partials/account/signin.htm */
class __TwigTemplate_a534cd9b78a798ea13a433e2b779b060960289e0d6b2e19c37d3c8bdc213c889 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<h3>Авторизация</h3>
<form
    data-request=\"onSignin\" data-request-data=\"redirect: '";
        // line 3
        echo twig_escape_filter($this->env, ($context["redirect_url"] ?? null), "html", null, true);
        echo "'\">
    <div class=\"form-group\">
        <label for=\"userSigninLogin\">";
        // line 5
        echo twig_escape_filter($this->env, ($context["loginAttributeLabel"] ?? null), "html", null, true);
        echo "</label>
        <input
            name=\"login\"
            type=\"text\"
            class=\"form-control\"
            id=\"userSigninLogin\"
            placeholder=\"Введите вашу почту\" />
            <!--  ";
        // line 12
        echo twig_escape_filter($this->env, twig_lower_filter($this->env, ($context["loginAttributeLabel"] ?? null)), "html", null, true);
        echo " -->
    </div>

    <div class=\"form-group\">
        <label for=\"userSigninPassword\">Пароль</label>
        <input
            name=\"password\"
            type=\"password\"
            class=\"form-control\"
            id=\"userSigninPassword\"
            placeholder=\"Введите пароль\" />
    </div>
    <button type=\"submit\" class=\"btn btn-dark\">Войти</button>
    <a class=\"dark-link pl-3\" href=\"/project/registration\"> Зарегестрироваться </a>
</form>";
    }

    public function getTemplateName()
    {
        return "C:\\wamp64\\www\\project/themes/mine/partials/account/signin.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  54 => 12,  44 => 5,  39 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<h3>Авторизация</h3>
<form
    data-request=\"onSignin\" data-request-data=\"redirect: '{{redirect_url}}'\">
    <div class=\"form-group\">
        <label for=\"userSigninLogin\">{{ loginAttributeLabel }}</label>
        <input
            name=\"login\"
            type=\"text\"
            class=\"form-control\"
            id=\"userSigninLogin\"
            placeholder=\"Введите вашу почту\" />
            <!--  {{ loginAttributeLabel|lower }} -->
    </div>

    <div class=\"form-group\">
        <label for=\"userSigninPassword\">Пароль</label>
        <input
            name=\"password\"
            type=\"password\"
            class=\"form-control\"
            id=\"userSigninPassword\"
            placeholder=\"Введите пароль\" />
    </div>
    <button type=\"submit\" class=\"btn btn-dark\">Войти</button>
    <a class=\"dark-link pl-3\" href=\"/project/registration\"> Зарегестрироваться </a>
</form>", "C:\\wamp64\\www\\project/themes/mine/partials/account/signin.htm", "");
    }
}
