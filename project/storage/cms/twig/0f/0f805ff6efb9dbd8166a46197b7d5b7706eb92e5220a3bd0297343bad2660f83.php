<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* C:\wamp64\www\project/themes/mine/pages/history.htm */
class __TwigTemplate_269342e8c2e5be10546e4e25de20c5d8038142ed311a7ecbec96ed0268220b90 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        $context["records"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "records", [], "any", false, false, false, 1);
        // line 2
        $context["displayColumn"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "displayColumn", [], "any", false, false, false, 2);
        // line 3
        $context["noRecordsMessage"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "noRecordsMessage", [], "any", false, false, false, 3);
        // line 4
        $context["detailsPage"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "detailsPage", [], "any", false, false, false, 4);
        // line 5
        $context["detailsKeyColumn"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "detailsKeyColumn", [], "any", false, false, false, 5);
        // line 6
        $context["detailsUrlParameter"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "detailsUrlParameter", [], "any", false, false, false, 6);
        // line 7
        echo "<div class=\"container p-3 pl-4\">
        <h1> История заказов </h1>
</div>
<div class=\"bg_t\">
        <div class=\"container p-3\">
            <div class=\"row\">
                    ";
        // line 13
        $context["full_price"] = 0;
        // line 14
        echo "                    ";
        $context["full_quantity"] = 0;
        // line 15
        echo "                    ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["records"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["record"]) {
            // line 16
            echo "                        ";
            $context["full_price"] = (($context["full_price"] ?? null) + twig_get_attribute($this->env, $this->source, $context["record"], "price", [], "any", false, false, false, 16));
            // line 17
            echo "                        ";
            $context["full_quantity"] = (($context["full_quantity"] ?? null) + 1);
            // line 18
            echo "                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['record'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 19
        echo "                    ";
        if ((($context["full_quantity"] ?? null) > 0)) {
            // line 20
            echo "                <div class=\"col-6 col-sm-8 col-md-9 col-lg-10\">
                    
                </div>
                ";
        } else {
            // line 24
            echo "                <h4 class=\"p-3\"> ";
            echo twig_escape_filter($this->env, ($context["noRecordsMessage"] ?? null), "html", null, true);
            echo " </h4> 
                ";
        }
        // line 26
        echo "                <div class=\"col-6 col-sm-4 col-md-3 col-lg-2 my-auto text-center p-0\">
                    ";
        // line 27
        if ((($context["full_quantity"] ?? null) > 0)) {
            echo " 
                    <form action=\"/project/account\"> 
                        <button class=\"btn btn-oytline-success my-2 my-sm-0 btn-dark\"> Личный кабинет </button>
                    </form>
                    
                    ";
        } else {
            // line 32
            echo " ";
        }
        echo "                                           
                </div> 
            </div>
        </div>
</div>
<div class=\"mx-auto container w-100 m-0\">
    <main class=\"col-12  p-0 pt-3\">
        
        <div class=\"row text-center col-12 w-100 m-0 p-0\">
            ";
        // line 41
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["records"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["record"]) {
            // line 42
            echo "                ";
            ob_start(function () { return ''; });
            echo "          
                    <div class=\"col-12 mb-3\" >
                        <div class=\"col-12 py-3 bg_t dark-link-1 item h-100\"> 

                            <div class=\"row\">
                                <div class=\"col-12 col-md-6 my-auto\">
                                        
                                <div class=\"text-center row \">
                                    <div class=\"col-12\">

                                        <h3> ID: ";
            // line 52
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["record"], "order_id", [], "any", false, false, false, 52), "html", null, true);
            echo " </h3> 

                                    </div>
                                    <div class=\"col-12 p-0 pt-md-0 my-auto \">

                                            <h4> ";
            // line 57
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["record"], "pickup", [], "any", false, false, false, 57), "html", null, true);
            echo " </h4> 
                                            <label> ";
            // line 58
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["record"], "address", [], "any", false, false, false, 58), "html", null, true);
            echo " </label> 

                                    </div>  
                                </div>
                                </div>
                                <div class=\"col-12 col-md-6 my-auto\">
                                        
                                    <div class=\"text-left text-md-center row \">
                                            
                                            
                                        <div class=\"col-12 col-md-6 p-0 border-right\">
                                            <div class=\"text-left row p-0 pl-5 pl-md-0\">
                                                <div class=\"col-12 my-auto\">
                                                    <label class=\"m-0\"> Создан: ";
            // line 71
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["record"], "created_at", [], "any", false, false, false, 71), "html", null, true);
            echo " </label> 
                                                </div>
                                                <div class=\"col-12 my-auto\">
                                                    <label class=\"m-0\"> Обновлен: ";
            // line 74
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["record"], "updated_at", [], "any", false, false, false, 74), "html", null, true);
            echo " </label> 
                                                </div>
                                                <div class=\"col-12 my-auto=\">
                                                    <label class=\"m-0\"> <b>Статус: ";
            // line 77
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["record"], "stage", [], "any", false, false, false, 77), "html", null, true);
            echo "</b> </label> 
                                                </div>
                                            </div>
                                        </div>

                                        <div class=\"col-12 col-md-6 my-auto text-center p-0 border-right\">
                                            
                                                <form method=\"Post\" action=\"";
            // line 84
            echo $this->extensions['Cms\Twig\Extension']->pageFilter(($context["detailsPage"] ?? null), [($context["detailsUrlParameter"] ?? null) => twig_get_attribute($this->env, $this->source, $context["record"], ($context["detailsKeyColumn"] ?? null), [], "any", false, false, false, 84)]);
            echo "\">
                                                        <input type=\"hidden\" name=\"this_id\" value=\"";
            // line 85
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["record"], "id", [], "any", false, false, false, 85), "html", null, true);
            echo "\" /> 
                                                        <input type=\"submit\" class=\"btn btn-oytline-success my-2 my-sm-0 btn-dark\" value=\"Подробнее\">
                                                </form>
                                        
                                        </div>
                                    </div>
                                
                                </div> 
                            
                            </div>
                            
                            
                        </div>
                    </div> 
                ";
            echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
            // line 100
            echo "            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['record'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 101
        echo "        </div>
    </main>
<div class=\"col-12 col-sm-12 col-md-12 col-lg-12 pt-3 pb-3\"> 
    ";
        // line 104
        if ((twig_get_attribute($this->env, $this->source, ($context["records"] ?? null), "lastPage", [], "any", false, false, false, 104) > 1)) {
            // line 105
            echo "        <ul class=\"pagination dark-link justify-content-center\">
            ";
            // line 106
            if ((twig_get_attribute($this->env, $this->source, ($context["records"] ?? null), "currentPage", [], "any", false, false, false, 106) > 1)) {
                // line 107
                echo "                <li>
                    <a class=\"page-link\" href=\"";
                // line 108
                echo $this->extensions['Cms\Twig\Extension']->pageFilter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "page", [], "any", false, false, false, 108), "baseFileName", [], "any", false, false, false, 108), [($context["pageParam"] ?? null) => (twig_get_attribute($this->env, $this->source, ($context["records"] ?? null), "currentPage", [], "any", false, false, false, 108) - 1)]);
                echo twig_escape_filter($this->env, ($context["paginats"] ?? null), "html", null, true);
                echo "\">
                            &#60
                    </a>
                </li>
            ";
            }
            // line 113
            echo "            ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(range(1, twig_get_attribute($this->env, $this->source, ($context["records"] ?? null), "lastPage", [], "any", false, false, false, 113)));
            foreach ($context['_seq'] as $context["_key"] => $context["page"]) {
                // line 114
                echo "                <li class=\" ";
                echo (((twig_get_attribute($this->env, $this->source, ($context["records"] ?? null), "currentPage", [], "any", false, false, false, 114) == $context["page"])) ? ("active") : (null));
                echo "\">
                    <a class=\"page-link ";
                // line 115
                echo (((twig_get_attribute($this->env, $this->source, ($context["records"] ?? null), "currentPage", [], "any", false, false, false, 115) == $context["page"])) ? ("active bg_d") : (null));
                echo "\" href=\"";
                echo $this->extensions['Cms\Twig\Extension']->pageFilter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "page", [], "any", false, false, false, 115), "baseFileName", [], "any", false, false, false, 115), [($context["pageParam"] ?? null) => $context["page"]]);
                echo twig_escape_filter($this->env, ($context["paginats"] ?? null), "html", null, true);
                echo "\"><lable class=\"";
                echo (((twig_get_attribute($this->env, $this->source, ($context["records"] ?? null), "currentPage", [], "any", false, false, false, 115) == $context["page"])) ? ("light-color") : (null));
                echo "\">";
                echo twig_escape_filter($this->env, $context["page"], "html", null, true);
                echo "</lable></a>
                </li>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['page'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 118
            echo "            ";
            if ((twig_get_attribute($this->env, $this->source, ($context["records"] ?? null), "lastPage", [], "any", false, false, false, 118) > twig_get_attribute($this->env, $this->source, ($context["records"] ?? null), "currentPage", [], "any", false, false, false, 118))) {
                // line 119
                echo "                <li><a class=\"page-link\" href=\"";
                echo $this->extensions['Cms\Twig\Extension']->pageFilter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "page", [], "any", false, false, false, 119), "baseFileName", [], "any", false, false, false, 119), [($context["pageParam"] ?? null) => (twig_get_attribute($this->env, $this->source, ($context["records"] ?? null), "currentPage", [], "any", false, false, false, 119) + 1)]);
                echo twig_escape_filter($this->env, ($context["paginats"] ?? null), "html", null, true);
                echo "\">&#62</a></li>
            ";
            }
            // line 121
            echo "        </ul>
    ";
        }
        // line 123
        echo "</div>
</div>";
    }

    public function getTemplateName()
    {
        return "C:\\wamp64\\www\\project/themes/mine/pages/history.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  277 => 123,  273 => 121,  266 => 119,  263 => 118,  247 => 115,  242 => 114,  237 => 113,  228 => 108,  225 => 107,  223 => 106,  220 => 105,  218 => 104,  213 => 101,  207 => 100,  189 => 85,  185 => 84,  175 => 77,  169 => 74,  163 => 71,  147 => 58,  143 => 57,  135 => 52,  121 => 42,  117 => 41,  104 => 32,  95 => 27,  92 => 26,  86 => 24,  80 => 20,  77 => 19,  71 => 18,  68 => 17,  65 => 16,  60 => 15,  57 => 14,  55 => 13,  47 => 7,  45 => 6,  43 => 5,  41 => 4,  39 => 3,  37 => 2,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% set records = builderList.records %}
{% set displayColumn = builderList.displayColumn %}
{% set noRecordsMessage = builderList.noRecordsMessage %}
{% set detailsPage = builderList.detailsPage %}
{% set detailsKeyColumn = builderList.detailsKeyColumn %}
{% set detailsUrlParameter = builderList.detailsUrlParameter %}
<div class=\"container p-3 pl-4\">
        <h1> История заказов </h1>
</div>
<div class=\"bg_t\">
        <div class=\"container p-3\">
            <div class=\"row\">
                    {% set full_price = 0 %}
                    {% set full_quantity = 0 %}
                    {% for record in records %}
                        {%set full_price=full_price+record.price %}
                        {%set full_quantity=full_quantity+1 %}
                    {%endfor%}
                    {% if full_quantity > 0 %}
                <div class=\"col-6 col-sm-8 col-md-9 col-lg-10\">
                    
                </div>
                {% else %}
                <h4 class=\"p-3\"> {{ noRecordsMessage }} </h4> 
                {% endif %}
                <div class=\"col-6 col-sm-4 col-md-3 col-lg-2 my-auto text-center p-0\">
                    {% if full_quantity > 0 %} 
                    <form action=\"/project/account\"> 
                        <button class=\"btn btn-oytline-success my-2 my-sm-0 btn-dark\"> Личный кабинет </button>
                    </form>
                    
                    {% else %} {% endif %}                                           
                </div> 
            </div>
        </div>
</div>
<div class=\"mx-auto container w-100 m-0\">
    <main class=\"col-12  p-0 pt-3\">
        
        <div class=\"row text-center col-12 w-100 m-0 p-0\">
            {% for record in records %}
                {% spaceless %}          
                    <div class=\"col-12 mb-3\" >
                        <div class=\"col-12 py-3 bg_t dark-link-1 item h-100\"> 

                            <div class=\"row\">
                                <div class=\"col-12 col-md-6 my-auto\">
                                        
                                <div class=\"text-center row \">
                                    <div class=\"col-12\">

                                        <h3> ID: {{record.order_id}} </h3> 

                                    </div>
                                    <div class=\"col-12 p-0 pt-md-0 my-auto \">

                                            <h4> {{record.pickup}} </h4> 
                                            <label> {{record.address}} </label> 

                                    </div>  
                                </div>
                                </div>
                                <div class=\"col-12 col-md-6 my-auto\">
                                        
                                    <div class=\"text-left text-md-center row \">
                                            
                                            
                                        <div class=\"col-12 col-md-6 p-0 border-right\">
                                            <div class=\"text-left row p-0 pl-5 pl-md-0\">
                                                <div class=\"col-12 my-auto\">
                                                    <label class=\"m-0\"> Создан: {{record.created_at}} </label> 
                                                </div>
                                                <div class=\"col-12 my-auto\">
                                                    <label class=\"m-0\"> Обновлен: {{  record.updated_at }} </label> 
                                                </div>
                                                <div class=\"col-12 my-auto=\">
                                                    <label class=\"m-0\"> <b>Статус: {{ record.stage }}</b> </label> 
                                                </div>
                                            </div>
                                        </div>

                                        <div class=\"col-12 col-md-6 my-auto text-center p-0 border-right\">
                                            
                                                <form method=\"Post\" action=\"{{ detailsPage|page({ (detailsUrlParameter): attribute(record, detailsKeyColumn) }) }}\">
                                                        <input type=\"hidden\" name=\"this_id\" value=\"{{ record.id }}\" /> 
                                                        <input type=\"submit\" class=\"btn btn-oytline-success my-2 my-sm-0 btn-dark\" value=\"Подробнее\">
                                                </form>
                                        
                                        </div>
                                    </div>
                                
                                </div> 
                            
                            </div>
                            
                            
                        </div>
                    </div> 
                {% endspaceless %}
            {% endfor %}
        </div>
    </main>
<div class=\"col-12 col-sm-12 col-md-12 col-lg-12 pt-3 pb-3\"> 
    {% if records.lastPage > 1 %}
        <ul class=\"pagination dark-link justify-content-center\">
            {% if records.currentPage > 1 %}
                <li>
                    <a class=\"page-link\" href=\"{{ this.page.baseFileName|page({ (pageParam): (records.currentPage-1) }) }}{{paginats}}\">
                            &#60
                    </a>
                </li>
            {% endif %}
            {% for page in 1..records.lastPage %}
                <li class=\" {{ records.currentPage == page ? 'active' : null }}\">
                    <a class=\"page-link {{ records.currentPage == page ? 'active bg_d' : null }}\" href=\"{{ this.page.baseFileName|page({ (pageParam): page }) }}{{paginats}}\"><lable class=\"{{ records.currentPage == page ? 'light-color' : null }}\">{{ page }}</lable></a>
                </li>
            {% endfor %}
            {% if records.lastPage > records.currentPage %}
                <li><a class=\"page-link\" href=\"{{ this.page.baseFileName|page({ (pageParam): (records.currentPage+1) }) }}{{paginats}}\">&#62</a></li>
            {% endif %}
        </ul>
    {% endif %}
</div>
</div>", "C:\\wamp64\\www\\project/themes/mine/pages/history.htm", "");
    }
}
