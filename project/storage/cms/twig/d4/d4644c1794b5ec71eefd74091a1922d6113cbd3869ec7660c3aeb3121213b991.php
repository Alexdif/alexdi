<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* C:\wamp64\www\project/themes/mine/pages/history_details.htm */
class __TwigTemplate_7e5bd890270b3400c4e3050201c2bd8cf62e23fc881485e0d4ab6f77d218c8df extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        $context["records"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "records", [], "any", false, false, false, 1);
        // line 2
        $context["displayColumn"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "displayColumn", [], "any", false, false, false, 2);
        // line 3
        $context["noRecordsMessage"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "noRecordsMessage", [], "any", false, false, false, 3);
        // line 4
        $context["detailsPage"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "detailsPage", [], "any", false, false, false, 4);
        // line 5
        $context["detailsKeyColumn"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "detailsKeyColumn", [], "any", false, false, false, 5);
        // line 6
        $context["detailsUrlParameter"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "detailsUrlParameter", [], "any", false, false, false, 6);
        // line 7
        echo "
";
        // line 8
        $context["order"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "param", [], "any", false, false, false, 8), "order_id", [], "any", false, false, false, 8);
        // line 9
        echo "
<div class=\"container p-3 pl-4\">
        <h1> Заказ N: ";
        // line 11
        echo twig_escape_filter($this->env, ($context["order"] ?? null), "html", null, true);
        echo " </h1>
</div>
<div class=\"bg_t\">
        <div class=\"container p-3\">
            <div class=\"row\">
                    ";
        // line 16
        $context["full_price"] = 0;
        // line 17
        echo "                    ";
        $context["full_quantity"] = 0;
        // line 18
        echo "                    ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["records"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["record"]) {
            // line 19
            echo "                        ";
            $context["full_price"] = (($context["full_price"] ?? null) + twig_get_attribute($this->env, $this->source, $context["record"], "price", [], "any", false, false, false, 19));
            // line 20
            echo "                        ";
            $context["full_quantity"] = (($context["full_quantity"] ?? null) + 1);
            // line 21
            echo "                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['record'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 22
        echo "                    ";
        if ((($context["full_price"] ?? null) > 0)) {
            // line 23
            echo "                    <div class=\"col-6 col-sm-8 col-md-9 col-lg-10\">
                            <label>  Стоимость:<b> ";
            // line 24
            echo twig_escape_filter($this->env, ($context["full_price"] ?? null), "html", null, true);
            echo "₽</b></label> <br>  
                            <label>  Количество: <b> ";
            // line 25
            echo twig_escape_filter($this->env, ($context["full_quantity"] ?? null), "html", null, true);
            echo " шт.</b></label>
                        </div>
                ";
        } else {
            // line 28
            echo "                <h4 class=\"p-3\"> ";
            echo twig_escape_filter($this->env, ($context["noRecordsMessage"] ?? null), "html", null, true);
            echo " </h4> 
                ";
        }
        // line 30
        echo "                <div class=\"col-6 col-sm-4 col-md-3 col-lg-2 my-auto text-center p-0\">
                    ";
        // line 31
        if ((($context["full_price"] ?? null) > 0)) {
            echo " 
                    <form action=\"/project/history\"> 
                        <button class=\"btn btn-oytline-success my-2 my-sm-0 btn-dark\"> История заказов </button>
                    </form>
                    
                    ";
        } else {
            // line 36
            echo " ";
        }
        echo "                                           
                </div> 
            </div>
        </div>
</div>
<div class=\"mx-auto container w-100 m-0\">
    <main class=\"col-12  p-0 pt-3\">
        
        <div class=\"row text-center col-12 w-100 m-0 p-0\">
            ";
        // line 45
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["records"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["record"]) {
            // line 46
            echo "                ";
            ob_start(function () { return ''; });
            echo "          
                    <div class=\"col-12 mb-3\" >
                        <div class=\"col-12 pb-3 bg_t dark-link-1 item h-100\"> 

                            
                            <div class=\"row\">
                                <div class=\"col-6 col-md-4 col-lg-3 col-xl-2 pt-3\">
                                    ";
            // line 53
            if (($context["detailsPage"] ?? null)) {
                // line 54
                echo "                                        <a href=\"";
                echo $this->extensions['Cms\Twig\Extension']->pageFilter(($context["detailsPage"] ?? null), [($context["detailsUrlParameter"] ?? null) => twig_get_attribute($this->env, $this->source, $context["record"], ($context["detailsKeyColumn"] ?? null), [], "any", false, false, false, 54)]);
                echo "\">
                                    ";
            }
            // line 56
            echo "                                    <img src=\"";
            echo $this->extensions['System\Twig\Extension']->mediaFilter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["record"], "product", [], "any", false, false, false, 56), "photo", [], "any", false, false, false, 56));
            echo "\" alt=\"Изображение товара\" class=\"w-100\"> 
                                    ";
            // line 57
            if (($context["detailsPage"] ?? null)) {
                // line 58
                echo "                                        </a>
                                    ";
            }
            // line 60
            echo "                                </div>
                                
                                <div class=\"col-6 col-md-8 col-lg-9 col-xl-10 my-auto\">
                                        
                                    <div class=\"text-left text-md-center row \">
                                            
                                        <div class=\"col-12 my-auto col-md-6 p-0 pt-3 pt-md-0\">
                                            ";
            // line 67
            if (($context["detailsPage"] ?? null)) {
                // line 68
                echo "                                                <a href=\"";
                echo $this->extensions['Cms\Twig\Extension']->pageFilter(($context["detailsPage"] ?? null), [($context["detailsUrlParameter"] ?? null) => twig_get_attribute($this->env, $this->source, $context["record"], ($context["detailsKeyColumn"] ?? null), [], "any", false, false, false, 68)]);
                echo "\">
                                            ";
            }
            // line 70
            echo "                                            <h3> ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["record"], "product", [], "any", false, false, false, 70), "name", [], "any", false, false, false, 70), "html", null, true);
            echo " </h3> 
                                            ";
            // line 71
            if (($context["detailsPage"] ?? null)) {
                // line 72
                echo "                                                </a>
                                            ";
            }
            // line 74
            echo "                                        </div>      
                                        <div class=\"col-12 col-md-3 p-0 border-right\">
                                            <div class=\"text-left row p-0 \">
                                                <div class=\"col-12 my-auto\">
                                                    <label class=\"m-0\"> Цвет: ";
            // line 78
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["record"], "product", [], "any", false, false, false, 78), "color", [], "any", false, false, false, 78), "html", null, true);
            echo " </label> 
                                                </div>
                                                <div class=\"col-12 my-auto\">
                                                    <label class=\"m-0\">Размер: ";
            // line 81
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["record"], "size", [], "any", false, false, false, 81), "html", null, true);
            echo " </label> 
                                                </div>
                                                <div class=\"col-12 my-auto=\">
                                                    <label class=\"m-0\"> <b>Цена: ";
            // line 84
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["record"], "price", [], "any", false, false, false, 84), "html", null, true);
            echo "</b> </label> 
                                                </div>
                                            </div>
                                        </div>

                                        <div class=\"col-12 col-md-3 my-auto center p-0 border-right\">
                                            <form action=\"";
            // line 90
            echo $this->extensions['Cms\Twig\Extension']->pageFilter(($context["detailsPage"] ?? null), [($context["detailsUrlParameter"] ?? null) => twig_get_attribute($this->env, $this->source, $context["record"], ($context["detailsKeyColumn"] ?? null), [], "any", false, false, false, 90)]);
            echo "\">
                                            <input type=\"submit\" class=\"btn btn-oytline-success my-2 my-sm-0 btn-dark\" value=\"Подробнее\">
                                            </form>
                                        </form>                                        
                                        </div>
                                    </div>
                                
                                </div> 
                            </div>
                            
                            
                        </div>
                    </div> 
                ";
            echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
            // line 104
            echo "
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['record'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 106
        echo "        </div>
    </main>
<div class=\"col-12 col-sm-12 col-md-12 col-lg-12 pt-3 pb-3\"> 
    ";
        // line 109
        if ((twig_get_attribute($this->env, $this->source, ($context["records"] ?? null), "lastPage", [], "any", false, false, false, 109) > 1)) {
            // line 110
            echo "        <ul class=\"pagination dark-link justify-content-center\">
            ";
            // line 111
            if ((twig_get_attribute($this->env, $this->source, ($context["records"] ?? null), "currentPage", [], "any", false, false, false, 111) > 1)) {
                // line 112
                echo "                <li>
                    <a class=\"page-link\" href=\"";
                // line 113
                echo $this->extensions['Cms\Twig\Extension']->pageFilter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "page", [], "any", false, false, false, 113), "baseFileName", [], "any", false, false, false, 113), [($context["pageParam"] ?? null) => (twig_get_attribute($this->env, $this->source, ($context["records"] ?? null), "currentPage", [], "any", false, false, false, 113) - 1)]);
                echo twig_escape_filter($this->env, ($context["paginats"] ?? null), "html", null, true);
                echo "\">
                            &#60
                    </a>
                </li>
            ";
            }
            // line 118
            echo "            ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(range(1, twig_get_attribute($this->env, $this->source, ($context["records"] ?? null), "lastPage", [], "any", false, false, false, 118)));
            foreach ($context['_seq'] as $context["_key"] => $context["page"]) {
                // line 119
                echo "                <li class=\" ";
                echo (((twig_get_attribute($this->env, $this->source, ($context["records"] ?? null), "currentPage", [], "any", false, false, false, 119) == $context["page"])) ? ("active") : (null));
                echo "\">
                    <a class=\"page-link ";
                // line 120
                echo (((twig_get_attribute($this->env, $this->source, ($context["records"] ?? null), "currentPage", [], "any", false, false, false, 120) == $context["page"])) ? ("active bg_d") : (null));
                echo "\" href=\"";
                echo $this->extensions['Cms\Twig\Extension']->pageFilter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "page", [], "any", false, false, false, 120), "baseFileName", [], "any", false, false, false, 120), [($context["pageParam"] ?? null) => $context["page"]]);
                echo twig_escape_filter($this->env, ($context["paginats"] ?? null), "html", null, true);
                echo "\"><lable class=\"";
                echo (((twig_get_attribute($this->env, $this->source, ($context["records"] ?? null), "currentPage", [], "any", false, false, false, 120) == $context["page"])) ? ("light-color") : (null));
                echo "\">";
                echo twig_escape_filter($this->env, $context["page"], "html", null, true);
                echo "</lable></a>
                </li>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['page'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 123
            echo "            ";
            if ((twig_get_attribute($this->env, $this->source, ($context["records"] ?? null), "lastPage", [], "any", false, false, false, 123) > twig_get_attribute($this->env, $this->source, ($context["records"] ?? null), "currentPage", [], "any", false, false, false, 123))) {
                // line 124
                echo "                <li><a class=\"page-link\" href=\"";
                echo $this->extensions['Cms\Twig\Extension']->pageFilter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "page", [], "any", false, false, false, 124), "baseFileName", [], "any", false, false, false, 124), [($context["pageParam"] ?? null) => (twig_get_attribute($this->env, $this->source, ($context["records"] ?? null), "currentPage", [], "any", false, false, false, 124) + 1)]);
                echo twig_escape_filter($this->env, ($context["paginats"] ?? null), "html", null, true);
                echo "\">&#62</a></li>
            ";
            }
            // line 126
            echo "        </ul>
    ";
        }
        // line 128
        echo "</div>
</div>";
    }

    public function getTemplateName()
    {
        return "C:\\wamp64\\www\\project/themes/mine/pages/history_details.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  310 => 128,  306 => 126,  299 => 124,  296 => 123,  280 => 120,  275 => 119,  270 => 118,  261 => 113,  258 => 112,  256 => 111,  253 => 110,  251 => 109,  246 => 106,  239 => 104,  222 => 90,  213 => 84,  207 => 81,  201 => 78,  195 => 74,  191 => 72,  189 => 71,  184 => 70,  178 => 68,  176 => 67,  167 => 60,  163 => 58,  161 => 57,  156 => 56,  150 => 54,  148 => 53,  137 => 46,  133 => 45,  120 => 36,  111 => 31,  108 => 30,  102 => 28,  96 => 25,  92 => 24,  89 => 23,  86 => 22,  80 => 21,  77 => 20,  74 => 19,  69 => 18,  66 => 17,  64 => 16,  56 => 11,  52 => 9,  50 => 8,  47 => 7,  45 => 6,  43 => 5,  41 => 4,  39 => 3,  37 => 2,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% set records = builderList.records %}
{% set displayColumn = builderList.displayColumn %}
{% set noRecordsMessage = builderList.noRecordsMessage %}
{% set detailsPage = builderList.detailsPage %}
{% set detailsKeyColumn = builderList.detailsKeyColumn %}
{% set detailsUrlParameter = builderList.detailsUrlParameter %}

{% set order = this.param.order_id %}

<div class=\"container p-3 pl-4\">
        <h1> Заказ N: {{order}} </h1>
</div>
<div class=\"bg_t\">
        <div class=\"container p-3\">
            <div class=\"row\">
                    {% set full_price = 0 %}
                    {% set full_quantity = 0 %}
                    {% for record in records %}
                        {%set full_price=full_price+record.price %}
                        {%set full_quantity=full_quantity+1 %}
                    {%endfor%}
                    {% if full_price > 0 %}
                    <div class=\"col-6 col-sm-8 col-md-9 col-lg-10\">
                            <label>  Стоимость:<b> {{full_price}}₽</b></label> <br>  
                            <label>  Количество: <b> {{full_quantity}} шт.</b></label>
                        </div>
                {% else %}
                <h4 class=\"p-3\"> {{ noRecordsMessage }} </h4> 
                {% endif %}
                <div class=\"col-6 col-sm-4 col-md-3 col-lg-2 my-auto text-center p-0\">
                    {% if full_price > 0 %} 
                    <form action=\"/project/history\"> 
                        <button class=\"btn btn-oytline-success my-2 my-sm-0 btn-dark\"> История заказов </button>
                    </form>
                    
                    {% else %} {% endif %}                                           
                </div> 
            </div>
        </div>
</div>
<div class=\"mx-auto container w-100 m-0\">
    <main class=\"col-12  p-0 pt-3\">
        
        <div class=\"row text-center col-12 w-100 m-0 p-0\">
            {% for record in records %}
                {% spaceless %}          
                    <div class=\"col-12 mb-3\" >
                        <div class=\"col-12 pb-3 bg_t dark-link-1 item h-100\"> 

                            
                            <div class=\"row\">
                                <div class=\"col-6 col-md-4 col-lg-3 col-xl-2 pt-3\">
                                    {% if detailsPage %}
                                        <a href=\"{{ detailsPage|page({ (detailsUrlParameter): attribute(record, detailsKeyColumn) }) }}\">
                                    {% endif %}
                                    <img src=\"{{record.product.photo|media}}\" alt=\"Изображение товара\" class=\"w-100\"> 
                                    {% if detailsPage %}
                                        </a>
                                    {% endif %}
                                </div>
                                
                                <div class=\"col-6 col-md-8 col-lg-9 col-xl-10 my-auto\">
                                        
                                    <div class=\"text-left text-md-center row \">
                                            
                                        <div class=\"col-12 my-auto col-md-6 p-0 pt-3 pt-md-0\">
                                            {% if detailsPage %}
                                                <a href=\"{{ detailsPage|page({ (detailsUrlParameter): attribute(record, detailsKeyColumn) }) }}\">
                                            {% endif %}
                                            <h3> {{record.product.name}} </h3> 
                                            {% if detailsPage %}
                                                </a>
                                            {% endif %}
                                        </div>      
                                        <div class=\"col-12 col-md-3 p-0 border-right\">
                                            <div class=\"text-left row p-0 \">
                                                <div class=\"col-12 my-auto\">
                                                    <label class=\"m-0\"> Цвет: {{record.product.color}} </label> 
                                                </div>
                                                <div class=\"col-12 my-auto\">
                                                    <label class=\"m-0\">Размер: {{  record.size }} </label> 
                                                </div>
                                                <div class=\"col-12 my-auto=\">
                                                    <label class=\"m-0\"> <b>Цена: {{ record.price }}</b> </label> 
                                                </div>
                                            </div>
                                        </div>

                                        <div class=\"col-12 col-md-3 my-auto center p-0 border-right\">
                                            <form action=\"{{ detailsPage|page({ (detailsUrlParameter): attribute(record, detailsKeyColumn) }) }}\">
                                            <input type=\"submit\" class=\"btn btn-oytline-success my-2 my-sm-0 btn-dark\" value=\"Подробнее\">
                                            </form>
                                        </form>                                        
                                        </div>
                                    </div>
                                
                                </div> 
                            </div>
                            
                            
                        </div>
                    </div> 
                {% endspaceless %}

            {% endfor %}
        </div>
    </main>
<div class=\"col-12 col-sm-12 col-md-12 col-lg-12 pt-3 pb-3\"> 
    {% if records.lastPage > 1 %}
        <ul class=\"pagination dark-link justify-content-center\">
            {% if records.currentPage > 1 %}
                <li>
                    <a class=\"page-link\" href=\"{{ this.page.baseFileName|page({ (pageParam): (records.currentPage-1) }) }}{{paginats}}\">
                            &#60
                    </a>
                </li>
            {% endif %}
            {% for page in 1..records.lastPage %}
                <li class=\" {{ records.currentPage == page ? 'active' : null }}\">
                    <a class=\"page-link {{ records.currentPage == page ? 'active bg_d' : null }}\" href=\"{{ this.page.baseFileName|page({ (pageParam): page }) }}{{paginats}}\"><lable class=\"{{ records.currentPage == page ? 'light-color' : null }}\">{{ page }}</lable></a>
                </li>
            {% endfor %}
            {% if records.lastPage > records.currentPage %}
                <li><a class=\"page-link\" href=\"{{ this.page.baseFileName|page({ (pageParam): (records.currentPage+1) }) }}{{paginats}}\">&#62</a></li>
            {% endif %}
        </ul>
    {% endif %}
</div>
</div>", "C:\\wamp64\\www\\project/themes/mine/pages/history_details.htm", "");
    }
}
