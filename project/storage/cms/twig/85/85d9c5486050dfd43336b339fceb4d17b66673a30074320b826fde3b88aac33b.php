<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* C:\wamp64\www\project/themes/mine/pages/mine_page.htm */
class __TwigTemplate_0a0f86a41f9bad753926ec2778a63ad479dd7a8657f23d143462bcb441117918 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<div class=\"row  w-100 m-0\">
    <div class=\"row col-12 col-lg-6 my-auto text-center w-100 m-0 p-0\">
        <div class=\"col-6 my-auto text-center p-0\"> 
            <p class=\"m-0\"> Air Max 270 </p>
            <h2 class=\"text-uppercase\"> Стиль жизни </h2>
            <p> Лимитированная серия </p>
            <form action=\"/project/product/air-max-270-black \">
                <button class=\"btn btn-dark\"> Купить </button>
            </form>
    </div>
    
    <div class=\"col-6 my-auto text-center p-0\"> 
        <a href=\"/project/product/air-max-270-black \">
            <img src=\"";
        // line 14
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/270two.png");
        echo "\"  alt=\"270\" class=\"d-block w-100\">
        </a>
    </div>
    <div class=\"col-6 my-auto text-center p-0\"> 
        <a href=\"/project/product/air-max-270-black \">
            <img src=\"";
        // line 19
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/270one.png");
        echo "\"  alt=\"270\" class=\"d-block w-100\">
        </a>
    </div>
    
    <div class=\"col-6 my-auto text-center p-0\"> 
        <p class=\"m-0\"> Air Max 270 </p>
        <h2 class=\"text-uppercase\"> Новая коллекция </h2>
        <p> Для сильных и независимых </p>
        <form action=\"/project/product/air-max-270-black \">
            <button class=\"btn btn-dark\"> Купить </button>
        </form>
        
    </div>
    </div>
    <div class=\"col-12 col-lg-6 p-0 div-rel\">
        <a href=\"/project/product/air-max-270-black \">
        <div class=\"col-12 img-wrap p-0 m-0\">
            <img src=\"";
        // line 36
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/270.png");
        echo "\"  alt=\"270\" class=\"d-block w-100\">
            <h2 class=\"img-wrap text-uppercase\">Одержи победу над собой</p>
        </div> 
    </a>   
    </div>
</div>

<div class=\"row  w-100 m-0 py-1\">
    <div class=\"col-12 col-lg-6 p-0 div-rel order-2 order-md-1\">
        <a href=\"/project/product/hightweater_eqt\">
        <div class=\"col-12 img-wrap p-0 m-0\">
            <img src=\"";
        // line 47
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/EQT.png");
        echo "\"  alt=\"270\" class=\"d-block w-100\">
            <h2 class=\"img-wrap text-uppercase\"> Куртка EQT <br> Создай себя сам</p>
        </div>   
    </a>  
    </div>
    <div class=\"row col-12 col-lg-6 my-auto text-center w-100 m-0 p-0 order-1 order-md-2\">
        <div class=\"col-12 my-auto text-center p-0\"> 
        <a href=\"/project/product/ADIDAS_EQUIPMENT_SUPPORT_A_green? \">
            <img src=\"";
        // line 55
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/EQTtwo.png");
        echo "\"  alt=\"270\" class=\"d-block w-100\">
        </a>
        </div>           
        <div class=\"col-6 my-auto text-center p-0\"> 
            <p class=\"m-0\"> EQT Support ADV </p>
            <h2 class=\"text-uppercase\"> Легче воздуха </h2>
            <h6 class=\"mb-3 text-uppercase\"> Быстрее ветра </h6>
            <form action=\"/project/product/ADIDAS_EQUIPMENT_SUPPORT_A_green\">
                <button class=\"btn btn-dark\"> Купить </button>
            </form>
        </div>
        <div class=\"col-6 my-auto text-center p-0\"> 
        <a href=\"/project/product/ADIDAS_EQUIPMENT_SUPPORT_A_green? \">
            <img src=\"";
        // line 68
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/EQTthree.png");
        echo "\"  alt=\"270\" class=\"d-block w-100\">
        </a>
        </div>        
    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "C:\\wamp64\\www\\project/themes/mine/pages/mine_page.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  119 => 68,  103 => 55,  92 => 47,  78 => 36,  58 => 19,  50 => 14,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"row  w-100 m-0\">
    <div class=\"row col-12 col-lg-6 my-auto text-center w-100 m-0 p-0\">
        <div class=\"col-6 my-auto text-center p-0\"> 
            <p class=\"m-0\"> Air Max 270 </p>
            <h2 class=\"text-uppercase\"> Стиль жизни </h2>
            <p> Лимитированная серия </p>
            <form action=\"/project/product/air-max-270-black \">
                <button class=\"btn btn-dark\"> Купить </button>
            </form>
    </div>
    
    <div class=\"col-6 my-auto text-center p-0\"> 
        <a href=\"/project/product/air-max-270-black \">
            <img src=\"{{ 'assets/images/270two.png'| theme }}\"  alt=\"270\" class=\"d-block w-100\">
        </a>
    </div>
    <div class=\"col-6 my-auto text-center p-0\"> 
        <a href=\"/project/product/air-max-270-black \">
            <img src=\"{{ 'assets/images/270one.png'| theme }}\"  alt=\"270\" class=\"d-block w-100\">
        </a>
    </div>
    
    <div class=\"col-6 my-auto text-center p-0\"> 
        <p class=\"m-0\"> Air Max 270 </p>
        <h2 class=\"text-uppercase\"> Новая коллекция </h2>
        <p> Для сильных и независимых </p>
        <form action=\"/project/product/air-max-270-black \">
            <button class=\"btn btn-dark\"> Купить </button>
        </form>
        
    </div>
    </div>
    <div class=\"col-12 col-lg-6 p-0 div-rel\">
        <a href=\"/project/product/air-max-270-black \">
        <div class=\"col-12 img-wrap p-0 m-0\">
            <img src=\"{{ 'assets/images/270.png'| theme }}\"  alt=\"270\" class=\"d-block w-100\">
            <h2 class=\"img-wrap text-uppercase\">Одержи победу над собой</p>
        </div> 
    </a>   
    </div>
</div>

<div class=\"row  w-100 m-0 py-1\">
    <div class=\"col-12 col-lg-6 p-0 div-rel order-2 order-md-1\">
        <a href=\"/project/product/hightweater_eqt\">
        <div class=\"col-12 img-wrap p-0 m-0\">
            <img src=\"{{ 'assets/images/EQT.png'| theme }}\"  alt=\"270\" class=\"d-block w-100\">
            <h2 class=\"img-wrap text-uppercase\"> Куртка EQT <br> Создай себя сам</p>
        </div>   
    </a>  
    </div>
    <div class=\"row col-12 col-lg-6 my-auto text-center w-100 m-0 p-0 order-1 order-md-2\">
        <div class=\"col-12 my-auto text-center p-0\"> 
        <a href=\"/project/product/ADIDAS_EQUIPMENT_SUPPORT_A_green? \">
            <img src=\"{{ 'assets/images/EQTtwo.png'| theme }}\"  alt=\"270\" class=\"d-block w-100\">
        </a>
        </div>           
        <div class=\"col-6 my-auto text-center p-0\"> 
            <p class=\"m-0\"> EQT Support ADV </p>
            <h2 class=\"text-uppercase\"> Легче воздуха </h2>
            <h6 class=\"mb-3 text-uppercase\"> Быстрее ветра </h6>
            <form action=\"/project/product/ADIDAS_EQUIPMENT_SUPPORT_A_green\">
                <button class=\"btn btn-dark\"> Купить </button>
            </form>
        </div>
        <div class=\"col-6 my-auto text-center p-0\"> 
        <a href=\"/project/product/ADIDAS_EQUIPMENT_SUPPORT_A_green? \">
            <img src=\"{{ 'assets/images/EQTthree.png'| theme }}\"  alt=\"270\" class=\"d-block w-100\">
        </a>
        </div>        
    </div>
</div>", "C:\\wamp64\\www\\project/themes/mine/pages/mine_page.htm", "");
    }
}
